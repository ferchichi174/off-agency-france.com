<?php
// turn on debug for localhost etc
$whitelist = array('127.0.0.1', '::1', 'localhost', 'web357.com', 'www.web357.com');
if (in_array($_SERVER['SERVER_NAME'], $whitelist)) {
	//error_reporting(E_ALL);
    //ini_set('display_errors', '1');
}

/* ======================================================
 # Fixed HTML Toolbar for WordPress - v1.0.3 (free version)
 # -------------------------------------------------------
 # For WordPress
 # Author: Web357
 # Copyright @ 2014-2021 Web357. All rights reserved.
 # License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 # Website: https:/www.web357.com
 # Demo: https://demo.web357.com/wordpress/?page_id=17
 # Support: support@web357.com
 # Last modified: Wednesday 28 July 2021, 05:21:04 PM
 ========================================================= */
 
/**
 * Plugin Name:       Fixed HTML Toolbar
 * Plugin URI:        https://www.web357.com/product/fixed-html-toolbar-wordpress-plugin
 * Description:       A fixed HTML toolbar which displayed at the bottom or at the top of your website. You can add up to 5 linked icons or just an HTML code. It mostly used to show social icons or any other notification that needs to be static during page scrolling.
 * Version:           1.0.3
 * Author:            Web357
 * Author URI:        https://www.web357.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       fixed-html-toolbar
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'FIXEDHTMLTOOLBAR_VERSION', '1.0.3' );

/**
 * The code that runs during plugin activation.
 */
function activate_FixedHtmlToolbar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-activator.php';
	FixedHtmlToolbar_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_FixedHtmlToolbar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-deactivator.php';
	FixedHtmlToolbar_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_FixedHtmlToolbar' );
register_deactivation_hook( __FILE__, 'deactivate_FixedHtmlToolbar' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-main.php';

/**
 * Begins execution of the plugin.
 */
function run_FixedHtmlToolbar() {

	$plugin = new FixedHtmlToolbar();
	$plugin->run();

}
run_FixedHtmlToolbar();