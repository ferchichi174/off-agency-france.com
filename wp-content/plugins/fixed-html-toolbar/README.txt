=== Fixed HTML Toolbar ===
Contributors: yiannistaos
Tags: sticky, toolbar, fixed, web357
Donate link: https://www.paypal.me/web357
Requires at least: 5.3
Tested up to: 5.8
Requires PHP: 7.0
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A fixed HTML toolbar which displayed at the bottom or at the top of your website. You can add up to 5 linked icons or just an HTML code. It mostly used to show social icons or any other notification that needs to be static during page scrolling.

== Description ==
A fixed HTML toolbar which displayed at the bottom or at the top of your website. You can add up to 5 linked icons or just an HTML code. It mostly used to show social icons or any other notification that needs to be static during page scrolling.

[Live Demo](https://demo.web357.com/wordpress/fixed-html-toolbar/)

== Installation ==
The plugin is simple to install:

1. Download the file `fixed-html-toolbar.zip`.
2. Unzip it.
3. Upload `fixed-html-toolbar` directory to your `/wp-content/plugins` directory.
4. Go to the plugin management page and enable the plugin.
5. Configure the options from the `Settings > Fixed HTML Toolbar` page

== Frequently Asked Questions ==
= Why would I want to use this plugin? =

This plugin is mostly used to show a static content to your top or bottom of your website.
It is useful for discount notifications or the display of social icons.

== Screenshots ==
1. Base Settings with HTML code on the toolbar
2. Base Settings with custom icons on the toolbar
3. The display of toolbar at the bottom of page
4. The display of toolbar at the top of page
5. Styling options

== Changelog ==
= 28-Jul-2021 : v1.0.3 =
* [Compatibility] Fully compatible with WordPress v5.8
* Minor fixes and improvements

= 09-Apr-2021 : v1.0.2 =
* [Compatibility] Fully compatible with WordPress v5.7
* Minor fixes and improvements

= 17-Jul-2019 : v1.0.1 =
* [New parameter] Add new setting "Locations". You can display the fixed html toolbar only in specific locations like posts, pages, search, archives, categories, homepage.
* Hide the Fixed HTML Toolbar when printing a web page.
* Minor bug fixes

= 18-Apr-2019 : v1.0.0 =
* First beta release