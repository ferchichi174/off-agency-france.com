<?php
/* ======================================================
 # Fixed HTML Toolbar for WordPress - v1.0.3 (free version)
 # -------------------------------------------------------
 # For WordPress
 # Author: Web357
 # Copyright @ 2014-2021 Web357. All rights reserved.
 # License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 # Website: https:/www.web357.com
 # Demo: https://demo.web357.com/wordpress/?page_id=17
 # Support: support@web357.com
 # Last modified: Wednesday 28 July 2021, 05:21:04 PM
 ========================================================= */
/**
 * Define the internationalization functionality
 */
class FixedHtmlToolbar_i18n {

	/**
	 * Load the plugin text domain for translation.
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'fixed-html-toolbar',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}
}