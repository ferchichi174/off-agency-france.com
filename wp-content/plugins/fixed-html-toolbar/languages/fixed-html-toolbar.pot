#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-04-22 14:44+0300\n"
"PO-Revision-Date: 2019-04-11 16:38+0300\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;esc_html;esc_html__;esc_att;esc_attr__;"
"esc_attr_e;esc_html_e\n"
"X-Poedit-SearchPath-0: .\n"

#: admin/class-admin.php:73
msgid "Fixed HTML Toolbar settings"
msgstr ""

#: admin/class-admin.php:92 admin/class-admin.php:253
#: admin/class-admin.php:739
msgid "Settings"
msgstr ""

#: admin/class-admin.php:97
msgid "About Fixed HTML Toolbar"
msgstr ""

#: admin/class-admin.php:102
msgid "About Web357"
msgstr ""

#: admin/class-admin.php:313
msgid "Status"
msgstr ""

#: admin/class-admin.php:321
msgid "Published"
msgstr ""

#: admin/class-admin.php:322
msgid "Unpublished"
msgstr ""

#: admin/class-admin.php:331
msgid "Position"
msgstr ""

#: admin/class-admin.php:339
msgid "Bottom"
msgstr ""

#: admin/class-admin.php:340
msgid "Top"
msgstr ""

#: admin/class-admin.php:349
msgid "License Key"
msgstr ""

#: admin/class-admin.php:369
msgid "Type of Toolbar Content"
msgstr ""

#: admin/class-admin.php:377
msgid "HTML"
msgstr ""

#: admin/class-admin.php:378
msgid "Icons"
msgstr ""

#: admin/class-admin.php:387
msgid "Add HTML into the Toolbar"
msgstr ""

#: admin/class-admin.php:408
msgid "Icon"
msgstr ""

#: admin/class-admin.php:424
msgid "Link for the icon"
msgstr ""

#: admin/class-admin.php:443
msgid "Background Color"
msgstr ""

#: admin/class-admin.php:458
msgid "Border Color"
msgstr ""

#: admin/class-admin.php:473
msgid "Border thickness"
msgstr ""

#: admin/class-admin.php:482 admin/class-admin.php:565
msgid "recommended"
msgstr ""

#: admin/class-admin.php:500
msgid "px"
msgstr ""

#: admin/class-admin.php:504
msgid "Gap (the vertical padding)"
msgstr ""

#: admin/class-admin.php:518
msgid "Text Alignment"
msgstr ""

#: admin/class-admin.php:526
msgid "Left"
msgstr ""

#: admin/class-admin.php:527
msgid "Center"
msgstr ""

#: admin/class-admin.php:528
msgid "Right"
msgstr ""

#: admin/class-admin.php:536
msgid "Link Target"
msgstr ""

#: admin/class-admin.php:545
msgid "Open in the same window"
msgstr ""

#: admin/class-admin.php:546
msgid "Open in a new window"
msgstr ""

#: admin/class-admin.php:554
msgid "Margin between icons"
msgstr ""

#: admin/class-admin.php:581
msgid "Add Custom CSS Code"
msgstr ""

#: admin/class-admin.php:740
msgid "Base Settings"
msgstr ""

#: admin/class-admin.php:745
msgid "Toolbar's Content"
msgstr ""

#: admin/class-admin.php:750
msgid "Styling Options"
msgstr ""

#: admin/partials/about-plugin-view.php:7
msgid "Description"
msgstr ""

#: admin/partials/about-plugin-view.php:12
msgid "Need support?"
msgstr ""

#: admin/partials/about-plugin-view.php:15
#, php-format
msgid ""
"<p>If you are having problems with this plugin, please browse it's "
"<a href=\"%1$s\">Documentation</a> or talk about them in the <a "
"href=\"%2$s\">Support forum</a></p>"
msgstr ""

#: admin/partials/about-plugin-view.php:23
msgid "Do you like this plugin?"
msgstr ""

#: admin/partials/about-plugin-view.php:27
#, php-format
msgid "<p><a href=\"%1$s\">Rate it 5</a> on WordPress.org</p>"
msgstr ""

#: admin/partials/about-plugin-view.php:32
#, php-format
msgid ""
"<p>Blog about it &amp; link to the <a href=\"%1$s\">plugin page</a>."
"</p>"
msgstr ""

#: admin/partials/about-plugin-view.php:37
#, php-format
msgid ""
"<p>Check out our other <a href=\"%1$s\">WordPress plugins</a>.</p>"
msgstr ""

#: admin/partials/settings-view.php:8
msgid "Save Settings"
msgstr ""

#: fixed-html-toolbar.php:98
#, php-format
msgid ""
"You need to deactivate and delete the old <b>Fixed HTML Toolbar "
"(Free) plugin</b> on the plugins page. %sClick here to Deactivate it"
"%s."
msgstr ""

#: includes/plugin-update-checker/Puc/v4p6/Plugin/Ui.php:54
msgid "View details"
msgstr ""

#: includes/plugin-update-checker/Puc/v4p6/Plugin/Ui.php:77
#, php-format
msgid "More information about %s"
msgstr ""

#: includes/plugin-update-checker/Puc/v4p6/Plugin/Ui.php:128
msgid "Check for updates"
msgstr ""

#: includes/plugin-update-checker/Puc/v4p6/Plugin/Ui.php:223
#, php-format
msgid "Unknown update checker status \"%s\""
msgstr ""

#: includes/plugin-update-checker/Puc/v4p6/Vcs/PluginUpdateChecker.php:98
msgid "There is no changelog available."
msgstr ""

#: public/class-public.php:82
#, php-format
msgid "icon-%1$s"
msgstr ""

#: public/class-public.php:103
msgid "You can place your HTML code in the plugin settings."
msgstr ""
