<?php
/* ======================================================
 # Fixed HTML Toolbar for WordPress - v1.0.3 (free version)
 # -------------------------------------------------------
 # For WordPress
 # Author: Web357
 # Copyright @ 2014-2021 Web357. All rights reserved.
 # License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 # Website: https:/www.web357.com
 # Demo: https://demo.web357.com/wordpress/?page_id=17
 # Support: support@web357.com
 # Last modified: Wednesday 28 July 2021, 05:21:04 PM
 ========================================================= */
class FixedHtmlToolbar_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The options of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $options    The current options of this plugin.
	 */
	public $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->options = (object) get_option( 'fixed_html_toolbar_options' );
		$this->test = 'default';
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() 
	{
		if ($this->displayFixedHtmlToolbar() === false)
		{
			return;
		}

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/public.min.css', array(), $this->version, 'all' );
		wp_add_inline_style( 'fixed-html-toolbar', $this->cssCode() );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() 
	{
		if ($this->displayFixedHtmlToolbar() === false)
		{
			return;
		}

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/public.min.js', array( 'jquery' ), $this->version, false );
	}

	public function displayFixedHtmlToolbar()
	{
		// Show the Fixed HTML toolbar only in specific places.
		if (isset($this->options->location))
		{
			if (is_home() || is_front_page())
			{
				// Homepage
				$display = (in_array('homepage', $this->options->location)) ? true : false;
			}
			elseif (is_archive() && !is_category())
			{
				// Archives
				$display = (in_array('archives', $this->options->location)) ? true : false;
			}
			elseif (is_category())
			{
				// Categories
				$display = (in_array('categories', $this->options->location)) ? true : false;
			}
			elseif (is_search())
			{
				// Search
				$display = (in_array('search', $this->options->location)) ? true : false;
			}
			elseif (is_page())
			{
				// Pages
				$display = (in_array('pages', $this->options->location)) ? true : false;
			}
			elseif (is_single())
			{
				// Posts
				$display = (in_array('posts', $this->options->location)) ? true : false;
			}
			else
			{
				$display = false;
			}
		}
		else 
		{
			// display by default
			$display = true;
		}
		
		if ($display === true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function showFixedHtmlToolbar() 
	{
		if ($this->displayFixedHtmlToolbar() === false)
		{
			return;
		}
		
		?>
		<div class="fixed-html-toolbar <?php echo ($this->options->position == 'top' ? 'fixed-html-toolbar-top' : 'fixed-html-toolbar-bottom'); ?>">
			<div class="fixed-html-toolbar-inner">
				<div class="fixed-html-toolbar-content">

					<?php if ($this->options->type_of_toolbar_content == 'icons'): ?>
						<?php for ($i=1;$i<=5;$i++): ?>

							<?php 
							if (!empty($this->options->{'icon_'.$i})):
							$image_attributes = wp_get_attachment_image_src( $this->options->{'icon_'.$i}, 'full' );
							$image_source= $image_attributes[0];
							?>

								<?php if (!empty($this->options->{'link_icon_'.$i})): ?><a href="<?php echo esc_url($this->options->{'link_icon_'.$i}); ?>" target="_<?php echo $this->options->link_target; ?>"><?php endif; ?><img src="<?php echo esc_url($image_source); ?>" alt="<?php echo sprintf(esc_attr__( 'icon-%1$s', 'fixed-html-toolbar' ),$i); ?>" /><?php if (!empty($this->options->{'link_icon_'.$i})): ?></a><?php endif; ?>
		
							<?php endif; ?>	

						<?php endfor; ?>

					<?php elseif (!empty($this->options->htmlcode) && $this->options->type_of_toolbar_content == 'html'): ?>

						<?php 
						add_filter( 'the_content_fixed_html_toolbar', 'wptexturize'       );
						add_filter( 'the_content_fixed_html_toolbar', 'convert_smilies'   );
						add_filter( 'the_content_fixed_html_toolbar', 'convert_chars'     );
						add_filter( 'the_content_fixed_html_toolbar', 'wpautop'           );
						add_filter( 'the_content_fixed_html_toolbar', 'shortcode_unautop' );
						add_filter( 'the_content_fixed_html_toolbar', 'do_shortcode'      );
						$content = apply_filters( 'the_content_fixed_html_toolbar', $this->options->htmlcode);
						echo __($content, 'fixed-html-toolbar');
						?>
						
					<?php else: ?>

						<?php echo esc_html__('You can place your HTML code in the plugin settings.', 'fixed-html-toolbar'); ?>

					<?php endif; ?>

				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Custom CSS Style based on Settings
	 */
	public function cssCode()
	{
		$padding_left = ($this->options->alignment == 'left') ? 'padding-left: 15px' : 'padding-left: 0';
		$padding_right = ($this->options->alignment == 'right') ? 'padding-right: 15px' : 'padding-right: 0';

		// Styles
		$css = '';
		if ($this->options->position == 'top')
		{
			// Toolbar at the TOP of the page
			$css .= '.fixed-html-toolbar-inner { background: '.$this->options->background_color.'; border-bottom: '.$this->options->border_thickness.'px solid '.$this->options->border_color.'; }';
			$css .= '.fixed-html-toolbar-content { padding-top: '.intval($this->options->gap).'px; padding-bottom: '.intval($this->options->gap).'px; text-align: '.$this->options->alignment.';'.$padding_left.';'.$padding_right.'; }';
			$css .= '.fixed-html-toolbar-content img { margin-right: '.$this->options->icons_margin.'px; }';

			if (is_admin_bar_showing())
			{
				$css .= 'body { margin-top: '.((intval($this->options->gap) * 2) + 32).'px !important; }';
				$css .= '.fixed-html-toolbar-top { top: 32px!important; }';
			}
			else
			{
				$css .= 'body { margin-top: '.(intval($this->options->gap) * 2).'px !important; }';
			}
		}
		else
		{
			// Toolbar at the BOTTOM of the page
			$css .= 'body { margin-bottom: '.(intval($this->options->gap) * 2).'px !important; }';
			$css .= '.fixed-html-toolbar-inner { background: '.$this->options->background_color.'; border-top: '.$this->options->border_thickness.'px solid '.$this->options->border_color.'; }';
			$css .= '.fixed-html-toolbar-content { padding-top: '.intval($this->options->gap).'px; padding-bottom: '.intval($this->options->gap).'px; text-align: '.$this->options->alignment.';'.$padding_left.';'.$padding_right.'; }';
			$css .= '.fixed-html-toolbar-content img { margin-right: '.$this->options->icons_margin.'px; }';
		}

		// Custom CSS code from the plugin settings
		if (!empty($this->options->csscode))
		{
			$css .= $this->options->csscode;
		}

		return $css;
	}
}
