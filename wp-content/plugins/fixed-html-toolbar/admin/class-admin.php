<?php
/* ======================================================
 # Fixed HTML Toolbar for WordPress - v1.0.3 (free version)
 # -------------------------------------------------------
 # For WordPress
 # Author: Web357
 # Copyright @ 2014-2021 Web357. All rights reserved.
 # License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 # Website: https:/www.web357.com
 # Demo: https://demo.web357.com/wordpress/?page_id=17
 # Support: support@web357.com
 # Last modified: Wednesday 28 July 2021, 05:21:04 PM
 ========================================================= */
class FixedHtmlToolbar_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() 
	{
        wp_enqueue_style( 'wp-color-picker' ); 
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/admin.min.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() 
	{
		wp_enqueue_media();
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/admin.min.js', array( 'jquery', 'wp-color-picker' ), $this->version, false );

		// CSS editor
		$cm_settings['codeEditor'] = wp_enqueue_code_editor(array('type' => 'text/css'));
		wp_add_inline_script( $this->plugin_name, sprintf(
			'jQuery( function( $ ) {
				
				if ( $( ".w357-css-code-textarea-fixed-html-toolbar" ).length ) {
					wp.codeEditor.initialize($(".w357-css-code-textarea-fixed-html-toolbar"), %s);
				}

			} );',
			wp_json_encode( $cm_settings )
		) );
 		wp_enqueue_script('wp-theme-plugin-editor');
		wp_enqueue_style('wp-codemirror');
	}

	/**
	 * Adds the option in WordPress Admin menu
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	public function options_page() 
	{
		add_options_page( 
			esc_html__( 'Fixed HTML Toolbar settings', 'fixed-html-toolbar'),
			'Fixed HTML Toolbar',
			'manage_options', 
			'fixed-html-toolbar',
			array($this, 'options_page_content') 
		);
	}

	/**
	 * Adds the admin page content
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function options_page_content() 
	{
		$tabs = array(
			[
				'id'       => 'settings',
				'name'     => esc_html__( 'Settings', 'fixed-html-toolbar' ),
				'template' => 'settings-view.php',
			],
			[
				'id'       => 'about-plugin',
				'name'     => esc_html__( 'About Fixed HTML Toolbar', 'fixed-html-toolbar' ),
				'template' => 'about-plugin-view.php',
			],
			[
				'id'       => 'about-web357',
				'name'     => esc_html__( 'About Web357', 'fixed-html-toolbar' ),
				'template' => 'about-web357-view.php',
			],
		);

		include_once( 'partials/admin-view.php');
	}

	/**
	 * Function that will validate all fields.
	 */
	public function validateSettings( $fields ) 
	{ 
		$options = get_option( 'fixed_html_toolbar_options' );
		$valid_fields = array();

		// Validate "status" Field
		$status = trim( $fields['status'] );
		$status = strip_tags( stripslashes( $status ) );
		$status = ($status == 'published' || $status == 'unpublished') ? $status : 'published';
		$valid_fields['status'] = $status;

		// Validate "position" Field
		$position = trim( $fields['position'] );
		$position = strip_tags( stripslashes( $position ) );
		$position = ($position == 'top' || $position == 'bottom') ? $position : 'bottom';
		$valid_fields['position'] = $position;

		// Validate "location" Field
		$valid_fields['location'] = (isset($fields['location']) && is_array($fields['location'])) ? $fields['location'] : array();

		// Validate "license_key" Field
		$license_key = trim( $fields['license_key'] );
		$license_key = strip_tags( stripslashes( $license_key ) );
		$valid_fields['license_key'] = $license_key;

		// Validate "type_of_toolbar_content" Field
		$type_of_toolbar_content = trim( $fields['type_of_toolbar_content'] );
		$type_of_toolbar_content = strip_tags( stripslashes( $type_of_toolbar_content ) );
		$type_of_toolbar_content = ($type_of_toolbar_content == 'html' || $type_of_toolbar_content == 'icons') ? $type_of_toolbar_content : 'html';
		$valid_fields['type_of_toolbar_content'] = $type_of_toolbar_content;

		// Validate "htmlcode" Field
		$valid_fields['htmlcode'] = !empty( $fields['htmlcode'] ) ? $fields['htmlcode'] : 'Your HTML code goes here...';

		// Validate "icons" and "links"
		for ($i=1;$i<=5;$i++)
		{
			// icon
			${'icon_'.$i} = is_numeric( $fields['icon_'.$i] ) ? $fields['icon_'.$i] : '';
			$valid_fields['icon_'.$i] = ${'icon_'.$i};

			// link
			${'link_icon_'.$i} = $fields['link_icon_'.$i];
			if (!empty($fields['link_icon_'.$i]))
			{
				if( FALSE === wp_http_validate_url( $fields['link_icon_'.$i] ) ) 
				{
					// Set the error message
					add_settings_error( 'fixed-html-toolbar', 'fixed-html-toolbar_error', 'Please, insert a valid URL for the icon #'.$i, 'error' );
					// Get the previous valid value
					$valid_fields['link_icon_'.$i] = $options['link_icon_'.$i];
				} else {
					$valid_fields['link_icon_'.$i] = ${'link_icon_'.$i};
				}
			}
			else
			{
				$valid_fields['link_icon_'.$i] = '';
			}
		}

		// Validate "background_color" Field
		$background_color = trim( $fields['background_color'] );
		$background_color = strip_tags( stripslashes( $background_color ) );
		
		// Check if is a valid hex color
		if( FALSE === $this->check_color( $background_color ) ) 
		{
			// Set the error message
			add_settings_error( 'fixed-html-toolbar', 'fixed-html-toolbar_error', 'Please, insert a valid color for the Background Color field', 'error' );
			// Get the previous valid value
			$valid_fields['background_color'] = $options['background_color'];
		} else {
			$valid_fields['background_color'] = $background_color;  
		}

		// Validate "border_color" Field
		$border_color = trim( $fields['border_color'] );
		$border_color = strip_tags( stripslashes( $border_color ) );
		
		// Check if is a valid hex color
		if( FALSE === $this->check_color( $border_color ) ) 
		{
			// Set the error message
			add_settings_error( 'fixed-html-toolbar', 'fixed-html-toolbar_error', 'Please, insert a valid color for the Border Color field', 'error' );
			// Get the previous valid value
			$valid_fields['border_color'] = $options['border_color'];
		} else {
			$valid_fields['border_color'] = $border_color;  
		}

		// Validate "border_thickness" Field
		$border_thickness = is_numeric( $fields['border_thickness'] ) ? $fields['border_thickness'] : 1;
		$valid_fields['border_thickness'] = $border_thickness;

		// Validate "gap" Field
		$gap = is_numeric( $fields['gap'] ) ? $fields['gap'] : 1;
		$valid_fields['gap'] = $gap;

		// Validate "alignment" Field
		$alignment = trim( $fields['alignment'] );
		$alignment = strip_tags( stripslashes( $alignment ) );
		$alignment = ($alignment == 'left' || $alignment == 'center' || $alignment == 'right') ? $alignment : 'center';
		$valid_fields['alignment'] = $alignment;

		// Validate "link_target" Field
		$link_target = trim( $fields['link_target'] );
		$link_target = strip_tags( stripslashes( $link_target ) );
		$link_target = ($link_target == 'parent' || $link_target == 'blank') ? $link_target : 'parent';
		$valid_fields['link_target'] = $link_target;

		// Validate "icons_margin" Field
		$icons_margin = is_numeric( $fields['icons_margin'] ) ? $fields['icons_margin'] : 15;
		$valid_fields['icons_margin'] = $icons_margin;

		// Validate "csscode" Field
		$valid_fields['csscode'] = !empty( $fields['csscode'] ) ? $fields['csscode'] : '';

		return apply_filters( 'validateSettings', $valid_fields, $fields);
	}
	
	/**
	 * Function that will check if value is a valid HEX color.
	 */
	public function check_color( $value ) { 
		
		if ( preg_match( '/^#[a-f0-9]{6}$/i', $value ) ) { // if user insert a HEX color with #     
			return true;
		}
		
		return false;
	}

	/**
	 * Initialize the settings link
	 *
	 * @access   public
	 */
	public function settings_link($links) 
	{
		$link = 'options-general.php?page=fixed-html-toolbar';
		$settings_link = '<a href="'.esc_url($link).'">'.esc_html__( 'Settings', 'fixed-html-toolbar' ).'</a>';
		array_push( $links, $settings_link );
		return $links;
	}

	/**
	 * Initialize the settings page
	 *
	 * @since    3.2.0
	 * @access   public
	 */
	public function settings_init() 
	{
		/**
		 * REGISTER SETTINGS
		 */
		register_setting( 'fixed-html-toolbar', 'fixed_html_toolbar_options', array($this, 'validateSettings'));

		/**
		 * SECTIONS
		 */
		add_settings_section(
			'base_settings_section', 
			'', 
			array($this, 'base_settings_section_callbacack'),
			'fixed-html-toolbar'
		);
		add_settings_section(
			'toolbar_content_section', 
			'',
			array($this, 'toolbar_content_section_callbacack'),
			'fixed-html-toolbar'
		);
		add_settings_section(
			'styling_options_section', 
			'', 
			array($this, 'styling_options_section_callbacack'),
			'fixed-html-toolbar'
		);

		/**
		 * Define Vars
		 */
		// Type of the Toolbar Content
		$options = get_option( 'fixed_html_toolbar_options' );
		if (isset($options['type_of_toolbar_content']))
		{
			$type_of_toolbar_content = $options['type_of_toolbar_content'];
		}
		else
		{
			$type_of_toolbar_content = 'html'; // default
		}

		/**
		 * FIELDS
		 */
		// Status
		add_settings_field( 
			'status', 
			esc_html__( 'Status', 'fixed-html-toolbar' ), 
			array($this, 'radioField'),
			'fixed-html-toolbar', 
			'base_settings_section',
			[
				'id' => 'status',
				'default_value' => 'published',
				'options' => [
					['id' => 'published', 'label' => esc_html__( 'Published', 'fixed-html-toolbar' ), 'value' => 'published'],
					['id' => 'unpublished', 'label' => esc_html__( 'Unpublished', 'fixed-html-toolbar' ), 'value' => 'unpublished',],
				],
				'field_description' => 'Show or hide the HTML toolbar.'
			]
		);
		
		// Position
		add_settings_field( 
			'position', 
			esc_html__( 'Position', 'fixed-html-toolbar' ),
			array($this, 'radioField'),
			'fixed-html-toolbar', 
			'base_settings_section',
			[
				'id' => 'position',
				'default_value' => 'bottom',
				'options' => [
					['id' => 'position_bottom', 'label' => esc_html__( 'Bottom', 'fixed-html-toolbar' ), 'value' => 'bottom'],
					['id' => 'position_top', 'label' => esc_html__( 'Top', 'fixed-html-toolbar' ), 'value' => 'top',],
				]
			]
		);

		// Location
		add_settings_field( 
			'location', 
			esc_html__( 'Location', 'fixed-html-toolbar' ),
			array($this, 'checkboxField'),
			'fixed-html-toolbar', 
			'base_settings_section',
			[
				'id' => 'location',
				'default_value' => array('homepage', 'pages', 'posts', 'archives', 'categories', 'search'),
				'options' => [
					['id' => 'location_show_on_homepage', 'label' => esc_html__( 'Homepage', 'fixed-html-toolbar' ), 'value' => 'homepage'],
					['id' => 'location_show_on_pages', 'label' => esc_html__( 'Pages', 'fixed-html-toolbar' ), 'value' => 'pages'],
					['id' => 'location_show_on_posts', 'label' => esc_html__( 'Posts', 'fixed-html-toolbar' ), 'value' => 'posts'],
					['id' => 'location_show_on_archives', 'label' => esc_html__( 'Archives', 'fixed-html-toolbar' ), 'value' => 'archives'],
					['id' => 'location_show_on_categories', 'label' => esc_html__( 'Categories', 'fixed-html-toolbar' ), 'value' => 'categories'],
					['id' => 'location_show_on_search', 'label' => esc_html__( 'Search', 'fixed-html-toolbar' ), 'value' => 'search'],
				],
				'field_description' => 'Show the Fixed HTML toolbar only in specific places.'
			]
		);
		
		// License Key
		add_settings_field( 
			'license_key', 
			esc_html__( 'License Key', 'fixed-html-toolbar' )
			. ' <span class="w357-tooltip w357-tooltip-bottom" data-tip="In order to update commercial Web357 plugins, you have to enter your License key. The key can be found in your account details at web357.com."><div class="dashicons dashicons-info w357-dashicon-info"></div></span>',
			
			array($this, 'hiddenField'),
			'fixed-html-toolbar', 
			'base_settings_section',
			[
				'label-for' => 'license_key',
				'name' => 'license_key',
				
				'class' => 'license_key hidden',
				'default_value' => '',
				'size' => 60,
				'maxlength' => 60,
				'placeholder' => 'Enter your license key from web357.com',
			]
		);

		// Type of Toolbar Content
		add_settings_field( 
			'type_of_toolbar_content', 
			esc_html__( 'Type of Toolbar Content', 'fixed-html-toolbar' ),
			array($this, 'radioField'),
			'fixed-html-toolbar', 
			'toolbar_content_section',
			[
				'id' => 'type_of_toolbar_content',
				'default_value' => 'html',
				'options' => [
					['id' => 'type_of_toolbar_content_html', 'label' => esc_html__( 'HTML', 'fixed-html-toolbar' ), 'value' => 'html'],
					['id' => 'type_of_toolbar_content_icons', 'label' => esc_html__( 'Icons', 'fixed-html-toolbar' ), 'value' => 'icons'],
				],
				'field_description' => ''
			]
		);

		// Add HTML into the Toolbar
		add_settings_field( 
			'htmlcode', 
			esc_html__( 'Add HTML into the Toolbar', 'fixed-html-toolbar' ), 
			array($this, 'textareaWordpressEditorField'),
			'fixed-html-toolbar', 
			'toolbar_content_section',
			[
				'label-for' => 'htmlcode',
				'class' => 'htmlcode '.($type_of_toolbar_content=='html'?'fixed-html-toolbar-show':'fixed-html-toolbar-hide'),
				'_class' => 'w357-html-code-textarea',
				'name' => 'htmlcode',
				'cols' => 50,
				'rows' => 10,
				'default_value' => 'Your HTML code goes here...',
			]
		);

		// Images and Links
		for ($i=1;$i<=5;$i++)
		{
			// Image
			add_settings_field( 
				'icon_'.$i, 
				esc_html__( 'Icon', 'fixed-html-toolbar' ) . ' #'.$i,
				array($this, 'imageField'), 
				'fixed-html-toolbar', 
				'toolbar_content_section',
				[
					'id' => 'icon_'.$i,
					'class' => 'icons '.($type_of_toolbar_content=='icons'?'fixed-html-toolbar-show':'fixed-html-toolbar-hide'),
					'width' => 64,
					'height' => 64,
					'img_id' => 'img_id_'.$i
				]
			);
			
			// Link
			add_settings_field( 
				'link_icon_'.$i, 
				esc_html__( 'Link for the icon', 'fixed-html-toolbar' ) . ' #'.$i,
				array($this, 'textField'),
				'fixed-html-toolbar', 
				'toolbar_content_section',
				[
					'label-for' => 'link_icon_'.$i,
					'name' => 'link_icon_'.$i,
					'class' => 'link_icons '.($type_of_toolbar_content=='icons'?'fixed-html-toolbar-show':'fixed-html-toolbar-hide'),
					'default_value' => '',
					'size' => 60,
					'maxlength' => 3,
					'placeholder' => 'https://your-link-for-the-icon-'.$i.'-goes-here',
				]
			);
		}

		// Background Color
		add_settings_field( 
			'background_color', 
			esc_html__( 'Background Color', 'fixed-html-toolbar' ), 
			array($this, 'textField'),
			'fixed-html-toolbar', 
			'styling_options_section',
			[
				'label-for' => 'background_color',
				'name' => 'background_color',
				'_class' => 'color-field',
				'default_value' => '#e5e5e5',
			]
		);

		// Border Color
		add_settings_field( 
			'border_color', 
			esc_html__( 'Border Color', 'fixed-html-toolbar' ), 
			array($this, 'textField'),
			'fixed-html-toolbar', 
			'styling_options_section',
			[
				'label-for' => 'border_color',
				'name' => 'border_color',
				'_class' => 'color-field',
				'default_value' => '#b5b5b5',
			]
		);

		// Border thickness
		add_settings_field( 
			'border_thickness', 
			esc_html__( 'Border thickness', 'fixed-html-toolbar' ), 
			array($this, 'selectField'),
			'fixed-html-toolbar', 
			'styling_options_section',
			[
				'id' => 'border_thickness',
				'default_value' => '1',
				'options' => [
					['id' => '0', 'label' => 'none', 'value' => '0'],
					['id' => '1', 'label' => '1px'.' ('.esc_html__( 'recommended', 'fixed-html-toolbar' ).')', 'value' => '1'],
					['id' => '2', 'label' => '2px', 'value' => '2'],
					['id' => '3', 'label' => '3px', 'value' => '3'],
					['id' => '4', 'label' => '4px', 'value' => '4'],
					['id' => '5', 'label' => '5px', 'value' => '5'],
					['id' => '6', 'label' => '6px', 'value' => '6'],
					['id' => '7', 'label' => '7px', 'value' => '7'],
					['id' => '8', 'label' => '8px', 'value' => '8'],
					['id' => '9', 'label' => '9px', 'value' => '9'],
					['id' => '10', 'label' => '10px', 'value' => '10'],
				]
			]
		);

		// Gap
		$gap_options = array();
		for ($i=0;$i<=150;$i++)
		{
			$gap_options[] = ['id' => $i, 'label' => esc_html__( $i.'px'.($i==15?' (recommended)' : ''), 'fixed-html-toolbar' ), 'value' => $i];
		}
		add_settings_field( 
			'gap', 
			esc_html__( 'Gap (the vertical padding)', 'fixed-html-toolbar' ), 
			array($this, 'selectField'),
			'fixed-html-toolbar', 
			'styling_options_section',
			[
				'id' => 'gap',
				'default_value' => '15',
				'options' => $gap_options
			]
		);

		// Alignment
		add_settings_field( 
			'alignment', 
			esc_html__( 'Text Alignment', 'fixed-html-toolbar' ), 
			array($this, 'radioField'),
			'fixed-html-toolbar', 
			'styling_options_section',
			[
				'id' => 'alignment',
				'default_value' => 'center',
				'options' => [
					['id' => 'alignment_left', 'label' => esc_html__( 'Left', 'fixed-html-toolbar' ), 'value' => 'left'],
					['id' => 'alignment_center', 'label' => esc_html__( 'Center', 'fixed-html-toolbar' ), 'value' => 'center'],
					['id' => 'alignment_right', 'label' => esc_html__( 'Right', 'fixed-html-toolbar' ), 'value' => 'right'],
				]
			]
		);

		// Link Target
		add_settings_field( 
			'link_target', 
			esc_html__( 'Link Target', 'fixed-html-toolbar' ), 
			array($this, 'radioField'),
			'fixed-html-toolbar', 
			'styling_options_section',
			[
				'id' => 'link_target',
				'class' => 'link_target '.($type_of_toolbar_content=='icons'?'fixed-html-toolbar-show':'fixed-html-toolbar-hide'),
				'default_value' => 'parent',
				'options' => [
					['id' => 'link_target_parent', 'label' => esc_html__( 'Open in the same window', 'fixed-html-toolbar' ), 'value' => 'parent'],
					['id' => 'link_target_blank', 'label' => esc_html__( 'Open in a new window', 'fixed-html-toolbar' ), 'value' => 'blank'],
				]
			]
		);

		// Margin between icons
		add_settings_field( 
			'icons_margin', 
			esc_html__( 'Margin between icons', 'fixed-html-toolbar' ), 
			array($this, 'selectField'),
			'fixed-html-toolbar', 
			'styling_options_section',
			[
				'id' => 'icons_margin',
				'class' => 'icons_margin '.($type_of_toolbar_content=='icons'?'fixed-html-toolbar-show':'fixed-html-toolbar-hide'),
				'default_value' => '15',
				'options' => [
					['id' => '0', 'label' => '0px', 'value' => '0'],
					['id' => '5', 'label' => '5px', 'value' => '5'],
					['id' => '10', 'label' => '10px ('.esc_html__( 'recommended', 'fixed-html-toolbar' ).')', 'value' => '10'],
					['id' => '15', 'label' => '15px', 'value' => '15'],
					['id' => '20', 'label' => '20px', 'value' => '20'],
					['id' => '25', 'label' => '25px', 'value' => '25'],
					['id' => '30', 'label' => '30px', 'value' => '30'],
					['id' => '35', 'label' => '35px', 'value' => '35'],
					['id' => '40', 'label' => '40px', 'value' => '40'],
					['id' => '45', 'label' => '45px', 'value' => '45'],
					['id' => '50', 'label' => '50px', 'value' => '50'],
				]
			]
		);

		// Add Custom CSS Code
		add_settings_field( 
			'csscode', 
			esc_html__( 'Add Custom CSS Code', 'fixed-html-toolbar' ), 
			array($this, 'textareaField'),
			'fixed-html-toolbar', 
			'styling_options_section',
			[
				'label-for' => 'csscode',
				'class' => 'csscode',
				'_class' => 'w357-css-code-textarea-fixed-html-toolbar',
				'name' => 'csscode',
				'cols' => 50,
				'rows' => 10,
				'placeholder' => "",
				'default_value' => "",
			]
		);
	}

	function imageField($args) 
	{ 
		$options = get_option( 'fixed_html_toolbar_options' );
		$name = $args['id'];
		$width = $args['width'];
		$height = $args['height'];
		$img_id = $args['img_id'];
		$default_image = '';

		// Set variables
		if ( !empty( $options[$name] ) ) {
			$image_attributes = wp_get_attachment_image_src( $options[$name], array( $width, $height ) );
			$src = $image_attributes[0];
			$value = $options[$name];
		} else {
			$src = $default_image;
			$value = '';
		}
		?>

		<div class="w357-imageField">

			<?php if (!empty($src)): ?>
					<img data-src="<?php echo esc_url($default_image); ?>" src="<?php echo esc_url($src); ?>" width="<?php echo absint($width); ?>px" height="<?php echo absint($height); ?>px" />		
			<?php else: ?>
				<img data-src="<?php echo esc_url($default_image); ?>" src="<?php echo esc_url($src); ?>" width="<?php echo absint($width); ?>px" height="<?php echo absint($height); ?>px" style="display:none" />		
			<?php endif; ?>

			<div>
				<input type="hidden" name="fixed_html_toolbar_options[<?php echo $name; ?>]" id="fixed_html_toolbar_options[<?php echo $name; ?>]" value="<?php echo esc_attr($value); ?>" />
				<button type="submit" class="upload_image_button button">Upload image</button>

				<?php if (!empty($src)): ?>
					<button type="submit" class="remove_image_button button">&times;</button>
				<?php else: ?>
					<button type="submit" class="remove_image_button button" style="display:none">&times;</button>
				<?php endif; ?>

			</div>
		</div>
		<?php
	}

	function textField($args) 
	{ 
		$options = get_option('fixed_html_toolbar_options');
		$class = (isset($args['_class'])) ? $args['_class'] : '';
		$placeholder = (isset($args['placeholder'])) ? $args['placeholder'] : '';
		$size = (isset($args['size'])) ? $args['size'] : 10;
		$maxlength = (isset($args['maxlength'])) ? $args['maxlength'] : 50;
		$default_value = (isset($args['default_value'])) ? $args['default_value'] : '';
		?>
		<input 
			type='text' 
			name='fixed_html_toolbar_options[<?php echo esc_attr($args['name']); ?>]' 
			id='<?php echo esc_attr($args['label-for']); ?>' 
			class='<?php echo esc_attr($class); ?>' 
			placeholder='<?php echo esc_html__($placeholder); ?>'
			value='<?php echo esc_attr(isset($options[$args['name']]) ? $options[$args['name']] : $default_value); ?>'
			size='<?php echo absint($size); ?>'
			size='<?php echo absint($maxlength); ?>'
			>
		<?php
	}

	function hiddenField($args) 
	{ 
		$options = get_option('fixed_html_toolbar_options');
		$default_value = (isset($args['default_value'])) ? $args['default_value'] : '';
		?>
		<input 
			type='hidden' 
			name='fixed_html_toolbar_options[<?php echo esc_attr($args['name']); ?>]' 
			value='<?php echo esc_attr(isset($options[$args['name']]) ? $options[$args['name']] : $default_value); ?>'
			>
		<?php
	}

	function textareaWordpressEditorField($args) 
	{ 
		$options = get_option('fixed_html_toolbar_options');
	    $editor_id = $args['name']; 
		$class = (isset($args['_class'])) ? $args['_class'] : '';
		$editor_settings = array('textarea_name' => 'fixed_html_toolbar_options['.$args['name'].']', 'editor_class' => $class);
		$default_value = (isset($args['default_value'])) ? $args['default_value'] : '';
		$content = (isset($options[$args['name']])) ? $options[$args['name']] : $default_value;
		wp_editor( $content, $editor_id, $editor_settings );
	}

	function textareaField($args) 
	{ 
		$options = get_option('fixed_html_toolbar_options');
		$class = (isset($args['_class'])) ? $args['_class'] : '';
		$default_value = (isset($args['default_value'])) ? $args['default_value'] : '';
		?>
		
		<textarea 
			id="<?php echo esc_attr($args['name']); ?>" 
			name="fixed_html_toolbar_options[<?php echo esc_attr($args['name']); ?>]" 
			rows="<?php echo absint($args['rows']); ?>" 
			cols="<?php echo absint($args['cols']); ?>" 
			class="<?php echo esc_attr($class); ?>"
			placeholder="<?php echo esc_html__($args['placeholder']); ?>"><?php echo esc_textarea(isset($options[$args['name']]) && !empty($options[$args['name']]) ? $options[$args['name']] : $default_value); ?></textarea>
		<?php
	}

	function selectField($args)
	{ 
		$name = $args['id'];
		$default_value = $args['default_value'];
		$select_options = $args['options'];
		$options = get_option('fixed_html_toolbar_options');

		?>
		<select name="fixed_html_toolbar_options[<?php echo $name; ?>]">

		<?php for ($i=0;$i<count($select_options);$i++): ?>

			<option value="<?php echo esc_attr($select_options[$i]['value']); ?>" <?php echo (($select_options[$i]['value'] == (isset($options[$name]) ? $options[$name] : $default_value) ) ? 'selected' : ''); ?>><?php echo $select_options[$i]['label']; ?></option>

		<?php endfor; ?>
		</select>
		<?php
	}

	function radioField($args)
	{ 
		$name = $args['id'];
		$default_value = $args['default_value'];
		$radio_options = $args['options'];
		$field_description = (isset($args['field_description'])) ? $args['field_description'] : '';
		$options = get_option('fixed_html_toolbar_options');

		for ($i=0;$i<count($radio_options);$i++): ?>

			<input 
				type='radio' 
				id='<?php echo $radio_options[$i]['id']; ?>' 
				name='fixed_html_toolbar_options[<?php echo $name; ?>]' 
				value='<?php echo esc_attr($radio_options[$i]['value']); ?>'
				<?php if ( $radio_options[$i]['value'] == (isset($options[$name]) ? $options[$name] : $default_value) ) echo 'checked="checked"'; ?>
			>
			<label for="<?php echo $radio_options[$i]['id']; ?>" style="margin-right: 10px;"><?php echo $radio_options[$i]['label']; ?></label>

		<?php endfor; ?>

		<?php if (!empty($field_description)): ?>
			<div class="w357_settings_field_description"><?php echo $field_description; ?></div>
		<?php endif; ?>
		<?php
	}

	function checkboxField($args)
	{
		$name = $args['id'];
		$default_value = $args['default_value'];
		$ckeckbox_options = $args['options'];
		$field_description = (isset($args['field_description'])) ? $args['field_description'] : '';
		$options = get_option('fixed_html_toolbar_options');

		for ($i=0;$i<count($ckeckbox_options);$i++):
		?>

			<input 
				type='checkbox' 
				id='<?php echo $ckeckbox_options[$i]['id']; ?>' 
				name='fixed_html_toolbar_options[<?php echo $name; ?>][]' 
				value='<?php echo esc_attr($ckeckbox_options[$i]['value']); ?>'
				<?php if (in_array($ckeckbox_options[$i]['value'], (isset($options[$name]) ? $options[$name] : $default_value))) echo 'checked="checked"'; ?>
			>
			<label for="<?php echo $ckeckbox_options[$i]['id']; ?>" style="margin-right: 10px;"><?php echo $ckeckbox_options[$i]['label']; ?></label>

		<?php endfor; ?>

		<?php if (!empty($field_description)): ?>
			<div class="w357_settings_field_description"><?php echo $field_description; ?></div>
		<?php endif; ?>
		<?php
	}

	function base_settings_section_callbacack() 
	{ 
		echo '<div class="w357_settings_main_heading">'.esc_html__( 'Settings', 'fixed-html-toolbar' ).'</div>';
		echo '<div class="w357_settings_section_heading">'.esc_html__( 'Base Settings', 'fixed-html-toolbar' ).'</div>';
	}

	function toolbar_content_section_callbacack() 
	{ 
		echo '<div class="w357_settings_section_heading">'.esc_html__( 'Toolbar\'s Content', 'fixed-html-toolbar' ).'</div>';
	}

	function styling_options_section_callbacack() 
	{ 
		echo '<div class="w357_settings_section_heading">'.esc_html__( 'Styling Options', 'fixed-html-toolbar' ).'</div>';
	}
}