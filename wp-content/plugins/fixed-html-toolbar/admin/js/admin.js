/* ======================================================
 # Fixed HTML Toolbar for WordPress - v1.0.3 (free version)
 # -------------------------------------------------------
 # For WordPress
 # Author: Web357
 # Copyright @ 2014-2021 Web357. All rights reserved.
 # License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 # Website: https:/www.web357.com
 # Demo: https://demo.web357.com/wordpress/?page_id=17
 # Support: support@web357.com
 # Last modified: Wednesday 28 July 2021, 05:21:04 PM
 ========================================================= */
jQuery(function ($) {

	'use strict';

	$(window).load(function () {

		// The "Upload" button
		$('.upload_image_button').click(function () {
			var send_attachment_bkp = wp.media.editor.send.attachment;
			var button = $(this);
			wp.media.editor.send.attachment = function (props, attachment) {
				$(button).parent().prev().show();
				$(button).next().show();
				$(button).parent().prev().attr('src', attachment.url);
				$(button).prev().val(attachment.id);
				wp.media.editor.send.attachment = send_attachment_bkp;
			}
			wp.media.editor.open(button);
			return false;
		});

		// The "Remove" button (remove the value from input type='hidden')
		$('.remove_image_button').click(function () {
			var answer = confirm('Are you sure?');
			if (answer == true) {
				var src = $(this).parent().prev().attr('data-src');
				$(this).parent().prev().attr('src', src);
				$(this).prev().prev().val('');
				$(this).parent().prev().hide();
				$(this).hide();
			}
			return false;
		});

		// Type of Toolbar Content: Show Icon fields
		$('#type_of_toolbar_content_icons').click(function () {
			$('.htmlcode').hide();
			$('.icons').show();
			$('.link_icons').show();
			$('.link_target').show();
			$('.icons_margin').show();
		});

		// Type of Toolbar Content: Show HTML textarea field
		$('#type_of_toolbar_content_html').click(function () {
			$('.htmlcode').show();
			$('.icons').hide();
			$('.link_icons').hide();
			$('.link_target').hide();
			$('.icons_margin').hide();
		});

		// Add Color Picker to all inputs that have 'color-field' class
		$(function () {
			$('.color-field').wpColorPicker();
		});

	});

});