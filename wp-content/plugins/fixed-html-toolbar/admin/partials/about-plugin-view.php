<?php
/* ======================================================
 # Fixed HTML Toolbar for WordPress - v1.0.3 (free version)
 # -------------------------------------------------------
 # For WordPress
 # Author: Web357
 # Copyright @ 2014-2021 Web357. All rights reserved.
 # License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 # Website: https:/www.web357.com
 # Demo: https://demo.web357.com/wordpress/?page_id=17
 # Support: support@web357.com
 # Last modified: Wednesday 28 July 2021, 05:21:04 PM
 ========================================================= */
// About Plugin page in Settings
?>
<div class="wrap">

    <div style="margin-top: 20px;">
        <h3><?php echo esc_html__( 'Description', 'fixed-html-toolbar'); ?></h3>
        <p>A fixed HTML toolbar which displayed at the bottom or at the top of your website. You can add up to 5 linked icons or just an HTML code. It mostly used to show social icons or any other notification that needs to be static during page scrolling.</p>
    </div>

    <div style="margin-top: 30px;">
        <h3><?php echo esc_html__( 'Need support?', 'fixed-html-toolbar'); ?></h3>
        <?php
        echo sprintf(
            __( '<p>If you are having problems with this plugin, please browse it\'s <a href="%1$s">Documentation</a> or <a href="%2$s">Contact us</a></p>', 'fixed-html-toolbar' ),
            esc_url('https://www.web357.com/support'),
            esc_url( 'https://www.web357.com/contact' )
        );
        ?>
    </div>
    
    <div style="margin-top: 30px;">
        <h3><?php echo esc_html__( 'Do you like this plugin?', 'fixed-html-toolbar'); ?></h3>

        <?php
        echo sprintf(
            __( '<p><a href="%1$s">Rate it 5</a> on WordPress.org</p>', 'fixed-html-toolbar' ),
            esc_url('https://wordpress.org/plugins/fixed-html-toolbar/')
        );
  
        echo sprintf(
            __( '<p>Blog about it &amp; link to the <a href="%1$s">plugin page</a>.</p>', 'fixed-html-toolbar' ),
            esc_url('https://www.web357.com/product/fixed-html-toolbar-wordpress-plugin')
        );

        echo sprintf(
            __( '<p>Check out our other <a href="%1$s">WordPress plugins</a>.</p>', 'fixed-html-toolbar' ),
            esc_url('https://www.web357.com/products/wordpress-plugins')
        );
        ?>
    </div>

</div>