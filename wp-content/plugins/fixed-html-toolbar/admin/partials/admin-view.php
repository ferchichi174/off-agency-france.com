<?php
/* ======================================================
 # Fixed HTML Toolbar for WordPress - v1.0.3 (free version)
 # -------------------------------------------------------
 # For WordPress
 # Author: Web357
 # Copyright @ 2014-2021 Web357. All rights reserved.
 # License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 # Website: https:/www.web357.com
 # Demo: https://demo.web357.com/wordpress/?page_id=17
 # Support: support@web357.com
 # Last modified: Wednesday 28 July 2021, 05:21:04 PM
 ========================================================= */
/**
 * The view for admin page
 *
 */
if ( ! current_user_can( 'manage_options' ) ) {
	return;
}

$active_tab = isset( $_GET['tab'] ) ? sanitize_text_field($_GET['tab']) : sanitize_text_field($tabs[0]['id']);

?>
	<div class="wrap fixed-html-toolbar-plugin">
		<h1>Fixed HTML Toolbar v1.0.3</h1>
		<h2 class="nav-tab-wrapper">
			<?php foreach ( $tabs as $tab ) {
				$is_active = $tab['id'] === $active_tab; 
				$url = '?page=fixed-html-toolbar&tab='.$tab['id'];
				?>
				<a href="<?php echo esc_url($url); ?>"
				   class="nav-tab <?php echo $is_active ? 'nav-tab-active' : ''; ?>">
					<?php echo $tab['name']; ?>
				</a>
			<?php } ?>
		</h2>
		<?php foreach ( $tabs as $tab ) {
			$is_active = $tab['id'] === $active_tab;
			if ( $is_active ) {
				include_once( $tab['template'] );
			}
		} ?>
	</div>
<?php
