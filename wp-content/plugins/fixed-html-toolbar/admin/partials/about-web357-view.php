<?php
/* ======================================================
 # Fixed HTML Toolbar for WordPress - v1.0.3 (free version)
 # -------------------------------------------------------
 # For WordPress
 # Author: Web357
 # Copyright @ 2014-2021 Web357. All rights reserved.
 # License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 # Website: https:/www.web357.com
 # Demo: https://demo.web357.com/wordpress/?page_id=17
 # Support: support@web357.com
 # Last modified: Wednesday 28 July 2021, 05:21:04 PM
 ========================================================= */
// About Web357 page in Settings
$html  = '<div class="about-web357" style="margin-top: 30px;">';

$plugin_dir_path = esc_url( plugins_url( 'img', dirname(__FILE__) ) );

// About
$web357_link = 'https://www.web357.com/?utm_source=CLIENT&utm_medium=CLIENT-AboutUsLink-web357&utm_content=CLIENT-AboutUsLink&utm_campaign=aboutelement';

$html .= '<a href="'.esc_url($web357_link).'" target="_blank"><img src="'.esc_url($plugin_dir_path).'/web357-logo.png" alt="Web357 logo" style="float: left; margin-right: 20px;" /></a>';

$html .= "<p>We are a young team of professionals and internet lovers who specialise in the development of professional websites and useful plugins for WordPress and Joomla!. We pride ourselves on providing expertise via our talented and skillful team. We are passionate of our work and that is what makes us stand out in our goal to improve WordPress and Joomla! websites by providing better user interface, increasing performance, efficiency and security.</p><p>Our Web357 team carries years of experience in web design and development especially with Joomla! and WordPress platforms. As a result we decided to put together our expertise and eventually Web357 was born. We are proud to be able to contribute to the WordPress and Joomla! world by delivering the smartest and most cost efficient solutions for the web.</p><p>Our products focus on extending WordPress and Joomla's functionality and making repetitive tasks easier, safer and faster. Our source code is completely open (not encoded or encrypted), giving you the maximum flexibility to either modify it yourself or through our consultants.</p><p>We believe in strong long-term relationships with our clients and our working ethic strives for delivering high standard of products and customer support. All our extensions are being regularly updated and improved based on our customers' feedback and new web trends. In addition, Web357 supports personal customisations, as well as we provide assistance and guidance to our clients' individual requirements</p><p>Whether you are thinking of using our expertise for the first time or you are an existing client, we are here to help.</p><p>Web357 Team<br><a href=\"".esc_url($web357_link)."\" target=\"_blank\">www.web357.com</a></p>";

$html .= '</div>';

// BEGIN: Social sharing buttons
$html .= '<h4>Stay connected!</h4>';

$social_icons_dir_path = $plugin_dir_path.'/social-icons';
$social_sharing_buttons  = '<div class="web357-about-social-icons">'; // https://www.iconfinder.com/icons/252077/tweet_twitter_icon#size=32
        
// facebook
$social_sharing_buttons .= '<a href="'.esc_url('https://www.facebook.com/web357eu').'" target="_blank" title="Like us on Facebook"><img src="'.esc_url($social_icons_dir_path).'/facebook.png" alt="Facebook" /></a>';

// twitter
$social_sharing_buttons .= '<a href="'.esc_url('https://twitter.com/web357').'" target="_blank" title="Follow us on Twitter"><img src="'.esc_url($social_icons_dir_path).'/twitter.png" alt="Twitter" /></a>';

// youtube
$social_sharing_buttons .= '<a href="'.esc_url('https://www.youtube.com/channel/UC-yYIuMfdE-NKwZVs19Fmrg').'" target="_blank" title="Follow us on Youtube"><img src="'.esc_url($social_icons_dir_path).'/youtube.png" alt="Youtube" /></a>';

// newsletter
$social_sharing_buttons .= '<a href="'.esc_url('https://www.web357.com/newsletter').'" target="_blank" title="Subscribe to our Newsletter"><img src="'.esc_url($social_icons_dir_path).'/newsletter.png" alt="Newsletter" /></a>';

$social_sharing_buttons .= '</div>';

$html .= $social_sharing_buttons;
// END: Social sharing buttons

echo $html;