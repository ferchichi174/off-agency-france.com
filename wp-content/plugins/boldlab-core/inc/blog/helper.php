<?php

if ( ! function_exists( 'boldlab_core_include_blog_shortcodes' ) ) {
	/**
	 * Function that includes shortcodes
	 */
	function boldlab_core_include_blog_shortcodes() {
		foreach ( glob( BOLDLAB_CORE_INC_PATH . '/blog/shortcodes/*/include.php' ) as $shortcode ) {
			include_once $shortcode;
		}
	}

	add_action( 'qode_framework_action_before_shortcodes_register', 'boldlab_core_include_blog_shortcodes' );
}

if ( ! function_exists( 'boldlab_core_include_blog_shortcodes_widget' ) ) {
	/**
	 * Function that includes widgets
	 */
	function boldlab_core_include_blog_shortcodes_widget() {
		foreach ( glob( BOLDLAB_CORE_INC_PATH . '/blog/shortcodes/*/widget/include.php' ) as $widget ) {
			include_once $widget;
		}
	}

	add_action( 'qode_framework_action_before_widgets_register', 'boldlab_core_include_blog_shortcodes_widget' );
}

if ( ! function_exists( 'boldlab_core_set_blog_single_page_title' ) ) {
	/**
	 * Function that enable/disable page title area for blog single page
	 *
	 * @param $enable_page_title bool
	 *
	 * @return bool
	 */
	function boldlab_core_set_blog_single_page_title( $enable_page_title ) {

		if ( is_singular( 'post' ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_blog_single_enable_page_title' ) !== 'no';

			if ( isset ( $option ) ) {
				$enable_page_title = $option;
			}

			$meta_option = get_post_meta( get_the_ID(), 'qodef_enable_page_title', true );

			if ( ! empty( $meta_option ) ) {
				$enable_page_title = $meta_option;
			}
		}

		return $enable_page_title;
	}

	add_filter( 'boldlab_filter_enable_page_title', 'boldlab_core_set_blog_single_page_title' );
}

if ( ! function_exists( 'boldlab_core_set_blog_single_sidebar_layout' ) ) {
	/**
	 * Function that return sidebar layout
	 *
	 * @param $layout string
	 *
	 * @return string
	 */
	function boldlab_core_set_blog_single_sidebar_layout( $layout ) {

		if ( is_singular( 'post' ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_blog_single_sidebar_layout' );

			if ( ! empty( $option ) ) {
				$layout = $option;
			}

			$meta_option = get_post_meta( get_the_ID(), 'qodef_page_sidebar_layout', true );

			if ( ! empty( $meta_option ) ) {
				$layout = $meta_option;
			}
		}

		return $layout;
	}

	add_filter( 'boldlab_filter_sidebar_layout', 'boldlab_core_set_blog_single_sidebar_layout' );
}

if ( ! function_exists( 'boldlab_core_set_blog_single_custom_sidebar_name' ) ) {
	/**
	 * Function that return sidebar name
	 *
	 * @param $sidebar_name string
	 *
	 * @return string
	 */
	function boldlab_core_set_blog_single_custom_sidebar_name( $sidebar_name ) {

		if ( is_singular( 'post' ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_blog_single_custom_sidebar' );

			if ( ! empty( $option ) ) {
				$sidebar_name = $option;
			}

			$meta_option = get_post_meta( get_the_ID(), 'qodef_page_custom_sidebar', true );

			if ( ! empty( $meta_option ) ) {
				$sidebar_name = $meta_option;
			}
		}

		return $sidebar_name;
	}

	add_filter( 'boldlab_filter_sidebar_name', 'boldlab_core_set_blog_single_custom_sidebar_name' );
}

if ( ! function_exists( 'boldlab_core_set_blog_single_sidebar_grid_gutter_classes' ) ) {
	/**
	 * Function that returns grid gutter classes
	 *
	 * @param $classes string
	 *
	 * @return string
	 */
	function boldlab_core_set_blog_single_sidebar_grid_gutter_classes( $classes ) {

		if ( is_singular( 'post' ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_blog_single_sidebar_grid_gutter' );

			if ( ! empty( $option ) ) {
				$classes = 'qodef-gutter--' . esc_attr( $option );
			}

			$meta_option = get_post_meta( get_the_ID(), 'qodef_page_sidebar_grid_gutter', true );

			if ( ! empty( $meta_option ) ) {
				$classes = 'qodef-gutter--' . esc_attr( $meta_option );
			}
		}

		return $classes;
	}

	add_filter( 'boldlab_filter_grid_gutter_classes', 'boldlab_core_set_blog_single_sidebar_grid_gutter_classes' );
}

if ( ! function_exists( 'boldlab_core_set_blog_list_excerpt_length' ) ) {
	/**
	 * Function that set number of characters for excerpt on blog list page
	 *
	 * @param $excerpt_length int
	 *
	 * @return int
	 */
	function boldlab_core_set_blog_list_excerpt_length( $excerpt_length ) {
		$option = boldlab_core_get_post_value_through_levels( 'qodef_blog_list_excerpt_number_of_characters' );

		if ( $option !== '' ) {
			$excerpt_length = $option;
		}

		return $excerpt_length;
	}

	add_filter( 'boldlab_filter_blog_list_excerpt_length', 'boldlab_core_set_blog_list_excerpt_length' );
}

if ( ! function_exists( 'boldlab_core_set_blog_archive_sidebar_layout' ) ) {
	/**
	 * Function that return sidebar layout
	 *
	 * @param $layout string
	 *
	 * @return string
	 */
	function boldlab_core_set_blog_archive_sidebar_layout( $layout ) {

		if ( ( is_archive() || ( is_home() && is_front_page() ) ) && ( qode_framework_is_installed( 'woocommerce' ) && ! boldlab_core_is_woo_page( 'any' ) ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_blog_archive_sidebar_layout' );

			if ( ! empty( $option ) ) {
				$layout = $option;
			}
		}

		return $layout;
	}

	add_filter( 'boldlab_filter_sidebar_layout', 'boldlab_core_set_blog_archive_sidebar_layout' );
}

if ( ! function_exists( 'boldlab_core_set_blog_archive_custom_sidebar_name' ) ) {
	/**
	 * Function that return sidebar name
	 *
	 * @param $sidebar_name string
	 *
	 * @return string
	 */
	function boldlab_core_set_blog_archive_custom_sidebar_name( $sidebar_name ) {

		if ( ( is_archive() || ( is_home() && is_front_page() ) ) && ( qode_framework_is_installed( 'woocommerce' ) && ! boldlab_core_is_woo_page( 'any' ) ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_blog_archive_custom_sidebar' );

			if ( ! empty( $option ) ) {
				$sidebar_name = $option;
			}
		}

		return $sidebar_name;
	}

	add_filter( 'boldlab_filter_sidebar_name', 'boldlab_core_set_blog_archive_custom_sidebar_name' );
}

if ( ! function_exists( 'boldlab_core_set_blog_archive_sidebar_grid_gutter_classes' ) ) {
	/**
	 * Function that returns grid gutter classes
	 *
	 * @param $classes string
	 *
	 * @return string
	 */
	function boldlab_core_set_blog_archive_sidebar_grid_gutter_classes( $classes ) {

		if ( ( is_archive() || ( is_home() && is_front_page() ) ) && ( qode_framework_is_installed( 'woocommerce' ) && ! boldlab_core_is_woo_page( 'any' ) ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_blog_single_archive_grid_gutter' );

			if ( ! empty( $option ) ) {
				$classes = 'qodef-gutter--' . esc_attr( $option );
			}
		}

		return $classes;
	}

	add_filter( 'boldlab_filter_grid_gutter_classes', 'boldlab_core_set_blog_archive_sidebar_grid_gutter_classes' );
}

if ( ! function_exists( 'boldlab_core_blog_single_set_post_title_instead_of_page_title_text' ) ) {
	/**
	 * Function that set current post title text for single posts
	 *
	 * @param $title string
	 *
	 * @return string
	 */
	function boldlab_core_blog_single_set_post_title_instead_of_page_title_text( $title ) {
		$option = boldlab_core_get_option_value( 'admin', 'qodef_blog_single_set_post_title_in_title_area' );

		if ( is_singular( 'post' ) && $option === 'yes' ) {
			$title = get_the_title( qode_framework_get_page_id() );
		}

		return $title;
	}

	add_filter( 'boldlab_filter_page_title_text', 'boldlab_core_blog_single_set_post_title_instead_of_page_title_text' );
}