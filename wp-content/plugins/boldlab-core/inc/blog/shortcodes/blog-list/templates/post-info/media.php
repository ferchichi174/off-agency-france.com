<div class="qodef-e-media">
	<?php switch ( get_post_format() ) {
		case 'gallery':
			boldlab_core_theme_template_part( 'blog', 'templates/parts/post-format/gallery' );
			break;
		case 'video':
			boldlab_core_theme_template_part( 'blog', 'templates/parts/post-format/video' );
			break;
		case 'audio':
			boldlab_core_theme_template_part( 'blog', 'templates/parts/post-format/audio' );
			break;
		default:
			boldlab_core_template_part( 'blog/shortcodes/blog-list', 'templates/post-info/image' );
			break;
	} ?>
</div>