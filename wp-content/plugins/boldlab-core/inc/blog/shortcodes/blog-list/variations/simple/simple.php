<?php

if ( ! function_exists( 'boldlab_core_add_blog_list_variation_simple' ) ) {
	function boldlab_core_add_blog_list_variation_simple( $variations ) {
		$variations['simple'] = esc_html__( 'Simple', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_blog_list_layouts', 'boldlab_core_add_blog_list_variation_simple' );
}