<?php

if ( ! function_exists( 'boldlab_core_add_blog_list_variation_standard' ) ) {
	function boldlab_core_add_blog_list_variation_standard( $variations ) {
		$variations['standard'] = esc_html__( 'Standard', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_blog_list_layouts', 'boldlab_core_add_blog_list_variation_standard' );
}

if ( ! function_exists( 'boldlab_core_load_blog_list_standard_assets' ) ) {
	function boldlab_core_load_blog_list_standard_assets( $is_enabled, $params ) {
		
		if ( $params['layout'] === 'standard' ) {
			$is_enabled = true;
		}
		
		return $is_enabled;
	}
	
	add_filter( 'boldlab_core_filter_load_blog_list_assets', 'boldlab_core_load_blog_list_standard_assets', 10, 2 );
}