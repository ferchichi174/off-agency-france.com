<?php

if ( ! function_exists( 'boldlab_core_add_blog_list_variation_minimal' ) ) {
	function boldlab_core_add_blog_list_variation_minimal( $variations ) {
		$variations['minimal'] = esc_html__( 'Minimal', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_blog_list_layouts', 'boldlab_core_add_blog_list_variation_minimal' );
}

if ( ! function_exists( 'boldlab_core_add_blog_list_options_minimal' ) ) {
	function boldlab_core_add_blog_list_options_minimal( $options ) {
		$minimal_options   = array();

		$animation_option = ( array(
			'field_type'    => 'select',
			'name'          => 'loading_animation',
			'title'         => esc_html__( 'Enable Loading Animation', 'boldlab-core' ),
			'options'       => boldlab_core_get_select_type_options_pool( 'no_yes', false ),
			'default_value' => 'no',
			'dependency'    => array(
				'show' => array(
					'layout' => array(
						'values'        => 'minimal',
						'default_value' => ''
					)
				)
			),
			'group' => esc_html__( 'Animation', 'boldlab-core' )
		) );

		$minimal_options[] = $animation_option;
		
		return array_merge( $options, $minimal_options );
	}
	
	add_filter( 'boldlab_core_filter_blog_list_extra_options', 'boldlab_core_add_blog_list_options_minimal', 10, 2 );
}