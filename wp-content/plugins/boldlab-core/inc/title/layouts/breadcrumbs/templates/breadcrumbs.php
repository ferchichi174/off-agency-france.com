<?php
// Load title image template
boldlab_core_get_page_title_image(); ?>
<div class="qodef-m-content <?php echo esc_attr( boldlab_core_get_page_title_content_classes() ); ?>">
	<?php
	// Load breadcrumbs template
	boldlab_core_breadcrumbs(); ?>
</div>

