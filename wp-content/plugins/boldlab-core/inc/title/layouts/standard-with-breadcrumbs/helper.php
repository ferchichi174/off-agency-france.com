<?php

if ( ! function_exists( 'boldlab_core_register_standard_with_breadcrumbs_title_layout' ) ) {
	function boldlab_core_register_standard_with_breadcrumbs_title_layout( $layouts ) {
		$layouts['standard-with-breadcrumbs'] = 'BoldlabCoreStandardWithBreadcrumbsTitle';
		
		return $layouts;
	}
	
	add_filter( 'boldlab_core_filter_register_title_layouts', 'boldlab_core_register_standard_with_breadcrumbs_title_layout');
}

if ( ! function_exists( 'boldlab_core_add_standard_with_breadcrumbs_title_layout_option' ) ) {
	/**
	 * Function that set new value into title layout options map
	 *
	 * @param $layouts array - module layouts
	 *
	 * @return array
	 */
	function boldlab_core_add_standard_with_breadcrumbs_title_layout_option( $layouts ) {
		$layouts['standard-with-breadcrumbs'] = esc_html__( 'Standard With Breadcrums', 'boldlab-core' );
		
		return $layouts;
	}
	
	add_filter( 'boldlab_core_filter_title_layout_options', 'boldlab_core_add_standard_with_breadcrumbs_title_layout_option' );
}

