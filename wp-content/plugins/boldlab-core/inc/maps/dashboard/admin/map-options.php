<?php

if ( ! function_exists( 'boldlab_core_add_map_options' ) ) {
	/**
	 * Function that add map options
	 */
	function boldlab_core_add_map_options() {
		$qode_framework = qode_framework_get_framework_root();
		
		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BOLDLAB_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'map',
				'icon'        => 'fa fa-book',
				'title'       => esc_html__( 'Maps', 'boldlab-core' ),
				'description' => esc_html__( 'Global settings related to maps', 'boldlab-core' )
			)
		);
		
		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'text',
					'name'          => 'qodef_maps_api_key',
					'title'         => esc_html__( 'Maps API Key', 'boldlab-core' ),
					'description'   => esc_html__( 'Enter Google Maps api key', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'textarea',
					'name'          => 'qodef_map_style',
					'title'         => esc_html__( 'Map Style', 'boldlab-core' ),
					'description'   => esc_html__( 'Enter snazzy map style json code', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'text',
					'name'          => 'qodef_map_zoom',
					'title'         => esc_html__( 'Map Zoom', 'boldlab-core' ),
					'description'   => esc_html__( 'Enter default zoom value for map', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_map_scroll',
					'title'         => esc_html__( 'Enable Map Scroll', 'boldlab-core' ),
					'description'   => esc_html__( 'Use this option to enable map scrolling', 'boldlab-core' ),
					'default_value' => 'no'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_map_drag',
					'title'         => esc_html__( 'Enable Map Dragging', 'boldlab-core' ),
					'description'   => esc_html__( 'Use this option to enable map dragging', 'boldlab-core' ),
					'default_value' => 'yes'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_map_street_view_control',
					'title'         => esc_html__( 'Enable Map Street View Control', 'boldlab-core' ),
					'description'   => esc_html__( 'Use this option to enable street view control on map', 'boldlab-core' ),
					'default_value' => 'yes'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_map_zoom_control',
					'title'         => esc_html__( 'Enable Map Zoom Control', 'boldlab-core' ),
					'description'   => esc_html__( 'Use this option to enable zoom control on map', 'boldlab-core' ),
					'default_value' => 'yes'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_map_type_control',
					'title'         => esc_html__( 'Enable Map Type Control', 'boldlab-core' ),
					'description'   => esc_html__( 'Use this option to enable type control on map', 'boldlab-core' ),
					'default_value' => 'yes'
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_map_full_screen_control',
					'title'         => esc_html__( 'Enable Map Full Screen Control', 'boldlab-core' ),
					'description'   => esc_html__( 'Use this option to enable full screen control on map', 'boldlab-core' ),
					'default_value' => 'yes'
				)
			);
			
			// Hook to include additional options after module options
			do_action( 'boldlab_core_action_after_map_options_map', $page );
		}
	}
	
	add_action( 'boldlab_core_action_default_options_init', 'boldlab_core_add_map_options', boldlab_core_get_admin_options_map_position( 'maps' ) );
}