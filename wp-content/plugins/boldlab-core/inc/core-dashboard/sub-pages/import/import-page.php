<?php
if ( ! function_exists( 'boldlab_core_add_import_sub_page_to_list' ) ) {
	function boldlab_core_add_import_sub_page_to_list( $sub_pages ) {
		$sub_pages[] = 'BoldlabCoreImportPage';
		return $sub_pages;
	}
	
	add_filter( 'boldlab_core_filter_add_welcome_sub_page', 'boldlab_core_add_import_sub_page_to_list', 11 );
}

if ( class_exists( 'BoldlabCoreSubPage' ) ) {
	class BoldlabCoreImportPage extends BoldlabCoreSubPage {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function add_sub_page() {
			$this->set_base( 'import' );
			$this->set_title( esc_html__('Import', 'boldlab-core'));
			$this->set_atts( $this->set_atributtes());
		}

		public function set_atributtes(){
			$params = array();

			$iparams = BoldlabCoreDashboard::get_instance()->get_import_params();
			if(is_array($iparams) && isset($iparams['submit'])) {
				$params['submit'] = $iparams['submit'];
			}

			return $params;
		}
	}
}