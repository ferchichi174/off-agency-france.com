<?php

if ( ! function_exists( 'boldlab_core_add_list_image_sizes' ) ) {
	function boldlab_core_add_list_image_sizes( $image_sizes ) {
		$image_sizes[] = array(
			'slug'           => 'boldlab_image_size_square',
			'label'          => esc_html__( 'Square Size', 'boldlab-core' ),
			'label_simple'   => esc_html__( 'Square', 'boldlab-core' ),
			'default_crop'   => true,
			'default_width'  => 650,
			'default_height' => 650
		);
		
		$image_sizes[] = array(
			'slug'           => 'boldlab_image_size_landscape',
			'label'          => esc_html__( 'Landscape Size', 'boldlab-core' ),
			'label_simple'   => esc_html__( 'Landscape', 'boldlab-core' ),
			'default_crop'   => true,
			'default_width'  => 1300,
			'default_height' => 650
		);
		
		$image_sizes[] = array(
			'slug'           => 'boldlab_image_size_portrait',
			'label'          => esc_html__( 'Portrait Size', 'boldlab-core' ),
			'label_simple'   => esc_html__( 'Portrait', 'boldlab-core' ),
			'default_crop'   => true,
			'default_width'  => 650,
			'default_height' => 1300
		);
		
		$image_sizes[] = array(
			'slug'           => 'boldlab_image_size_huge-square',
			'label'          => esc_html__( 'Huge Square Size', 'boldlab-core' ),
			'label_simple'   => esc_html__( 'Huge Square', 'boldlab-core' ),
			'default_crop'   => true,
			'default_width'  => 1300,
			'default_height' => 1300
		);
		
		return $image_sizes;
	}
	
	add_filter( 'qode_framework_filter_populate_image_sizes', 'boldlab_core_add_list_image_sizes' );
}

if ( ! function_exists( 'boldlab_core_add_pool_masonry_list_image_sizes' ) ) {
	function boldlab_core_add_pool_masonry_list_image_sizes( $options, $type, $enable_default, $exclude ) {
		if ( $type == 'masonry_image_dimension' ) {
			$options = array();
			if ( $enable_default ) {
				$options[''] = esc_html__( 'Default', 'boldlab-core' );
			}
			$options['boldlab_image_size_square']      = esc_html__( 'Square', 'boldlab-core' );
			$options['boldlab_image_size_landscape']   = esc_html__( 'Landscape', 'boldlab-core' );
			$options['boldlab_image_size_portrait']    = esc_html__( 'Portrait', 'boldlab-core' );
			$options['boldlab_image_size_huge-square'] = esc_html__( 'Huge Square', 'boldlab-core' );
		}
		
		return $options;
	}
	
	add_filter( 'boldlab_core_filter_select_type_option', 'boldlab_core_add_pool_masonry_list_image_sizes', 10, 4 );
}

if ( ! function_exists( 'boldlab_core_get_custom_image_size_class_name' ) ) {
	function boldlab_core_get_custom_image_size_class_name( $image_size ) {
		return ! empty( $image_size ) ? 'qodef-item--' . str_replace( 'boldlab_image_size_', '', $image_size ) : '';
	}
}

if ( ! function_exists( 'boldlab_core_get_custom_image_size_meta' ) ) {
	function boldlab_core_get_custom_image_size_meta( $type, $name, $post_id ) {
		$image_size_meta  = qode_framework_get_option_value( '', $type, $name, '', $post_id );
		$image_size       = ! empty( $image_size_meta ) ? esc_attr( $image_size_meta ) : 'full';

		return array(
			'size'  => $image_size,
			'class' => boldlab_core_get_custom_image_size_class_name( $image_size )
		);
	}
}