<?php

if ( ! function_exists( 'boldlab_core_add_fonts_options' ) ) {
	/**
	 * Function that add options for this module
	 */
	function boldlab_core_add_fonts_options() {
		$qode_framework = qode_framework_get_framework_root();
		
		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BOLDLAB_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'fonts',
				'title'       => esc_html__( 'Fonts', 'boldlab-core' ),
				'description' => esc_html__( 'General Fonts Options', 'boldlab-core' ),
				'icon'        => 'fa fa-cog'
			)
		);
		
		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_google_fonts',
					'title'         => esc_html__( 'Enable Google Fonts', 'boldlab-core' ),
					'default_value' => 'yes',
					'args'          => array(
						'custom_class' => 'qodef-enable-google-fonts'
					)
				)
			);
			
			$google_fonts_section = $page->add_section_element(
				array(
					'name'       => 'qodef_google_fonts_section',
					'title'      => esc_html__( 'Google Fonts Options', 'boldlab-core' ),
					'dependency' => array(
						'show' => array(
							'qodef_enable_google_fonts' => array(
								'values'        => 'yes',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$page_repeater = $google_fonts_section->add_repeater_element(
				array(
					'name'        => 'qodef_choose_google_fonts',
					'title'       => esc_html__( 'Google Fonts To Include', 'boldlab-core' ),
					'description' => esc_html__( 'Choose google fonts which you want to use on your website', 'boldlab-core' ),
					'button_text' => esc_html__( 'Add New Google Font', 'boldlab-core' )
				)
			);
			
			$page_repeater->add_field_element( array(
				'field_type'  => 'googlefont',
				'name'        => 'qodef_choose_google_font',
				'title'       => esc_html__( 'Google Font', 'boldlab-core' ),
				'description' => esc_html__( 'Choose google font', 'boldlab-core' ),
				'args'        => array(
					'include' => 'google-fonts'
				)
			) );
			
			$google_fonts_section->add_field_element(
				array(
					'field_type'  => 'checkbox',
					'name'        => 'qodef_google_fonts_weight',
					'title'       => esc_html__( 'Google Fonts Style & Weight', 'boldlab-core' ),
					'description' => esc_html__( 'Choose a default Google font weights for your site. Impact on page load time', 'boldlab-core' ),
					'options'     => array(
						'100'  => esc_html__( '100 Thin', 'boldlab-core' ),
						'100i' => esc_html__( '100 Thin Italic', 'boldlab-core' ),
						'200'  => esc_html__( '200 Extra-Light', 'boldlab-core' ),
						'200i' => esc_html__( '200 Extra-Light Italic', 'boldlab-core' ),
						'300'  => esc_html__( '300 Light', 'boldlab-core' ),
						'300i' => esc_html__( '300 Light Italic', 'boldlab-core' ),
						'400'  => esc_html__( '400 Regular', 'boldlab-core' ),
						'400i' => esc_html__( '400 Regular Italic', 'boldlab-core' ),
						'500'  => esc_html__( '500 Medium', 'boldlab-core' ),
						'500i' => esc_html__( '500 Medium Italic', 'boldlab-core' ),
						'600'  => esc_html__( '600 Semi-Bold', 'boldlab-core' ),
						'600i' => esc_html__( '600 Semi-Bold Italic', 'boldlab-core' ),
						'700'  => esc_html__( '700 Bold', 'boldlab-core' ),
						'700i' => esc_html__( '700 Bold Italic', 'boldlab-core' ),
						'800'  => esc_html__( '800 Extra-Bold', 'boldlab-core' ),
						'800i' => esc_html__( '800 Extra-Bold Italic', 'boldlab-core' ),
						'900'  => esc_html__( '900 Ultra-Bold', 'boldlab-core' ),
						'900i' => esc_html__( '900 Ultra-Bold Italic', 'boldlab-core' )
					)
				)
			);
			
			$google_fonts_section->add_field_element(
				array(
					'field_type'  => 'checkbox',
					'name'        => 'qodef_google_fonts_subset',
					'title'       => esc_html__( 'Google Fonts Style & Weight', 'boldlab-core' ),
					'description' => esc_html__( 'Choose a default Google font weights for your site. Impact on page load time', 'boldlab-core' ),
					'options'     => array(
						'latin'        => esc_html__( 'Latin', 'boldlab-core' ),
						'latin-ext'    => esc_html__( 'Latin Extended', 'boldlab-core' ),
						'cyrillic'     => esc_html__( 'Cyrillic', 'boldlab-core' ),
						'cyrillic-ext' => esc_html__( 'Cyrillic Extended', 'boldlab-core' ),
						'greek'        => esc_html__( 'Greek', 'boldlab-core' ),
						'greek-ext'    => esc_html__( 'Greek Extended', 'boldlab-core' ),
						'vietnamese'   => esc_html__( 'Vietnamese', 'boldlab-core' )
					)
				)
			);
			
			$page_repeater = $page->add_repeater_element(
				array(
					'name'        => 'qodef_custom_fonts',
					'title'       => esc_html__( 'Custom Fonts', 'boldlab-core' ),
					'description' => esc_html__( 'Add Custom Fonts', 'boldlab-core' ),
					'button_text' => esc_html__( 'Add New Custom Font', 'boldlab-core' )
				)
			);
			
			$page_repeater->add_field_element( array(
				'field_type' => 'file',
				'name'       => 'qodef_custom_font_ttf',
				'title'      => esc_html__( 'Custom Font TTF', 'boldlab-core' ),
				'args'       => array(
					'allowed_type' => 'font/ttf'
				)
			) );
			
			$page_repeater->add_field_element( array(
				'field_type' => 'file',
				'name'       => 'qodef_custom_font_otf',
				'title'      => esc_html__( 'Custom Font OTF', 'boldlab-core' ),
				'args'       => array(
					'allowed_type' => 'font/otf'
				)
			) );
			
			$page_repeater->add_field_element( array(
				'field_type' => 'file',
				'name'       => 'qodef_custom_font_woff',
				'title'      => esc_html__( 'Custom Font WOFF', 'boldlab-core' ),
				'args'       => array(
					'allowed_type' => 'font/woff'
				)
			) );
			
			$page_repeater->add_field_element( array(
				'field_type' => 'file',
				'name'       => 'qodef_custom_font_woff2',
				'title'      => esc_html__( 'Custom Font WOFF2', 'boldlab-core' ),
				'args'       => array(
					'allowed_type' => 'font/woff2'
				)
			) );
			
			$page_repeater->add_field_element( array(
				'field_type' => 'text',
				'name'       => 'qodef_custom_font_name',
				'title'      => esc_html__( 'Custom Font Name', 'boldlab-core' ),
			) );
			
			do_action( 'boldlab_core_action_after_page_fonts_options_map', $page );
		}
	}
	
	add_action( 'boldlab_core_action_default_options_init', 'boldlab_core_add_fonts_options', boldlab_core_get_admin_options_map_position( 'fonts' ) );
}