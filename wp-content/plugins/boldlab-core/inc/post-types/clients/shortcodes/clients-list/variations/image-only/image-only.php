<?php

if ( ! function_exists( 'boldlab_core_add_clients_list_variation_image_only' ) ) {
	function boldlab_core_add_clients_list_variation_image_only( $variations ) {
		
		$variations['image-only'] = esc_html__( 'Image Only', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_clients_list_layouts', 'boldlab_core_add_clients_list_variation_image_only' );
}