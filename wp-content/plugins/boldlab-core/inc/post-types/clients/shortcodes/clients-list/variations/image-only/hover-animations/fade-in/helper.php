<?php
if ( ! function_exists( 'boldlab_core_filter_clients_list_image_only_fade_in' ) ) {
	function boldlab_core_filter_clients_list_image_only_fade_in( $variations ) {
		
		$variations['fade-in'] = esc_html__( 'Fade In', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_clients_list_image_only_animation_options', 'boldlab_core_filter_clients_list_image_only_fade_in' );
}