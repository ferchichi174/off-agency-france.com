<div class="qodef-e qodef-portfolio-social-share">
	<?php
		$params = array();
		$params['title'] = esc_html__( 'Share:', 'boldlab-core' );
		$params['layout'] = 'list';
		echo BoldlabCoreSocialShareShortcode::call_shortcode( $params );
	?>
</div>