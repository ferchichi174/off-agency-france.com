(function ($) {

    $(window).on('load', function () {
        qodefPtfInfoOnHoverAnimation.init();
    });

    var qodefPtfInfoOnHoverAnimation = {
        init: function () {
            var $sections = $('.qodef-portfolio-list.qodef-item-layout--info-on-hover.qodef--with-animation');

            $sections.length && $sections.each(function () {
                var $section = $(this),
                    $items = qodefPtfInfoOnHoverAnimation.getItems($section);

                qodefPtfInfoOnHoverAnimation.observe($section, $items);
            });
        },
        observe: function ($section, $items) {
            $section.appear(function () {
                qodefPtfInfoOnHoverAnimation.animate($items);
            })
        },
        animate: function ($items) {
            var i = 0;

            TweenMax.staggerFromTo($items, .5, {
                scaleX: 1.25,
                transformOrigin: '0 0'
            }, {
                scaleX: 1,
                ease: Power4.easeOut,
                onComplete: function () {
                    TweenMax.set($items[i], {
                        'pointer-events': 'auto'
                    });
                    i++;
                }
            }, .05)

            TweenMax.staggerFromTo($items, .5, {
                autoAlpha: 0,
            }, {
                autoAlpha: 1,
                ease: Expo.easeOut,
            }, .05)
        },
        shuffle: function (array) {
            var currentIndex = array.length,
                temporaryValue, randomIndex;

            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        },
        getItems: function ($holder) {
            var $items = $holder.find('.qodef-e-inner').sort(function (a, b) {
                return 0.5 - Math.random()
            })
            return $items;
        }
    };

})(jQuery);