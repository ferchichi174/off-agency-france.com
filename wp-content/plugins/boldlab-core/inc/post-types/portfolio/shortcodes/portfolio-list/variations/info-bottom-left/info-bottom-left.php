<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_list_variation_info_bottom_left' ) ) {
	function boldlab_core_add_portfolio_list_variation_info_bottom_left( $variations ) {
		
		$variations['info-bottom-left'] = esc_html__( 'Info Bottom Left', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_list_layouts', 'boldlab_core_add_portfolio_list_variation_info_bottom_left' );
}