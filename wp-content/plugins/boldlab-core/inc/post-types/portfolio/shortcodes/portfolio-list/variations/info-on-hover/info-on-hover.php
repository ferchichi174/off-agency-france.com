<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_list_variation_info_on_hover' ) ) {
	function boldlab_core_add_portfolio_list_variation_info_on_hover( $variations ) {
		
		$variations['info-on-hover'] = esc_html__( 'Info On Hover', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_list_layouts', 'boldlab_core_add_portfolio_list_variation_info_on_hover' );
}

if ( ! function_exists( 'boldlab_core_add_portfolio_loading_animation_options' ) ) {
	function boldlab_core_add_portfolio_loading_animation_options( $options ) {
		$info_on_hover_options   = array();

		$animation_option = ( array(
			'field_type'    => 'select',
			'name'          => 'loading_animation',
			'title'         => esc_html__( 'Enable Loading Animation', 'boldlab-core' ),
			'options'       => boldlab_core_get_select_type_options_pool( 'no_yes', false ),
			'default_value' => 'no',
			'dependency'    => array(
				'show' => array(
					'layout' => array(
						'values'        => 'info-on-hover',
						'default_value' => ''
					)
				)
			),
			'group' => esc_html__( 'Animation', 'boldlab-core' )
		) );

		$info_on_hover_options[] = $animation_option;
		
		return array_merge( $options, $info_on_hover_options );
	}
	
	add_filter( 'boldlab_core_filter_portfolio_list_extra_options', 'boldlab_core_add_portfolio_loading_animation_options', 10, 2 );
}