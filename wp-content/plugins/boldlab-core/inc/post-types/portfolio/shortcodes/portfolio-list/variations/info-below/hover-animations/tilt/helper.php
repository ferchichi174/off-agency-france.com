<?php

if ( ! function_exists( 'boldlab_core_filter_portfolio_list_info_below_tilt' ) ) {
	function boldlab_core_filter_portfolio_list_info_below_tilt( $variations ) {
		$variations['tilt'] = esc_html__( 'Tilt', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_list_info_below_animation_options', 'boldlab_core_filter_portfolio_list_info_below_tilt' );
}