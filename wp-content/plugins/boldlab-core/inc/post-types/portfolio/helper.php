<?php

if ( ! function_exists( 'boldlab_core_include_portfolio_media_fields' ) ) {
	function boldlab_core_include_portfolio_media_fields() {
		foreach ( glob( BOLDLAB_CORE_INC_PATH . '/post-types/portfolio/dashboard/media/*.php' ) as $media ) {
			include_once $media;
		}
	}

	add_action( 'qode_framework_action_custom_media_fields', 'boldlab_core_include_portfolio_media_fields' );
}

if ( ! function_exists( 'boldlab_core_generate_portfolio_single_layout' ) ) {
	function boldlab_core_generate_portfolio_single_layout() {
		$portfolio_template = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_single_layout' );
		$portfolio_template = empty( $portfolio_template ) ? '' : $portfolio_template;
		
		return $portfolio_template;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_single_layout', 'boldlab_core_generate_portfolio_single_layout' );
}

if ( ! function_exists( 'boldlab_core_get_portfolio_holder_classes' ) ) {
	/**
	 * Function that return classes for the main portfolio holder
	 *
	 * @return string
	 */
	function boldlab_core_get_portfolio_holder_classes() {
		$classes = array( '' );

			$classes[]      = 'qodef-portfolio-single';

			$item_layout    = boldlab_core_generate_portfolio_single_layout();
			$classes[]      =' qodef-item-layout--' . $item_layout;
		
		return implode( ' ', $classes );
	}
}

if ( ! function_exists( 'boldlab_core_set_portfolio_single_body_classes' ) ) {
	/**
	 * Function that return classes for the single custom post type
	 *
	 * @return array $classes
	 */
	function boldlab_core_set_portfolio_single_body_classes( $classes ) {
		$item_layout = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_single_layout' );
		
		if ( is_singular( 'portfolio' ) && ! empty( $item_layout ) ) {
			$classes[] = ' qodef-porfolio-single-layout--' . $item_layout;
		}
		
		return $classes;
	}
	
	add_filter( 'body_class', 'boldlab_core_set_portfolio_single_body_classes' );
}

if ( ! function_exists( 'boldlab_core_generate_portfolio_archive_with_shortcode' ) ) {
	/**
	 * Function that executes portfolio list shortcode with params on archive pages
	 *
	 * @param string $tax - type of taxonomy
	 * @param string $tax_slug - slug of taxonomy
	 *
	 */
	function boldlab_core_generate_portfolio_archive_with_shortcode( $tax, $tax_slug ) {
		$params = array();
		
		$params['additional_params'] = 'tax';
		$params['tax'] = $tax;
		$params['tax_slug'] = $tax_slug;
		$params['layout'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_item_layout' );
		$params['behavior'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_behavior' );
		$params['masonry_images_proportion'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_masonry_images_proportion' );
		$params['columns'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_columns' );
		$params['space'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_space' );
		$params['columns_responsive'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_columns_responsive' );
		$params['columns_1440'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_columns_1440' );
		$params['columns_1366'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_columns_1366' );
		$params['columns_1024'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_columns_1024' );
		$params['columns_768'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_columns_768' );
		$params['columns_680'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_columns_680' );
		$params['columns_480'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_columns_480' );
		$params['slider_loop'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_slider_loop' );
		$params['slider_autoplay'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_slider_autoplay' );
		$params['slider_speed'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_slider_speed' );
		$params['slider_navigation'] = boldlab_core_get_post_value_through_levels( 'navigation' );
		$params['slider_pagination'] = boldlab_core_get_post_value_through_levels( 'pagination' );
		$params['pagination_type'] = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_pagination_type' );
		$params['title_tag'] = 'h4';

		echo BoldlabCorePortfolioListShortcode::call_shortcode( $params );
	}
}

if ( ! function_exists( 'boldlab_core_is_portfolio_title_enabled' ) ) {
	function boldlab_core_is_portfolio_title_enabled( $is_enabled ) {

		if ( is_singular( 'portfolio' ) ) {
			$portfolio_title = boldlab_core_get_post_value_through_levels( 'qodef_enable_portfolio_title' );
			$is_enabled = $portfolio_title == '' ? $is_enabled : ($portfolio_title == 'no' ? false : true);
		}
		return $is_enabled;
	}

	add_filter( 'boldlab_core_filter_is_page_title_enabled ', 'boldlab_core_is_portfolio_title_enabled');
}

if ( ! function_exists( 'boldlab_core_portfolio_title_grid' ) ) {
	function boldlab_core_portfolio_title_grid( $enable_title_grid ) {
		if ( is_singular( 'portfolio' ) ) {
			$portfolio_title_grid = boldlab_core_get_post_value_through_levels( 'qodef_set_portfolio_title_area_in_grid' );
			$enable_title_grid = $portfolio_title_grid == '' ? $enable_title_grid : ($portfolio_title_grid == 'no' ? false : true);
		}

		return $enable_title_grid;
	}

	add_filter( 'boldlab_core_filter_page_title_in_grid', 'boldlab_core_portfolio_title_grid' );
}


if ( ! function_exists( 'boldlab_core_portfolio_breadcrumbs_title' ) ) {
	function boldlab_core_portfolio_breadcrumbs_title( $wrap_child, $settings ) {
	    if( is_tax( 'portfolio-category' ) ) {
	    	$wrap_child = '';
		    $category = get_term( get_queried_object_id(), 'portfolio-category' );
		
		    if ( isset( $category->parent ) && $category->parent !== 0 ) {
			    $parent = get_term( $category->parent );
			    $wrap_child .= sprintf( $settings['link'], get_term_link( $parent->term_id ), $parent->name . $settings['separator'] );
		    }
		    
		    $wrap_child .= sprintf( $settings['current_item'], single_cat_title( '', false ) );
	    } else if ( is_singular( 'portfolio' ) ) {
		    $wrap_child = '';
		    $categories = wp_get_post_terms(get_the_ID(), 'portfolio-category');
		    
		    if( !empty ( $categories ) ) {
			    $category = $categories[0];
			    if ( isset( $category->parent ) && $category->parent !== 0 ) {
			    	$parent = get_term( $category->parent );
				    $wrap_child .= sprintf( $settings['link'], get_term_link( $parent->term_id ), $parent->name . $settings['separator'] );
			    }
			    $wrap_child .= sprintf( $settings['link'], get_term_link( $category ), $category->name . $settings['separator'] );
		    }
		
		    $wrap_child .= sprintf( $settings['current_item'], get_the_title() );
	    }
	    return $wrap_child;
	}
	
	add_filter( 'boldlab_core_filter_breadcrumbs_content', 'boldlab_core_portfolio_breadcrumbs_title', 10, 2 );
}

if ( ! function_exists( 'boldlab_core_set_portfolio_custom_sidebar_name' ) ) {
	/**
	 * Function that return sidebar name
	 *
	 * @param $sidebar_name string
	 *
	 * @return string
	 */
	function boldlab_core_set_portfolio_custom_sidebar_name( $sidebar_name ) {
		
		if( is_singular( 'portfolio' ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_single_custom_sidebar' );
		} else if( is_tax() ) {
			$taxonomies = get_object_taxonomies( 'portfolio' );
			foreach ( $taxonomies as $tax ) {
				if( is_tax( $tax ) ) {
					$option = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_custom_sidebar' );
				}
			}
		}
		
		if ( isset( $option ) && ! empty( $option ) ) {
			$sidebar_name = $option;
		}
		
		return $sidebar_name;
	}
	
	add_filter( 'boldlab_filter_sidebar_name', 'boldlab_core_set_portfolio_custom_sidebar_name' );
}

if ( ! function_exists( 'boldlab_core_set_portfolio_sidebar_layout' ) ) {
	/**
	 * Function that return sidebar layout
	 *
	 * @param $layout string
	 *
	 * @return string
	 */
	function boldlab_core_set_portfolio_sidebar_layout( $layout ) {
		
		if( is_singular( 'portfolio' ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_single_sidebar_layout' );
		} else if( is_tax() ) {
			$taxonomies = get_object_taxonomies( 'portfolio' );
			foreach ( $taxonomies as $tax ) {
				if( is_tax( $tax ) ) {
					$option = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_sidebar_layout' );
				}
			}
		}
		
		if ( isset( $option ) && ! empty( $option ) ) {
			$layout = $option;
		}
		
		return $layout;
	}
	
	add_filter( 'boldlab_filter_sidebar_layout', 'boldlab_core_set_portfolio_sidebar_layout' );
}

if ( ! function_exists( 'boldlab_core_set_portfolio_sidebar_grid_gutter_classes' ) ) {
	/**
	 * Function that returns grid gutter classes
	 *
	 * @param $classes string
	 *
	 * @return string
	 */
	function boldlab_core_set_portfolio_sidebar_grid_gutter_classes( $classes ) {
		
		if( is_singular( 'portfolio' ) ) {
			$option = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_single_sidebar_grid_gutter' );
		} else if( is_tax() ) {
			$taxonomies = get_object_taxonomies( 'portfolio' );
			foreach ( $taxonomies as $tax ) {
				if( is_tax( $tax ) ) {
					$option = boldlab_core_get_post_value_through_levels( 'qodef_portfolio_archive_sidebar_grid_gutter' );
				}
			}
		}
		if ( isset( $option ) && ! empty( $option ) ) {
			$classes = 'qodef-gutter--' . esc_attr( $option );
		}
		
		return $classes;
	}
	
	add_filter('boldlab_filter_grid_gutter_classes', 'boldlab_core_set_portfolio_sidebar_grid_gutter_classes');
}

if ( ! function_exists( 'boldlab_core_portfolio_set_admin_options_map_position' ) ) {
	/**
	 * Function that set dashboard admin options map position for this module
	 *
	 * @param $position int
	 * @param $map string
	 *
	 * @return int
	 */
	function boldlab_core_portfolio_set_admin_options_map_position( $position, $map ) {
		
		if ( $map === 'portfolio' ) {
			$position = 50;
		}
		
		return $position;
	}
	
	add_filter( 'boldlab_core_filter_admin_options_map_position', 'boldlab_core_portfolio_set_admin_options_map_position', 10, 2 );
}

if ( ! function_exists( 'boldlab_core_portfolio_title_area_text' ) ) {

	function boldlab_core_portfolio_title_area_text($title) {
		if(is_singular('portfolio')) {
			$title =  esc_html__('Portfolio', 'boldlab-core');
		}
		return $title;
	}

	add_filter( 'boldlab_filter_page_title_text', 'boldlab_core_portfolio_title_area_text', 10, 2 );
}

