<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_single_variation_custom' ) ) {
	function boldlab_core_add_portfolio_single_variation_custom( $variations ) {
		$variations['custom'] = esc_html__( 'Custom', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_single_layout_options', 'boldlab_core_add_portfolio_single_variation_custom' );
}