<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_single_variation_gallery_small' ) ) {
	function boldlab_core_add_portfolio_single_variation_gallery_small( $variations ) {
		$variations['gallery-small'] = esc_html__( 'Gallery - Small', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_single_layout_options', 'boldlab_core_add_portfolio_single_variation_gallery_small' );
}