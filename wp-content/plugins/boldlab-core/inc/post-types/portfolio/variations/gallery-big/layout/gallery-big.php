<article <?php post_class( 'qodef-portfolio-single-item qodef-e' ); ?>>
    <div class="qodef-e-inner">
        <div class="qodef-media">
			<?php boldlab_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/media', 'gallery'); ?>
        </div>
        <div class="qodef-e-content qodef-grid qodef-layout--template qodef-gutter--small qodef--no-bottom-space">
            <div class="qodef-grid-inner clear">
                <div class="qodef-grid-item qodef-col--9">
					<?php boldlab_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/title' ); ?>
					<?php boldlab_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/content' ); ?>
                </div>
                <div class="qodef-grid-item qodef-col--3 qodef-portfolio-info">
					<?php boldlab_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/custom-fields' ); ?>
					<?php boldlab_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/date' ); ?>
                    <?php boldlab_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/categories' ); ?>
					<?php boldlab_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/social-share' ); ?>
                </div>
            </div>
        </div>
    </div>
	<?php boldlab_core_template_part( 'post-types/portfolio', 'single-navigation/templates/single-navigation' ); ?>
</article>