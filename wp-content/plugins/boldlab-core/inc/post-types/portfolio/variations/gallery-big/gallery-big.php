<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_single_variation_gallery_big' ) ) {
	function boldlab_core_add_portfolio_single_variation_gallery_big( $variations ) {
		$variations['gallery-big'] = esc_html__( 'Gallery - Big', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_single_layout_options', 'boldlab_core_add_portfolio_single_variation_gallery_big' );
}