(function ($) {

    $(document).ready(function () {
        qodefTeamInfoBelow.init();
    });

    var qodefTeamInfoBelow = {
        init: function () {
            var $sections = $('.qodef-team-list.qodef--with-animation');

            $sections.length && $sections.each(function () {
                var $section = $(this);
                qodefTeamInfoBelow.observe($section);
            });
        },
        observe: function ($section) {
            var $items = $section.find('.qodef-e');

            $section.appear(function () {
                $items.each(function (i) {
                    var $item = $(this);

                    qodefTeamInfoBelow.animate($item, i / 5);
                })
            })
        },
        animate: function ($item, delay) {
            var $image = $item.find('.qodef-e-image'),
                $role = $item.find('.qodef-e-role'),
                $title = $item.find('.qodef-e-title'),
                $icons = $item.find('.qodef-team-member-social-icons');

            var timeline = new TimelineMax();

            timeline
                .to($item, .6, {
                    autoAlpha: 1,
                    delay: Modernizr.touchevents ? 0 : delay
                })
                .from($image, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power4.easeOut
                }, "-=.6")
                .from($role, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($title, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($icons, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4");
        }
    };

})(jQuery);