<?php

include_once 'team-list.php';

foreach ( glob( BOLDLAB_CORE_INC_PATH . '/post-types/team/shortcodes/team-list/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}