<?php

if ( ! function_exists( 'boldlab_core_add_team_list_variation_info_on_hover' ) ) {
	function boldlab_core_add_team_list_variation_info_on_hover( $variations ) {
		
		$variations['info-on-hover'] = esc_html__( 'Info on Hover', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_team_list_layouts', 'boldlab_core_add_team_list_variation_info_on_hover' );
}