(function ($) {

    $(document).ready(function () {
        qodefCustomAnimations.init();
    });

    var qodefCustomAnimations = {
        init: function () {
            var $anims = $('.qodef-from-left, .qodef-from-right');

            $anims.length && $anims.each(function () {
                var $item = $(this);

                $item.css({
                    'visibility' : 'hidden',
                    'will-change': 'transform'
                });
                $item.hasClass('qodef-from-left') && qodefCustomAnimations.observe($item, 'left');
                $item.hasClass('qodef-from-right') && qodefCustomAnimations.observe($item, 'right');
            });
        },
        observe: function ($item, type) {
            $item.appear(function () {
                qodefCustomAnimations.animate($item, type);
            })
        },
        animate: function ($item, type) {
            TweenMax.from($item, .6, {
                autoAlpha: 0,
                x: type == 'left' ? -100 : 100,
                scaleX: 1.4,
                skewY: type == 'left' ? -10 : 10,
                ease: Quint.easeOut,
                delay: .2
            })
        }
    }
})(jQuery);