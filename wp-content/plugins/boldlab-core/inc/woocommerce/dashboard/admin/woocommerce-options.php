<?php

if ( ! function_exists( 'boldlab_core_add_woocommerce_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function boldlab_core_add_woocommerce_options() {
		$qode_framework = qode_framework_get_framework_root();

		$list_item_layouts = apply_filters( 'boldlab_core_filter_product_list_layouts', array() );
		$options_map       = boldlab_core_get_variations_options_map( $list_item_layouts );

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BOLDLAB_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'woocommerce',
				'icon'        => 'fa fa-book',
				'title'       => esc_html__( 'WooCommerce', 'boldlab-core' ),
				'description' => esc_html__( 'Global settings related to WooCommerce', 'boldlab-core' ),
				'layout'      => 'tabbed'
			)
		);

		if ( $page ) {

			$list_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-list',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Product List', 'boldlab-core' ),
					'description' => esc_html__( 'Settings related to product list', 'boldlab-core' )
				)
			);

			if ( $options_map['visibility'] ) {
				$list_tab->add_field_element(
					array(
						'field_type'    => 'select',
						'name'          => 'qodef_product_list_item_layout',
						'title'         => esc_html__( 'Item Layout', 'boldlab-core' ),
						'description'   => esc_html__( 'Choose layout for list item on shop list', 'boldlab-core' ),
						'options'       => $list_item_layouts,
						'default_value' => $options_map['default_value']
					)
				);
			}

			$list_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_woo_product_list_columns',
					'title'       => esc_html__( 'Columns Number', 'boldlab-core' ),
					'description' => esc_html__( 'Choose number of columns for product list on shop page', 'boldlab-core' ),
					'options'     => boldlab_core_get_select_type_options_pool( 'columns_number' )
				)
			);

			$list_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_woo_product_list_columns_space',
					'title'       => esc_html__( 'Space Between Items', 'boldlab-core' ),
					'description' => esc_html__( 'Choose space between items for product list on shop page', 'boldlab-core' ),
					'options'     => boldlab_core_get_select_type_options_pool( 'items_space' )
				)
			);

			$list_tab->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_woo_product_list_products_per_page',
					'title'       => esc_html__( 'Products per Page', 'boldlab-core' ),
					'description' => esc_html__( 'Set number of products on shop page. Default value is 12', 'boldlab-core' ),
				)
			);

			$list_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_woo_product_list_title_tag',
					'title'       => esc_html__( 'Title Tag', 'boldlab-core' ),
					'description' => esc_html__( 'Choose title tag for product list item on shop page', 'boldlab-core' ),
					'options'     => boldlab_core_get_select_type_options_pool( 'title_tag' )
				)
			);

			$list_tab->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_woo_product_list_sidebar_layout',
					'title'         => esc_html__( 'Sidebar Layout', 'boldlab-core' ),
					'description'   => esc_html__( 'Choose default sidebar layout for shop page', 'boldlab-core' ),
					'default_value' => 'no-sidebar',
					'options'       => boldlab_core_get_select_type_options_pool( 'sidebar_layouts', false )
				)
			);

			$custom_sidebars = boldlab_core_get_custom_sidebars();
			if ( ! empty( $custom_sidebars ) && count( $custom_sidebars ) > 1 ) {
				$list_tab->add_field_element(
					array(
						'field_type'  => 'select',
						'name'        => 'qodef_woo_product_list_custom_sidebar',
						'title'       => esc_html__( 'Custom Sidebar', 'boldlab-core' ),
						'description' => esc_html__( 'Choose a custom sidebar to display on shop page', 'boldlab-core' ),
						'options'     => $custom_sidebars
					)
				);
			}

			$list_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_woo_product_list_sidebar_grid_gutter',
					'title'       => esc_html__( 'Set Grid Gutter', 'boldlab-core' ),
					'description' => esc_html__( 'Choose grid gutter size to set space between content and sidebar', 'boldlab-core' ),
					'options'     => boldlab_core_get_select_type_options_pool( 'items_space' )
				)
			);

			// Hook to include additional options after section module options
			do_action( 'boldlab_core_action_after_woo_product_list_options_map', $list_tab );

			$single_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-single',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Product Single', 'boldlab-core' ),
					'description' => esc_html__( 'Settings related to product single', 'boldlab-core' )
				)
			);

			$single_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_woo_single_enable_page_title',
					'title'       => esc_html__( 'Enable Page Title', 'boldlab-core' ),
					'description' => esc_html__( 'Use this option to enable/disable page title on single product page', 'boldlab-core' ),
					'options'     => boldlab_core_get_select_type_options_pool( 'no_yes' )
				)
			);

			$single_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_woo_single_title_tag',
					'title'       => esc_html__( 'Title Tag', 'boldlab-core' ),
					'description' => esc_html__( 'Choose title tag for product on single product page', 'boldlab-core' ),
					'options'     => boldlab_core_get_select_type_options_pool( 'title_tag' )
				)
			);

			$single_tab->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_woo_single_enable_image_photo_swipe',
					'title'         => esc_html__( 'Enable Image Photo Swipe', 'boldlab-core' ),
					'description'   => esc_html__( 'Enabling this option will set lightbox functionality for images on single product page', 'boldlab-core' ),
					'default_value' => 'yes'
				)
			);

			$single_tab->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_woo_single_enable_image_zoom',
					'title'         => esc_html__( 'Enable Zoom Maginfier', 'boldlab-core' ),
					'description'   => esc_html__( 'Enabling this option will show magnifier image on hover on single product page', 'boldlab-core' ),
					'default_value' => 'yes'
				)
			);

			$single_tab->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_woo_single_thumbnail_images_columns',
					'title'         => esc_html__( 'Thumbnail Images Columns Number', 'boldlab-core' ),
					'description'   => esc_html__( 'Choose number of columns for thumbnail images on single product page', 'boldlab-core' ),
					'options'       => boldlab_core_get_select_type_options_pool( 'columns_number' ),
					'default_value' => '3'
				)
			);

			$single_tab->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_woo_single_related_product_list_columns',
					'title'         => esc_html__( 'Columns Number', 'boldlab-core' ),
					'description'   => esc_html__( 'Choose number of columns for related product list on single product page', 'boldlab-core' ),
					'options'       => boldlab_core_get_select_type_options_pool( 'columns_number' ),
					'default_value' => '3'
				)
			);

			// Hook to include additional options after section module options
			do_action( 'boldlab_core_action_after_woo_product_single_options_map', $single_tab );

			// Hook to include additional options after module options
			do_action( 'boldlab_core_action_after_woo_options_map', $page );
		}
	}

	add_action( 'boldlab_core_action_default_options_init', 'boldlab_core_add_woocommerce_options', boldlab_core_get_admin_options_map_position( 'woocommerce' ) );
}