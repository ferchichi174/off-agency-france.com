<?php boldlab_core_template_part( 'woocommerce/widgets/dropdown-cart', 'templates/parts/opener' ); ?>
<div class="qodef-m-dropdown">
	<div class="qodef-m-dropdown-inner">
		<?php if ( ! WC()->cart->is_empty() ) {
			boldlab_core_template_part( 'woocommerce/widgets/dropdown-cart', 'templates/parts/loop' );
			
			boldlab_core_template_part( 'woocommerce/widgets/dropdown-cart', 'templates/parts/order-details' );
			
			boldlab_core_template_part( 'woocommerce/widgets/dropdown-cart', 'templates/parts/button' );
		} else {
			boldlab_core_template_part( 'woocommerce/widgets/dropdown-cart', 'templates/posts-not-found' );
		} ?>
	</div>
</div>