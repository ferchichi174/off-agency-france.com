<?php

if ( ! function_exists( 'boldlab_core_add_product_list_variation_info_below' ) ) {
	function boldlab_core_add_product_list_variation_info_below( $variations ) {
		$variations['info-below'] = esc_html__( 'Info Below', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_product_list_layouts', 'boldlab_core_add_product_list_variation_info_below' );
}

if( ! function_exists( 'boldlab_core_register_shop_list_info_below_actions' ) ) {
	
	function boldlab_core_register_shop_list_info_below_actions() {
		// Add additional tags around results and ordering
		add_action( 'woocommerce_before_shop_loop', 'boldlab_core_add_results_and_ordering_holder', 15 ); // permission 5 is set because wc_print_notices hook is added on 10
		add_action( 'woocommerce_before_shop_loop', 'boldlab_core_add_results_and_ordering_holder_end', 40 ); // permission 40 is set because woocommerce_catalog_ordering hook is added on 30
		
		// Add additional tags around product list item
		add_action( 'woocommerce_before_shop_loop_item', 'boldlab_core_add_product_list_item_holder', 5 ); // permission 5 is set because woocommerce_template_loop_product_link_open hook is added on 10
		add_action( 'woocommerce_after_shop_loop_item', 'boldlab_core_add_product_list_item_holder_end', 30 ); // permission 30 is set because woocommerce_template_loop_add_to_cart hook is added on 10
		
		// Add additional tags around product list item image
		add_action( 'woocommerce_before_shop_loop_item_title', 'boldlab_core_add_product_list_item_image_holder', 5 ); // permission 5 is set because woocommerce_show_product_loop_sale_flash hook is added on 10
		add_action( 'woocommerce_before_shop_loop_item_title', 'boldlab_core_add_product_list_item_image_holder_end', 30 ); // permission 30 is set because woocommerce_template_loop_product_thumbnail hook is added on 10
		
		// Add additional tags around product list item content
		add_action( 'woocommerce_shop_loop_item_title', 'boldlab_core_add_product_list_item_content_holder', 5 ); // permission 5 is set because woocommerce_template_loop_product_title hook is added on 10
		add_action( 'woocommerce_after_shop_loop_item', 'boldlab_core_add_product_list_item_content_holder_end', 20 ); // permission 30 is set because woocommerce_template_loop_add_to_cart hook is added on 10
		
		// Add product categories on list
		add_action( 'woocommerce_shop_loop_item_title', 'boldlab_core_add_product_list_item_categories', 8 ); // permission 8 is set to be before woocommerce_template_loop_product_title hook it's added on 10
		
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
		
		add_action( 'woocommerce_after_shop_loop_item_title', 'boldlab_core_add_product_list_item_price_holder', 11 );
		add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 12 );
		add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 13 );
		add_action( 'woocommerce_after_shop_loop_item_title', 'boldlab_core_add_product_list_item_price_holder_end', 14 );
	}
	
	add_action( 'boldlab_core_action_shop_list_item_layout_info-below' , 'boldlab_core_register_shop_list_info_below_actions' );
}