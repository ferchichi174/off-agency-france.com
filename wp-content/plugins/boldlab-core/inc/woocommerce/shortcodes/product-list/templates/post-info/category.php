<?php

$categories = boldlab_core_woo_get_product_categories();
$display_category = isset( $display_category ) && $display_category !== 'no' ? true: false;

if ( ! empty( $categories ) && $display_category ) { ?>
	<div class="qodef-woo-product-categories"><?php echo wp_kses_post( $categories ); ?></div>
<?php } ?>