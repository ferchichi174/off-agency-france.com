<?php

$product = boldlab_core_woo_get_global_product();

if ( ! empty( $product ) && $product->is_on_sale() ) {
	echo boldlab_core_woo_sale_flash();
}

if ( ! empty( $product ) && ! $product->is_in_stock() ) {
	echo boldlab_core_get_out_of_stock_mark();
}

$product_id = $product->get_id();
$new = get_post_meta( $product_id, 'qodef_show_new_sign', true );

if ( $new === 'yes' ) {
	echo boldlab_core_get_new_mark();
}