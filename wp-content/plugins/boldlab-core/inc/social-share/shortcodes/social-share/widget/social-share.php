<?php

if ( ! function_exists( 'boldlab_core_add_social_share_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param $widgets array
	 *
	 * @return array
	 */
	function boldlab_core_add_social_share_widget( $widgets ) {
		$widgets[] = 'BoldlabCoreSocialShareWidget';
		
		return $widgets;
	}
	
	add_filter( 'boldlab_core_filter_register_widgets', 'boldlab_core_add_social_share_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BoldlabCoreSocialShareWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'boldlab_core_social_share'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'boldlab_core_social_share' );
				$this->set_name( esc_html__( 'Boldlab Social Share', 'boldlab-core' ) );
				$this->set_description( esc_html__( 'Add a social share element into widget areas', 'boldlab-core' ) );
			}
		}
		
		public function render( $atts ) {
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[boldlab_core_social_share $params]" ); // XSS OK
		}
	}
}