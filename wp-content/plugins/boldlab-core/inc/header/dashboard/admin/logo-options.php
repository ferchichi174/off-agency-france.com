<?php

if ( ! function_exists( 'boldlab_core_add_logo_options' ) ) {
	function boldlab_core_add_logo_options() {
		$qode_framework = qode_framework_get_framework_root();
		
		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BOLDLAB_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'logo',
				'icon'        => 'fa fa-cog',
				'title'       => esc_html__( 'Logo', 'boldlab-core' ),
				'description' => esc_html__( 'Logo Settings', 'boldlab-core' ),
				'layout'      => 'tabbed'
			)
		);
		
		if ( $page ) {
			
			$header_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-header',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Header Logo Options', 'boldlab-core' ),
					'description' => esc_html__( 'Set options for initial headers', 'boldlab-core' )
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_logo_height',
					'title'       => esc_html__( 'Logo Height', 'boldlab-core' ),
					'description' => esc_html__( 'Enter Logo Height', 'boldlab-core' ),
					'args'        => array(
						'suffix' => 'px'
					)
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_main',
					'title'       => esc_html__( 'Logo - Main', 'boldlab-core' ),
					'description' => esc_html__( 'Choose main logo image', 'boldlab-core' ),
					'default_value' => defined( 'BOLDLAB_ASSETS_ROOT' ) ? BOLDLAB_ASSETS_ROOT . '/img/logo.png' : '',
					'multiple'    => 'no'
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_dark',
					'title'       => esc_html__( 'Logo - Dark', 'boldlab-core' ),
					'description' => esc_html__( 'Choose dark logo image', 'boldlab-core' ),
					'multiple'    => 'no'
				)
			);
			
			$header_tab->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_light',
					'title'       => esc_html__( 'Logo - Light', 'boldlab-core' ),
					'description' => esc_html__( 'Choose light logo image', 'boldlab-core' ),
					'multiple'    => 'no'
				)
			);
			
			do_action( 'boldlab_core_action_after_header_logo_options_map', $page, $header_tab );
		}
	}
	
	add_action( 'boldlab_core_action_default_options_init', 'boldlab_core_add_logo_options', boldlab_core_get_admin_options_map_position( 'logo' ) );
}