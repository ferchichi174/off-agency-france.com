<?php

if ( ! function_exists( 'boldlab_core_add_vertical_header_options' ) ) {
	function boldlab_core_add_vertical_header_options( $page ) {

		$section = $page->add_section_element(
			array(
				'name'       => 'qodef_vertical_header_section',
				'title'      => esc_html__( 'Vertical Header', 'boldlab-core' ),
				'dependency' => array(
					'show' => array(
						'qodef_header_layout' => array(
							'values' => 'vertical',
							'default_value' => 'standard'
						)
					)
				)
			)
		);

		$section->add_field_element(
			array(
				'field_type'  => 'color',
				'name'        => 'qodef_vertical_header_background_color',
				'title'       => esc_html__( 'Header Background Color', 'boldlab-core' ),
				'description' => esc_html__( 'Enter header background color', 'boldlab-core' )
			)
		);

	}
	
	add_action( 'boldlab_core_action_after_header_options_map', 'boldlab_core_add_vertical_header_options' );
}