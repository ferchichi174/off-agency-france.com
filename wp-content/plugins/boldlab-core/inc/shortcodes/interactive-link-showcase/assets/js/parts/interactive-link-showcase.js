(function ($) {
    'use strict';

    $(document).ready(function () {
        qodefInteractiveLinkShowcase.init();
    });

    /**
     * Init interactive link showcase functionality
     */
    var qodefInteractiveLinkShowcase = {
        init: function () {
            this.holder = $('.qodef-interactive-link-showcase');

            if (this.holder.length) {
                this.holder.each(function () {
                    var $thisHolder = $(this),
                        $images = $thisHolder.find('.qodef-m-image'),
                        $titles = $thisHolder.find('.qodef-m-item-title');

                    var isUnderscore = qodef.body.hasClass('qodef--underscore');
                    isUnderscore && qodefInteractiveLinkShowcase.prepHtml();

                    if ($thisHolder.hasClass('qodef-layout--slider')) {
                        var $swiperSlider = new Swiper($thisHolder.find('.swiper-container'), {
                            loop: true,
                            slidesPerView: 'auto',
                            centeredSlides: true,
                            speed: 1000,
                            mousewheel: true,
                            init: false,
                        });

                        $thisHolder.waitForImages(function () {
                            $swiperSlider.init();
                        });

                        $swiperSlider.on('init', function () {
                            var i = 0;

                            var ready = function () {
                                $thisHolder.find('.swiper-slide-active').addClass('qodef--active');
                            }

                            TweenMax.staggerFromTo($thisHolder.find('.qodef-m-item-title'), .3, {
                                autoAlpha: 0,
                                x: -20,
                                skewX: -5,
                                transformOrigin: '0 0',
                            }, {
                                autoAlpha: 1,
                                x: 0,
                                skewX: 0,
                                ease: Power3.easeOut,
                                delay: .3,
                                onComplete: function (e) {
                                    i == 3 && ready();
                                    i++;
                                }
                            }, .1);

                            $images.eq(0).addClass('qodef--active');

                            $swiperSlider.on('slideChangeTransitionStart', function () {
                                var $swiperSlides = $thisHolder.find('.swiper-slide'),
                                    $activeSlideItem = $thisHolder.find('.swiper-slide-active');

                                $images.removeClass('qodef--active').eq($activeSlideItem.data('swiper-slide-index')).addClass('qodef--active');
                                $swiperSlides.removeClass('qodef--active');

                                $activeSlideItem.addClass('qodef--active');
                            });

                            $thisHolder.find('.swiper-slide').on('click', function (e) {
                                var $thisSwiperLink = $(this),
                                    $activeSlideItem = $thisHolder.find('.swiper-slide-active');

                                if (!$thisSwiperLink.hasClass('swiper-slide-active')) {
                                    e.preventDefault();
                                    e.stopImmediatePropagation();

                                    if (e.pageX < $activeSlideItem.offset().left) {
                                        $swiperSlider.slidePrev();
                                        return false;
                                    }

                                    if (e.pageX > $activeSlideItem.offset().left + $activeSlideItem.outerWidth()) {
                                        $swiperSlider.slideNext();
                                        return false;
                                    }
                                }
                            });

                            $thisHolder.addClass('qodef--init');
                        });
                    } else {
                        var $links = $thisHolder.find('.qodef-m-item');

                        var setActive = function () {
                            $images.eq(0).addClass('qodef--active');
                            $links.eq(0).addClass('qodef--active');
                        }

                        TweenMax.staggerFromTo($titles, .5, {
                            y: 60,
                            skewY: 10,
                            scaleY: 1.2,
                            autoAlpha: 0
                        }, {
                            y: 0,
                            skewY: 0,
                            scaleY: 1,
                            autoAlpha: 1,
                            ease: Power4.easeInOut,
                            delay: .5
                        }, .1, setActive)

                        $links.on('touchstart mouseenter', function (e) {
                            var $thisLink = $(this);

                            if (
                                !qodef.html.hasClass('touchevents') ||
                                (!$thisLink.hasClass('qodef--active') && qodef.windowWidth > 680)
                            ) {
                                e.preventDefault();
                                $images.removeClass('qodef--active').eq($thisLink.index()).addClass('qodef--active');
                                $links.removeClass('qodef--active').eq($thisLink.index()).addClass('qodef--active');
                            }


                        }).on('touchend mouseleave', function (e) {
                            var $thisLink = $(this);

                            if (
                                !qodef.html.hasClass('touchevents') ||
                                (!$thisLink.hasClass('qodef--active') && qodef.windowWidth > 680)
                            ) {
                                $links.removeClass('qodef--active').eq($thisLink.index()).addClass('qodef--active');
                                $images.removeClass('qodef--active').eq($thisLink.index()).addClass('qodef--active');
                            }

                        });

                        $thisHolder.addClass('qodef--init');

                    }
                });
            }
        },
        prepHtml: function () {
            var $titles = $('.qodef-m-item-title');

            $titles.each(function () {
                var $title = $(this),
                    titleText = $title.text();

                if (titleText.trim().endsWith('_')) {
                    $title.html(function (_, txt) {
                        return txt.replace('_', "<span class='qodef-underscore qodef--blinkable'>_</span>");
                    });
                }
            });
        }
    };

})(jQuery);