<?php

if ( ! function_exists( 'boldlab_core_add_interactive_link_showcase_variation_slider' ) ) {
	function boldlab_core_add_interactive_link_showcase_variation_slider( $variations ) {
		
		$variations['slider'] = esc_html__( 'Slider', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_interactive_link_showcase_layouts', 'boldlab_core_add_interactive_link_showcase_variation_slider' );
}
