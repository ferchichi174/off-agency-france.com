(function ($) {

    $(window).on('load', function () {
        qodefHighlight.init();
    });

    var qodefHighlight = {
        init: function () {
            var $sections = $('.qodef-highlight');

            $sections.length && $sections.each(function () {
                var $section = $(this);

                qodefHighlight.observe($section);
                qodefHighlight.setBg($section);
            });
        },
        setBg: function ($section) {
            var $t = $section.find('.qodef-highlight-text');
            h = $t.data('highlight') || false;

            h && $t.css({
                'background-image': '-webkit-gradient(linear,left top,left bottom,from(' + h + '),to(' + h + '))',
                'background-image': 'linear-gradient(' + h + ',' + h + ')'
            })
        },
        observe: function ($section) {
            !$section.closest('#qodef-side-area-inner').length && $section.appear(function () {
                qodefHighlight.animate($section);
            });

            $section.closest('#qodef-side-area-inner').length && $('.qodef-side-area-opener').one('click', function () {
                qodefHighlight.animate($section);
            })
        },
        animate: function ($section) {
            var $text = $section.find('.qodef-highlight-text');

            TweenMax.to($text, 1, {
                backgroundSize: '100% 100%',
                ease: Power2.easeInOut
            })
        }
    };

})(jQuery);