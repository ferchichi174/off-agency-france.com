<?php

if ( ! function_exists( 'boldlab_core_add_highlight_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param $widgets array
	 *
	 * @return array
	 */
	function boldlab_core_add_highlight_widget( $widgets ) {
		$widgets[] = 'BoldlabCoreHighlightWidget';
		
		return $widgets;
	}
	
	add_filter( 'boldlab_core_filter_register_widgets', 'boldlab_core_add_highlight_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BoldlabCoreHighlightWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'boldlab_core_highlight'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'boldlab_core_highlight' );
				$this->set_name( esc_html__( 'Boldlab Highlight', 'boldlab-core' ) );
				$this->set_description( esc_html__( 'Add a highlight element into widget areas', 'boldlab-core' ) );
			}
		}
		
		public function render( $atts ) {
			
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[boldlab_core_highlight $params]" ); // XSS OK
		}
	}
}
