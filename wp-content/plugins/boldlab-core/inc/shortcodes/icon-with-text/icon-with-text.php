<?php

if ( ! function_exists( 'boldlab_core_add_icon_with_text_shortcode' ) ) {
	/**
	 * Function that add shortcode into shortcodes list for registration
	 *
	 * @param $shortcodes array
	 *
	 * @return array
	 */
	function boldlab_core_add_icon_with_text_shortcode( $shortcodes ) {
		$shortcodes[] = 'BoldlabCoreIconWithTextShortcode';
		
		return $shortcodes;
	}
	
	add_filter( 'boldlab_core_filter_register_shortcodes', 'boldlab_core_add_icon_with_text_shortcode' );
}

if ( class_exists( 'BoldlabCoreShortcode' ) ) {
	class BoldlabCoreIconWithTextShortcode extends BoldlabCoreShortcode {
		
		public function __construct() {
			$this->set_layouts( apply_filters( 'boldlab_core_filter_icon_with_text_layouts', array() ) );
			
			$options_map = boldlab_core_get_variations_options_map( $this->get_layouts() );
			$default_value = $options_map['default_value'];
			
			$this->set_extra_options( apply_filters( 'boldlab_core_filter_icon_with_text_extra_options', array(), $default_value ) );
			
			parent::__construct();
		}
		
		public function map_shortcode() {
			$this->set_shortcode_path( BOLDLAB_CORE_SHORTCODES_URL_PATH . '/icon-with-text' );
			$this->set_base( 'boldlab_core_icon_with_text' );
			$this->set_name( esc_html__( 'Icon With Text', 'boldlab-core' ) );
			$this->set_description( esc_html__( 'Shortcode that adds icon with text element', 'boldlab-core' ) );
			$this->set_category( esc_html__( 'Boldlab Core', 'boldlab-core' ) );
			
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'custom_class',
				'title'      => esc_html__( 'Custom Class', 'boldlab-core' ),
			) );
			
			$options_map = boldlab_core_get_variations_options_map( $this->get_layouts() );
			
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'layout',
				'title'         => esc_html__( 'Layout', 'boldlab-core' ),
				'options'		=> $this->get_layouts(),
				'default_value' => $options_map['default_value'],
				'visibility'    => array( 'map_for_page_builder' => $options_map['visibility'] )
			) );
			
			$this->set_option( array(
				'field_type'    => 'text',
				'name'          => 'link',
				'title'         => esc_html__( 'Link', 'boldlab-core' ),
				'default_value' => ''
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'target',
				'title'         => esc_html__( 'Link Target', 'boldlab-core' ),
				'options'       => boldlab_core_get_select_type_options_pool( 'link_target' ),
				'default_value' => '_self'
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'icon_type',
				'title'         => esc_html__( 'Icon Type', 'boldlab-core' ),
				'options'       => array(
					'icon-pack'   => esc_html__( 'Icon Pack', 'boldlab-core' ),
					'custom-icon' => esc_html__( 'Custom Icon', 'boldlab-core' )
				),
				'default_value' => 'icon-pack',
				'group'         => esc_html__( 'Icon', 'boldlab-core' )
			) );
			$this->set_option( array(
				'field_type' => 'image',
				'name'       => 'custom_icon',
				'title'      => esc_html__( 'Custom Icon', 'boldlab-core' ),
				'group'      => esc_html__( 'Icon', 'boldlab-core' ),
				'dependency' => array(
					'show' => array(
						'icon_type' => array(
							'values'        => 'custom-icon',
							'default_value' => 'icon-pack'
						)
					)
				)
			) );
			$this->import_shortcode_options( array(
				'shortcode_base'    => 'boldlab_core_icon',
				'exclude'           => array( 'link', 'target', 'margin' ),
				'additional_params' => array(
					'group'      => esc_html__( 'Icon', 'boldlab-core' ),
					'dependency' => array(
						'show' => array(
							'icon_type' => array(
								'values'        => 'icon-pack',
								'default_value' => 'icon-pack'
							)
						)
					)
				)
			) );
			$this->set_option( array(
				'name'       => 'icon_animation',
				'title'      => esc_html__( 'Custom Icon Animation', 'boldlab-core' ),
				'field_type' => 'select',
				'options'    => boldlab_core_get_select_type_options_pool( 'yes_no' ),
				'group'      => esc_html__( 'Icon', 'boldlab-core' )
			) );
			$this->set_option( array(
				'name'       => 'loading_animation',
				'title'      => esc_html__( 'Loading Animation', 'boldlab-core' ),
				'field_type' => 'select',
				'options'    => boldlab_core_get_select_type_options_pool( 'yes_no' ),
				'group'      => esc_html__( 'Additional', 'boldlab-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'title',
				'title'      => esc_html__( 'Title', 'boldlab-core' ),
				'group'      => esc_html__( 'Content', 'boldlab-core' )
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'title_tag',
				'title'         => esc_html__( 'Title Tag', 'boldlab-core' ),
				'options'       => boldlab_core_get_select_type_options_pool( 'title_tag' ),
				'default_value' => 'h3',
				'group'         => esc_html__( 'Content', 'boldlab-core' )
			) );
			$this->set_option( array(
				'field_type' => 'color',
				'name'       => 'title_color',
				'title'      => esc_html__( 'Title Color', 'boldlab-core' ),
				'group'      => esc_html__( 'Content', 'boldlab-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'title_margin_top',
				'title'      => esc_html__( 'Title Margin Top', 'boldlab-core' ),
				'group'      => esc_html__( 'Content', 'boldlab-core' )
			) );
			$this->set_option( array(
				'field_type' => 'textarea',
				'name'       => 'text',
				'title'      => esc_html__( 'Text', 'boldlab-core' ),
				'group'      => esc_html__( 'Content', 'boldlab-core' )
			) );
			$this->set_option( array(
				'field_type' => 'color',
				'name'       => 'text_color',
				'title'      => esc_html__( 'Text Color', 'boldlab-core' ),
				'group'      => esc_html__( 'Content', 'boldlab-core' )
			) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'text_margin_top',
				'title'      => esc_html__( 'Text Margin Top', 'boldlab-core' ),
				'group'      => esc_html__( 'Content', 'boldlab-core' )
			) );
			$this->map_extra_options();
		}
		
		public function render( $options, $content = null ) {
			parent::render( $options );
			$atts = $this->get_atts();
			
			$atts['holder_classes'] = $this->get_holder_classes( $atts );
			$atts['title_styles']   = $this->get_title_styles( $atts );
			$atts['text_styles']    = $this->get_text_styles( $atts );
			$atts['icon_params']    = $this->generate_icon_params( $atts );
			
			return boldlab_core_get_template_part( 'shortcodes/icon-with-text', 'variations/' . $atts['layout'] . '/templates/' . $atts['layout'], '', $atts );
		}
		
		private function get_holder_classes( $atts ) {
			$holder_classes = $this->init_holder_classes();
			
			$holder_classes[] = 'qodef-icon-with-text';
			$holder_classes[] = ! empty( $atts['layout'] ) ? 'qodef-layout--' . $atts['layout'] : '';
			$holder_classes[] = ! empty( $atts['icon_type'] ) ? 'qodef--' . $atts['icon_type'] : '';
			$holder_classes[] = $atts['icon_animation'] !== 'no' ? 'qodef--icon-animation' : '';
			$holder_classes[] = $atts['loading_animation'] !== 'no' ? 'qodef--loading-animation' : '';
			
			$holder_classes = apply_filters( 'boldlab_core_filter_icon_with_text_variation_classes', $holder_classes, $atts );
			
			return implode( ' ', $holder_classes );
		}
		
		private function get_title_styles( $atts ) {
			$styles = array();
			
			if ( $atts['title_margin_top'] !== '' ) {
				$styles[] = 'margin-top: ' . intval( $atts['title_margin_top'] ) . 'px';
			}
			
			if ( ! empty( $atts['title_color'] ) ) {
				$styles[] = 'color: ' . $atts['title_color'];
			}
			
			return $styles;
		}
		
		private function get_text_styles( $atts ) {
			$styles = array();
			
			if ( $atts['text_margin_top'] !== '' ) {
				$styles[] = 'margin-top: ' . intval( $atts['text_margin_top'] ) . 'px';
			}
			
			if ( ! empty( $atts['text_color'] ) ) {
				$styles[] = 'color: ' . $atts['text_color'];
			}
			
			return $styles;
		}
		
		private function generate_icon_params( $atts ) {
			$params = $this->populate_imported_shortcode_atts( array(
				'shortcode_base' => 'boldlab_core_icon',
				'exclude'        => array( 'link', 'target', 'margin' ),
				'atts'           => $atts,
			) );
			
			return $params;
		}
	}
}