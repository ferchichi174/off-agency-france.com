(function ($) {
    'use strict';

    $(document).ready(function () {
        qodefIconWithText.init();
    });

    /**
     * Init icon with text animation functionality
     */
    var qodefIconWithText = {
        init: function () {
            this.holder = $('.qodef-icon-with-text');

            if (this.holder.length) {
                this.max = 360;
                this.min = 30;

                this.holder.each(function (i) {
                    var $thisHolder = $(this),
                        $icon = $thisHolder.find('.qodef-m-icon-wrapper');

                    $thisHolder.hasClass('qodef--icon-animation') && $thisHolder.appear(function () {
                        qodefIconWithText.animate($icon);
                    });

                    $thisHolder.hasClass('qodef--loading-animation') &&$thisHolder.appear(function () {
                        qodefIconWithText.loadingAnimation($thisHolder, i)
                    });
                });
            }
        },
        rotationVal: function () {
            return Math.floor(Math.random() * (1 + this.max - this.min) + this.min);
        },
        loadingAnimation: function ($section, i) {
            var delay = Modernizr.touchevents ? 0 : i * 0.1,
                $title = $section.find('.qodef-m-title'),
                $text = $section.find('.qodef-m-text');

            var timeline = new TimelineMax();

            timeline
                .to($section, .3, {
                    autoAlpha: 1,
                    delay: delay,
                })
                .from($title, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.8,
                    skewY: "20deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.6")
                .from($text, .5, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.8,
                    skewY: "20deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4");
        },
        animate: function ($icon) {
            TweenMax.to($icon, 1, {
                rotation: qodefIconWithText.rotationVal(),
                duration: 2,
                ease: Power4.easeInOut,
                onComplete: function () {
                    $icon.appear(function () {
                        qodefIconWithText.animate($icon);
                    });
                },
                delay: Math.random() * 2
            })
        }
    };

})(jQuery);