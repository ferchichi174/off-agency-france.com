<?php

if ( ! function_exists( 'boldlab_core_add_icon_with_text_variation_before_content' ) ) {
	function boldlab_core_add_icon_with_text_variation_before_content( $variations ) {
		
		$variations['before-content'] = esc_html__( 'Before Content', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_icon_with_text_layouts', 'boldlab_core_add_icon_with_text_variation_before_content' );
}
