<?php

if ( ! function_exists( 'boldlab_core_add_custom_font_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param $widgets array
	 *
	 * @return array
	 */
	function boldlab_core_add_custom_font_widget( $widgets ) {
		$widgets[] = 'BoldlabCoreCustomFontWidget';
		
		return $widgets;
	}
	
	add_filter( 'boldlab_core_filter_register_widgets', 'boldlab_core_add_custom_font_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BoldlabCoreCustomFontWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'boldlab_core_custom_font'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'boldlab_core_custom_font' );
				$this->set_name( esc_html__( 'Boldlab Custom Font', 'boldlab-core' ) );
				$this->set_description( esc_html__( 'Add a custom font element into widget areas', 'boldlab-core' ) );
			}
		}
		
		public function render( $atts ) {
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[boldlab_core_custom_font $params]" ); // XSS OK
		}
	}
}