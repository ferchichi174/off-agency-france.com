<?php

if ( ! function_exists( 'boldlab_core_add_button_variation_filled' ) ) {
	function boldlab_core_add_button_variation_filled( $variations ) {
		
		$variations['filled'] = esc_html__( 'Filled', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_button_layouts', 'boldlab_core_add_button_variation_filled' );
}
