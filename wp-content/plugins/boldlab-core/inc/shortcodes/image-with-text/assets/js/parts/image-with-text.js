(function ($) {
    'use strict';

    $(document).ready(function () {
        qodefImageWithTextTilt.init();
    });

    /**
     * Init tilt hover animation
     */
    var qodefImageWithTextTilt = {
        init: function () {
            var $iwts = $('.qodef-image-with-text');

            if ($iwts.length && !Modernizr.touchevents) {
                $iwts.each(function () {
                    var $trigger = $(this),
                        $targets = $trigger.find('.qodef-m-image');

                    $trigger
                        .on('mousemove touchmove', function (e) {
                            qodefImageWithTextTilt.enter(e, $trigger, $targets);
                        })
                        .on('mouseout touchend', function (e) {
                            qodefImageWithTextTilt.leave($targets);
                        });
                });

                $("#qodef-rev-slider-landing").length && qodefImageWithTextTilt.customBehavior($iwts);
            }
        },
        enter: function (e, tr, ta) {
            var aFx = 70,
                trF = 4,
                cH = tr.innerHeight(),
                cW = tr.innerWidth(),
                eX = (e.originalEvent.type === 'touchmove') ? e.originalEvent.touches[0].pageX : e.offsetX,
                eY = (e.originalEvent.type === 'touchmove') ? e.originalEvent.touches[0].pageY : e.offsetY;

            $.each(ta, function (i, el) {
                TweenMax.set($(el), {
                    transformOrigin: ((eX / (cW * trF) / 100 * 10000) + (trF * 10)) + '% ' + ((eY / (cH * trF) / 100 * 10000) + (trF * 10)) + '%',
                    transformPerspective: 1000 + (i * 500)
                });
                TweenMax.to($(el), 0.3, {
                    rotationX: ((eY - cH / 2) / aFx) - i * 2,
                    rotationY: ((eX - cW / 2) / aFx * -1) - i * 2,
                    y: (eY - (cH / 2)) / (30 - i * 20),
                    x: (eX - (cW / 2)) / (30 - i * 20)
                });
            });
        },
        leave: function (ta) {
            $.each(ta, function (i, el) {
                TweenMax.to($(el), .4, {
                    delay: .2,
                    y: 0,
                    x: 0,
                    rotationX: 0,
                    rotationY: 0,
                    transformPerspective: '1500'
                });
            });
        },
        customBehavior: function ($iwts) {
            //custom iwt behavior on landing page
            var animate = function () {
                TweenMax.staggerFrom($iwts.find('.qodef-m-title'), .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut,
                    delay: .6
                }, .1);

            }

            $(document).one('qodefScrolledTo', animate);
        }
    };

})(jQuery);