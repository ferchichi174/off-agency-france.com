<?php

if ( ! function_exists( 'boldlab_core_add_image_with_text_variation_text_below' ) ) {
	function boldlab_core_add_image_with_text_variation_text_below( $variations ) {
		
		$variations['text-below'] = esc_html__( 'Text Below', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_image_with_text_layouts', 'boldlab_core_add_image_with_text_variation_text_below' );
}
