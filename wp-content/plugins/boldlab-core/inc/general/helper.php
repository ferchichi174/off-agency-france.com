<?php

if ( ! function_exists( 'boldlab_core_set_general_styles' ) ) {
	/**
	 * Function that generates 404 page area inline styles
	 *
	 * @param $style string
	 *
	 * @return string
	 */
	function boldlab_core_set_general_styles( $style ) {
		$styles = array();
		
		$background_color      = boldlab_core_get_post_value_through_levels( 'qodef_page_background_color' );
		$background_image      = boldlab_core_get_post_value_through_levels( 'qodef_page_background_image' );
		$background_repeat     = boldlab_core_get_post_value_through_levels( 'qodef_page_background_repeat' );
		$background_size       = boldlab_core_get_post_value_through_levels( 'qodef_page_background_size' );
		$background_attachment = boldlab_core_get_post_value_through_levels( 'qodef_page_background_attachment' );
		
		if ( ! empty( $background_color ) ) {
			$styles['background-color'] = $background_color;
		}
		
		if ( ! empty( $background_image ) ) {
			$styles['background-image'] = 'url(' . esc_url( wp_get_attachment_image_url( $background_image, 'full' ) ) . ')';
		}
		
		if ( ! empty( $background_repeat ) ) {
			$styles['background-repeat'] = $background_repeat;
		}
		
		if ( ! empty( $background_size ) ) {
			$styles['background-size'] = $background_size;
		}
		
		if ( ! empty( $background_attachment ) ) {
			$styles['background-attachment'] = $background_attachment;
		}
		
		if ( ! empty( $styles ) ) {
			$style .= qode_framework_dynamic_style( 'body', $styles );
		}
		
		if ( boldlab_core_is_boxed_enabled() ) {
			$boxed_styles = array();
			
			$boxed_background_color = boldlab_core_get_post_value_through_levels( 'qodef_boxed_background_color' );
			
			if ( ! empty( $boxed_background_color ) ) {
				$boxed_styles['background-color'] = $boxed_background_color;
			}
			
			if ( ! empty( $boxed_styles ) ) {
				$style .= qode_framework_dynamic_style( '.qodef--boxed #qodef-page-wrapper', $boxed_styles );
			}
		}
		
		$page_content_style = array();
		
		$page_content_padding = boldlab_core_get_post_value_through_levels( 'qodef_page_content_padding' );
		if( ! empty ( $page_content_padding ) ) {
			$page_content_style['padding'] = $page_content_padding;
		}
		
		if ( ! empty( $page_content_style ) ) {
			$style .= qode_framework_dynamic_style( '#qodef-page-inner', $page_content_style );
		}
		
		$page_content_style_mobile = array();
		
		$page_content_padding_mobile = boldlab_core_get_post_value_through_levels( 'qodef_page_content_padding_mobile' );
		if( ! empty ( $page_content_padding_mobile ) ) {
			$page_content_style_mobile['padding'] = $page_content_padding_mobile;
		}
		
		if ( ! empty( $page_content_style_mobile ) ) {
			$style .= qode_framework_dynamic_style_responsive( '#qodef-page-inner', $page_content_style_mobile, '', '1024' );
		}
		
		return $style;
	}
	
	add_filter( 'boldlab_filter_add_inline_style', 'boldlab_core_set_general_styles' );
}

if ( ! function_exists( 'boldlab_core_set_general_main_color_styles' ) ) {
	/**
	 * Function that generates general main color inline styles
	 *
	 * @param $style string
	 *
	 * @return string
	 */
	function boldlab_core_set_general_main_color_styles( $style ) {
		$main_color = boldlab_core_get_post_value_through_levels( 'qodef_main_color' );
		
		if ( ! empty( $main_color ) ) {
			
			// Include main color selectors
			include_once 'main-color/main-color.php';
			
			$allowed_selectors = array(
				'color',
				'color_important',
				'background_color',
				'background_color_important',
				'border_color',
				'border_color_important',
				'fill_color',
				'fill_color_important',
				'stroke_color',
				'stroke_color_important',
			);
			
			foreach ( $allowed_selectors as $allowed_selector ) {
				$selector = $allowed_selector . '_selector';
				
				if ( isset( $$selector ) && ! empty( $$selector ) ) {
					
					if ( strpos( $selector, '_important' ) !== false ) {
						$attribute = str_replace( '_important', '', $allowed_selector );
						$color     = $main_color . '!important';
					} else {
						$attribute = $allowed_selector;
						$color     = $main_color;
					}
					
					$style .= qode_framework_dynamic_style( $$selector, array( str_replace( '_', '-', $attribute ) => $color ) );
				}
			}
		}
		
		return $style;
	}
	
	add_filter( 'boldlab_filter_add_inline_style', 'boldlab_core_set_general_main_color_styles' );
}

if ( ! function_exists( 'boldlab_core_is_boxed_enabled' ) ) {
	function boldlab_core_is_boxed_enabled() {
		return boldlab_core_get_post_value_through_levels( 'qodef_boxed' ) === 'yes';
	}
}

if ( ! function_exists( 'boldlab_core_add_general_options_body_classes' ) ) {
	function boldlab_core_add_general_options_body_classes( $classes ) {
		$content_width = boldlab_core_get_post_value_through_levels( 'qodef_content_width' );
		$content_behind_header = boldlab_core_get_post_value_through_levels( 'qodef_content_behind_header' );

		$classes[] = boldlab_core_is_boxed_enabled() ? 'qodef--boxed' : '';
		$classes[] = 'qodef-content-grid-' . $content_width;
		$classes[] = $content_behind_header == 'yes' ? 'qodef-content-behind-header' : '';

		return $classes;
	}
	
	add_filter( 'body_class', 'boldlab_core_add_general_options_body_classes' );
}

if ( ! function_exists( 'boldlab_core_add_boxed_wrapper_classes' ) ) {
	function boldlab_core_add_boxed_wrapper_classes( $classes ) {
		
		if ( boldlab_core_is_boxed_enabled() ) {
			$classes .= ' qodef-content-grid';
		}
		
		return $classes;
	}
	
	add_filter( 'boldlab_filter_page_wrapper_classes', 'boldlab_core_add_boxed_wrapper_classes' );
}