<?php

if ( ! function_exists( 'boldlab_core_is_back_to_top_enabled' ) ) {
	function boldlab_core_is_back_to_top_enabled() {
		return boldlab_core_get_post_value_through_levels( 'qodef_back_to_top' ) !== 'no';
	}
}

if ( ! function_exists( 'boldlab_core_add_back_to_top_to_body_classes' ) ) {
	function boldlab_core_add_back_to_top_to_body_classes( $classes ) {
		$classes[] = boldlab_core_is_back_to_top_enabled() ? 'qodef-back-to-top--enabled' : '';
		
		return $classes;
	}
	
	add_filter( 'body_class', 'boldlab_core_add_back_to_top_to_body_classes' );
}

if ( ! function_exists( 'boldlab_core_load_back_to_top' ) ) {
	/**
	 * Loads Back To Top HTML
	 */
	function boldlab_core_load_back_to_top() {
		
		if ( boldlab_core_is_back_to_top_enabled() ) {
			$parameters = array();
			
			boldlab_core_template_part( 'back-to-top', 'templates/back-to-top', '', $parameters );
		}
	}
	
	add_action( 'boldlab_action_before_wrapper_close_tag', 'boldlab_core_load_back_to_top' );
}