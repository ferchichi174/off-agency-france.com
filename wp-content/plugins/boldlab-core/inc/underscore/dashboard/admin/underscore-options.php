<?php

if ( ! function_exists( 'boldlab_core_add_underscore_options' ) ) {
	/**
	 * Function that adds general options for this module
	 */
	function boldlab_core_add_underscore_options( $page ) {
		
		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'underscore_animation',
					'title'         => esc_html__( 'Underscore animation', 'boldlab-core' ),
					'description'   => esc_html__( 'If set to yes, options will trigger theme predefined underscore animations', 'boldlab-core' ),
					'default_value' => 'no'
				)
			);
		}
	}
	
	add_action( 'boldlab_core_action_after_general_options_map', 'boldlab_core_add_underscore_options' );
}