(function ($) {

    $(window).on('load', function () {
        qodefUnderscore.init();
    });

    var qodefUnderscore = {
        init: function () {
            var isUnderscore = qodef.body.hasClass('qodef--underscore');

            isUnderscore && qodefUnderscore.prepHtml();
        },
        titleBlink: function ($title) {
            TweenMax.fromTo($title.find('.qodef--blinkable'), .5, {
                autoAlpha: 1,
            }, {
                autoAlpha: 0,
                yoyo: true,
                repeat: 1,
                delay: .1,
                ease: Power4.easeInOut,
                onComplete: function () {
                    qodefUnderscore.observeTitle($title);
                }
            })
        },
        observeTitle: function ($title) {
            $title.appear(function () {
                qodefUnderscore.titleBlink($title);
            })
        },
        prepTitles: function () {
            var $titles = $('.qodef-m-title');

            $titles.each(function () {
                var $title = $(this),
                    titleText = $title.text();

                if (titleText.trim().endsWith('_')) {
                    $title.html(function (_, txt) {
                        return txt.replace('_', "<span class='qodef-underscore qodef--blinkable'>_</span>");
                    });

                    qodefUnderscore.observeTitle($title);
                }
            });
        },
        prepButtons: function () {
            var $buttons = $('.qodef-button');
            
            $buttons.each(function () {
                var $button = $(this),
                    buttonText = $button.text();

                if (buttonText.trim().endsWith('_')) {
                    $button.html(function (_, txt) {
                        return txt.replace('_', "<span class='qodef-underscore qodef--blinkable'>_</span>");
                    });

                    $button.addClass('qodef--with-underscore');
                }
            });
        },
        prepHtml: function () {
            qodefUnderscore.prepTitles();
            qodefUnderscore.prepButtons();
        }

    };

})(jQuery);