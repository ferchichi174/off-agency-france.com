<?php

if ( ! function_exists( 'boldlab_core_add_instagram_list_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param $widgets array
	 *
	 * @return array
	 */
	function boldlab_core_add_instagram_list_widget( $widgets ) {
		$widgets[] = 'BoldlabCoreInstagramListWidget';
		
		return $widgets;
	}
	
	add_filter( 'boldlab_core_filter_register_widgets', 'boldlab_core_add_instagram_list_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BoldlabCoreInstagramListWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$this->set_widget_option(
				array(
					'field_type' => 'text',
					'name'       => 'widget_title',
					'title'      => esc_html__( 'Title', 'boldlab-core' )
				)
			);
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'boldlab_core_instagram_list'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'boldlab_core_instagram_list' );
				$this->set_name( esc_html__( 'Boldlab Instagram List', 'boldlab-core' ) );
				$this->set_description( esc_html__( 'Add a instagram list element into widget areas', 'boldlab-core' ) );
			}
		}
		
		public function render( $atts ) {
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[boldlab_core_instagram_list $params]" ); // XSS OK
		}
	}
}