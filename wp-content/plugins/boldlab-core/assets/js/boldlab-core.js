(function ($) {
	"use strict";
	
	window.qodefCore = {};
	
	$(document).ready(function () {
		qodefInlinePageStyle.init();
	});

	var qodefScroll = {
		disable: function(){
			if (window.addEventListener) {
				window.addEventListener('DOMMouseScroll', qodefScroll.preventDefaultValue, false);
			}

			window.onmousewheel = document.onmousewheel = qodefScroll.preventDefaultValue;
			document.onkeydown = qodefScroll.keyDown;
		},
		enable: function(){
			if (window.removeEventListener) {
				window.removeEventListener('DOMMouseScroll', qodefScroll.preventDefaultValue, false);
			}
			window.onmousewheel = document.onmousewheel = document.onkeydown = null;
		},
		preventDefaultValue: function(e){
			e = e || window.event;
			if (e.preventDefault) {
				e.preventDefault();
			}
			e.returnValue = false;
		},
		keyDown: function(e) {
			var keys = [37, 38, 39, 40];
			for (var i = keys.length; i--;) {
				if (e.keyCode === keys[i]) {
					qodefScroll.preventDefaultValue(e);
					return;
				}
			}
		}
	};
	
	qodefCore.qodefScroll = qodefScroll;
	
	var qodefPerfectScrollbar = {
		init: function (holder) {
			if (holder.length) {
				qodefPerfectScrollbar.qodefInitScroll(holder);
			}
		},
		qodefInitScroll: function (holder) {
			var $defaultParams = {
				wheelSpeed: 0.6,
				suppressScrollX: true
			};
			
			var ps = new PerfectScrollbar(holder.selector, $defaultParams);
			$(window).resize(function () {
				ps.update();
			});
		}
	};
	
	qodefCore.qodefPerfectScrollbar = qodefPerfectScrollbar;
	
	var qodefInlinePageStyle = {
		init: function () {
			this.holder = $('#boldlab-core-page-inline-style');
			
			if (this.holder.length) {
				var style = this.holder.data('style');
				
				if (style.length) {
					$('head').append('<style type="text/css">' + style + '</style>');
				}
			}
		}
	};

})(jQuery);
(function ($) {

    $(document).ready(function () {
        qodefBackToTop.init();
    });

    var qodefBackToTop = {
        init: function () {
            this.holder = $('#qodef-back-to-top');

            // Scroll To Top
            this.holder.on('click', function (e) {
                e.preventDefault();

                $('html, body').animate({scrollTop: 0}, $(window).scrollTop() / 6, 'swing');
            });

            qodefBackToTop.showHideBackToTop();
        },
        showHideBackToTop: function () {
            $(window).scroll(function () {
                var b = $(this).scrollTop(),
                    c = $(this).height(),
                    d;

                if (b > 0) {
                    d = b + c / 2;
                } else {
                    d = 1;
                }

                if (d < 1e3) {
                    qodefBackToTop.addClass('off');
                } else {
                    qodefBackToTop.addClass('on');
                }
            });
        },
        addClass: function (a) {
            this.holder.removeClass('qodef-back-to-top--off qodef-back-to-top--on');

            if (a === 'on') {
                this.holder.addClass('qodef-back-to-top--on');
            } else {
                this.holder.addClass('qodef-back-to-top--off');
            }
        }
    };

})(jQuery);

(function ($) {

    $(document).ready(function () {
        qodefCustomAnimations.init();
    });

    var qodefCustomAnimations = {
        init: function () {
            var $anims = $('.qodef-from-left, .qodef-from-right');

            $anims.length && $anims.each(function () {
                var $item = $(this);

                $item.css({
                    'visibility' : 'hidden',
                    'will-change': 'transform'
                });
                $item.hasClass('qodef-from-left') && qodefCustomAnimations.observe($item, 'left');
                $item.hasClass('qodef-from-right') && qodefCustomAnimations.observe($item, 'right');
            });
        },
        observe: function ($item, type) {
            $item.appear(function () {
                qodefCustomAnimations.animate($item, type);
            })
        },
        animate: function ($item, type) {
            TweenMax.from($item, .6, {
                autoAlpha: 0,
                x: type == 'left' ? -100 : 100,
                scaleX: 1.4,
                skewY: type == 'left' ? -10 : 10,
                ease: Quint.easeOut,
                delay: .2
            })
        }
    }
})(jQuery);
(function ($) {
	
	$(document).ready(function () {
		qodefFullscreenMenu.init();
	});
	
	var qodefFullscreenMenu = {
		init: function () {
			var $fullscreenMenuOpener = $('a.qodef-fullscreen-menu-opener'),
				$menuItems = $('.qodef-fullscreen-menu-holder nav ul li a');
			
			// Open popup menu
			$fullscreenMenuOpener.on('click', function (e) {
				e.preventDefault();
				
				if (!qodef.body.hasClass('qodef-fullscreen-menu--opened')) {
					qodefFullscreenMenu.openFullscreen();
					$(document).keyup(function (e) {
						if (e.keyCode === 27) {
							qodefFullscreenMenu.closeFullscreen();
						}
					});
				} else {
					qodefFullscreenMenu.closeFullscreen();
				}
			});
			
			//open dropdowns
			$menuItems.on('tap click', function (e) {
				var $thisItem = $(this);
				if ($thisItem.parent().hasClass('menu-item-has-children')) {
					e.preventDefault();
					qodefFullscreenMenu.clickItemWithChild($thisItem);
				} else if (($(this).attr('href') !== "http://#") && ($(this).attr('href') !== "#")) {
					qodefFullscreenMenu.closeFullscreen();
				}
			});
		},
		openFullscreen: function () {
			qodef.body.removeClass('qodef-fullscreen-menu-animate--out').addClass('qodef-fullscreen-menu--opened qodef-fullscreen-menu-animate--in');
			qodefCore.qodefScroll.disable();
		},
		closeFullscreen: function () {
			qodef.body.removeClass('qodef-fullscreen-menu--opened qodef-fullscreen-menu-animate--in').addClass('qodef-fullscreen-menu-animate--out');
			qodefCore.qodefScroll.enable();
			$("nav.qodef-fullscreen-menu ul.sub_menu").slideUp(200);
		},
		clickItemWithChild: function (thisItem) {
			var $thisItemParent = thisItem.parent(),
				$thisItemSubMenu = $thisItemParent.find('.sub-menu').first();
			
			if ($thisItemSubMenu.is(':visible')) {
				$thisItemSubMenu.slideUp(300);
			} else {
				$thisItemSubMenu.slideDown(300);
				$thisItemParent.siblings().find('.sub-menu').slideUp(400);
			}
		}
	};
	
})(jQuery);

(function($){

    $(document).ready(function () {
        qodefHeaderScrollAppearance.init();
    });

    var qodefHeaderScrollAppearance = {
        appearanceType: function(){
            return qodef.body.attr('class').indexOf('qodef-header-appearance--') !== -1 ? qodef.body.attr('class').match(/qodef-header-appearance--([\w]+)/)[1] : '';
        },
        init: function(){
            var appearanceType = this.appearanceType();

            if(appearanceType !== '' && appearanceType !== 'none'){
                window.qodef[appearanceType+"HeaderAppearance"]();
            }
        }
    };

})(jQuery);

(function ($) {
	"use strict";
	
	$(document).ready(function () {
        qodefMobileHeaderAppearance.init();
	});
	
	/*
	 **	Init mobile header functionality
	 */
	var qodefMobileHeaderAppearance = {
		init: function () {
			if (qodef.body.hasClass('qodef-mobile-header-appearance--sticky')) {

                var docYScroll1 = qodef.scroll,
                    displayAmount = qodefGlobal.vars.mobileHeaderHeight + qodefGlobal.vars.adminBarHeight,
                    $pageOuter = $('#qodef-page-outer');

                qodefMobileHeaderAppearance.showHideMobileHeader(docYScroll1, displayAmount, $pageOuter);
                $(window).scroll(function () {
                    qodefMobileHeaderAppearance.showHideMobileHeader(docYScroll1, displayAmount, $pageOuter);
                    docYScroll1 = qodef.scroll;
                });

                $(window).resize(function () {
                    $pageOuter.css('padding-top', 0);
                    qodefMobileHeaderAppearance.showHideMobileHeader(docYScroll1, displayAmount, $pageOuter);
                });
            }
		},
        showHideMobileHeader: function(docYScroll1, displayAmount,$pageOuter){
            if(qodef.windowWidth <= 1024) {
                if (qodef.scroll > displayAmount * 2) {
                    //set header to be fixed
                    qodef.body.addClass('qodef-mobile-header--sticky');

                    //add transition to it
                    setTimeout(function () {
                        qodef.body.addClass('qodef-mobile-header--sticky-animation');
                    }, 300); //300 is duration of sticky header animation

                    //add padding to content so there is no 'jumping'
                    $pageOuter.css('padding-top', qodefGlobal.vars.mobileHeaderHeight);
                } else {
                    //unset fixed header
                    qodef.body.removeClass('qodef-mobile-header--sticky');

                    //remove transition
                    setTimeout(function () {
                        qodef.body.removeClass('qodef-mobile-header--sticky-animation');
                    }, 300); //300 is duration of sticky header animation

                    //remove padding from content since header is not fixed anymore
                    $pageOuter.css('padding-top', 0);
                }

                if ((qodef.scroll > docYScroll1 && qodef.scroll > displayAmount) || (qodef.scroll < displayAmount * 3)) {
                    //show sticky header
                    qodef.body.removeClass('qodef-mobile-header--sticky-display');
                } else {
                    //hide sticky header
                    qodef.body.addClass('qodef-mobile-header--sticky-display');
                }
            }
        }
	};
	
})(jQuery);
(function ($) {

	$(document).ready(function () {
		qodefNavMenu.init();
		qodefNavMenu.wideDropdownPosition();
		qodefNavMenu.dropdownPosition();
	});

	var qodefNavMenu = {
		wideDropdownPosition: function () {
			var menuItems = $(".qodef-header-navigation > ul > li.qodef-menu-item--wide");

			if (menuItems.length) {
				menuItems.each(function (i) {
					var menuItem = $(this);
					var menuItemSubMenu = menuItem.find('.qodef-drop-down-second');

					if (menuItemSubMenu.length) {
						menuItemSubMenu.css('left', 0);

						var leftPosition = menuItemSubMenu.offset().left;

						if ($('body').hasClass('qodef--boxed')) {
							//boxed layout case
							var boxedWidth = $('.qodef-boxed .qodef-wrapper .qodef-wrapper-inner').outerWidth();
							leftPosition = leftPosition - (qodef.windowWidth - boxedWidth) / 2;
							menuItemSubMenu.css({'left': -leftPosition, 'width': boxedWidth});

						} else if ($('body').hasClass('qodef-drop-down-second--full-width')) {
							//wide dropdown full width case
							menuItemSubMenu.css({'left': -leftPosition});
						}
						else {
							//wide dropdown in grid case
							menuItemSubMenu.css({'left': -leftPosition + (qodef.windowWidth - menuItemSubMenu.width()) / 2});
						}
					}
				});
			}
		},
		dropdownPosition: function () {
			var $menuItems = $('.qodef-header-navigation > ul > li.qodef-menu-item--narrow.menu-item-has-children');

			if ($menuItems.length) {
				$menuItems.each(function (i) {
					var thisItem = $(this),
						menuItemPosition = thisItem.offset().left,
						dropdownHolder = thisItem.find('.qodef-drop-down-second'),
						dropdownMenuItem = dropdownHolder.find('.qodef-drop-down-second-inner ul'),
						dropdownMenuWidth = dropdownMenuItem.outerWidth(),
						menuItemFromLeft = $(window).width() - menuItemPosition;

					var dropDownMenuFromLeft;

					if (thisItem.find('li.menu-item-has-children').length > 0) {
						dropDownMenuFromLeft = menuItemFromLeft - dropdownMenuWidth;
					}

					dropdownHolder.removeClass('qodef-drop-down--right');
					dropdownMenuItem.removeClass('qodef-drop-down--right');
					if (menuItemFromLeft < dropdownMenuWidth || dropDownMenuFromLeft < dropdownMenuWidth) {
						dropdownHolder.addClass('qodef-drop-down--right');
						dropdownMenuItem.addClass('qodef-drop-down--right');
					}
				});
			}
		},
		init: function () {
			var $menuItems = $('.qodef-header-navigation > ul > li');

			$menuItems.each(function () {
				var thisItem = $(this);

				if (thisItem.find('.qodef-drop-down-second').length) {
					thisItem.waitForImages(function () {
						var dropDownHolder = thisItem.find('.qodef-drop-down-second'),
							dropDownHolderHeight = !qodef.menuDropdownHeightSet ? dropDownHolder.outerHeight() : 0;

						if (!qodef.menuDropdownHeightSet) {
							dropDownHolder.height(0);
						}

						if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
							thisItem.on("touchstart mouseenter", function () {
								dropDownHolder.css({
									'height': dropDownHolderHeight,
									'overflow': 'visible',
									'visibility': 'visible',
									'opacity': '1'
								});
							}).on("mouseleave", function () {
								dropDownHolder.css({
									'height': '0px',
									'overflow': 'hidden',
									'visibility': 'hidden',
									'opacity': '0'
								});
							});
						} else {
							if ($('body').hasClass('qodef-drop-down-second--animate-height')) {
								var animateConfig = {
									interval: 0,
									over: function () {
										setTimeout(function () {
											dropDownHolder.addClass('qodef-drop-down--start').css({
												'visibility': 'visible',
												'height': '0',
												'opacity': '1'
											});
											dropDownHolder.stop().animate({
												'height': dropDownHolderHeight
											}, 400, 'easeInOutQuint', function () {
												dropDownHolder.css('overflow', 'visible');
											});
										}, 100);
									},
									timeout: 100,
									out: function () {
										dropDownHolder.stop().animate({
											'height': '0',
											'opacity': 0
										}, 100, function () {
											dropDownHolder.css({
												'overflow': 'hidden',
												'visibility': 'hidden'
											});
										});

										dropDownHolder.removeClass('qodef-drop-down--start');
									}
								};

								thisItem.hoverIntent(animateConfig);
							} else {
								var config = {
									interval: 0,
									over: function () {
										setTimeout(function () {
											dropDownHolder.addClass('qodef-drop-down--start').stop().css({'height': dropDownHolderHeight});
										}, 150);
									},
									timeout: 150,
									out: function () {
										dropDownHolder.stop().css({'height': '0'}).removeClass('qodef-drop-down--start');
									}
								};
								thisItem.hoverIntent(config);
							}
						}
					});
				}
			});
		}
	};

})(jQuery);

(function ($) {
    "use strict";

    $(window).load(function () {
        qodefParallaxBackground.init();
    });

    /**
     * Init global parallax background functionality
     */
    var qodefParallaxBackground = {
        init: function (settings) {
            this.$sections = $('.qodef-parallax');

            // Allow overriding the default config
            $.extend(this.$sections, settings);

            var isSupported = !qodef.html.hasClass('touchevents') && !qodef.body.hasClass('qodef-browser--edge') && !qodef.body.hasClass('qodef-browser--ms-explorer');

            if (this.$sections.length && isSupported) {
                this.$sections.each(function () {
                    qodefParallaxBackground.ready($(this));
                });
            }
        },
        ready: function ($section) {
            $section.$imgWrapper = $section.find('.qodef-parallax-img-wrapper');
            $section.$img = $section.find('img');

            var h = $section.height(),
                imgWrapperH = $section.$imgWrapper.height();

            $section.movement = 100 * (imgWrapperH - h) / h / 2; //percentage (divided by 2 due to absolute img centering in CSS)

            $section.buffer = window.pageYOffset;
            $section.scrollBuffer = null;

            //calc and init loop
            requestAnimationFrame(function () {
                qodefParallaxBackground.calc($section);
                qodefParallaxBackground.loop($section);
            });

            //recalc
            $(window).on('resize', function () {
                qodefParallaxBackground.calc($section);
            });
        },
        calc: function ($section) {
            var wH = $section.$imgWrapper.height(),
                wW = $section.$imgWrapper.width();

            if ($section.$img.width() < wW) {
                $section.$img.css({
                    'width': '100%',
                    'height': 'auto'
                });
            }

            if ($section.$img.height() < wH) {
                $section.$img.css({
                    'height': '100%',
                    'width': 'auto',
                    'max-width': 'unset'
                });
            }
        },
        loop: function ($section) {
            if ($section.scrollBuffer === Math.round(window.pageYOffset)) {
                requestAnimationFrame(function () {
                    qodefParallaxBackground.loop($section);
                }); //repeat loop
                return false; //same scroll value, do nothing
            } else {
                $section.scrollBuffer = Math.round(window.pageYOffset);
            }

            var wH = window.outerHeight,
                sTop = $section.offset().top,
                sH = $section.height();

            if ($section.scrollBuffer + wH * 1.2 > sTop && $section.scrollBuffer < sTop + sH) {
                var delta = (Math.abs($section.scrollBuffer + wH - sTop) / (wH + sH)).toFixed(4), //coeff between 0 and 1 based on scroll amount
                    yVal = (delta * $section.movement).toFixed(4);

                if ($section.buffer !== delta) {
                    $section.$imgWrapper.css('transform', 'translate3d(0,' + yVal * 1.5 + '%, 0)');
                }

                $section.buffer = delta;
            }

            requestAnimationFrame(function () {
                qodefParallaxBackground.loop($section);
            }); //repeat loop
        }
    };

})(jQuery);
(function ($) {

    $(document).ready(function () {
        qodefSTC.init();
    });

    var qodefSTC = {
        data: [],
        DOM: [],
        init: function () {
            var revSliderLanding = $("#qodef-rev-slider-landing"); //unique ID for rev slider holder

            if (revSliderLanding.length && !Modernizr.touchevents && window.scrollY < window.innerHeight * 0.5) {
                qodefSTC.data.h = revSliderLanding.height();
                qodefSTC.data.offset = revSliderLanding.offset().top;
                qodefSTC.data.area = qodefSTC.data.h - qodefSTC.data.offset;
                qodefSTC.DOM.$revSlider = revSliderLanding.find('rs-module');
                qodefSTC.data.set = false;
                qodefSTC.data.done = false;

                qodefSTC.initEvents();
            }
        },
        scrollTo: function () {
            TweenMax.to(window, .8, {
                scrollTo: qodefSTC.data.area,
                ease: Quint.easeInOut,
                onStart: function() {
                    $(document).trigger('qodefScrolledTo');
                },
                onComplete: function () {
                    qodefSTC.data.done = true;
                }
            });
        },
        scrollHandler: function () {
            window.addEventListener('wheel', function (event) {
                if (!qodefSTC.data.done) {
                    event.preventDefault();
                    qodefSTC.scrollTo();
                }
            }, {
                passive: false
            });

            //scrollbar click
            $(document).on('mousedown', function (event) {
                if ($(window).outerWidth() <= event.pageX && !qodefSTC.data.done && $(window).scrollTop() == qodefSTC.data.offset) {
                    event.preventDefault();
                    qodefSTC.scrollTo();
                }
            });
        },
        initEvents: function () {
            //prevent mousewheel scroll
            window.addEventListener('wheel', function (event) {
                if (!qodefSTC.data.set) {
                    event.preventDefault();
                    window.scrollTop(0,0);
                }
            });

            //init
            qodefSTC.DOM.$revSlider.bind('revolution.slide.onloaded', function (e, data) {
                qodefSTC.data.set = true;
                qodefSTC.scrollHandler();
            });
        }
    };

})(jQuery);
(function ($) {
	
	$(document).ready(function () {
		qodefSideArea.init();
	});
	
	var qodefSideArea = {
		init: function () {
			var $sideAreaOpener = $('a.qodef-side-area-opener'),
				$sideAreaClose = $('#qodef-side-area-close'),
				$sideArea = $('#qodef-side-area');
				qodefSideArea.openerHoverColor($sideAreaOpener);
			// Open Side Area
			$sideAreaOpener.on('click', function (e) {
				e.preventDefault();
				
				if (!qodef.body.hasClass('qodef-side-area--opened')) {
					qodefSideArea.openSideArea();
					
					$(document).keyup(function (e) {
						if (e.keyCode === 27) {
							qodefSideArea.closeSideArea();
						}
					});
				} else {
					qodefSideArea.closeSideArea();
				}
			});
			
			$sideAreaClose.on('click', function (e) {
				e.preventDefault();
				qodefSideArea.closeSideArea();
			});
			
			if ($sideArea.length && typeof window.qodefCore.qodefPerfectScrollbar === 'object') {
				window.qodefCore.qodefPerfectScrollbar.init($sideArea);
			}
		},
		openSideArea: function () {
			var $wrapper = $('#qodef-page-wrapper');
			var currentScroll = $(window).scrollTop();

			$('.qodef-side-area-cover').remove();
			$wrapper.prepend('<div class="qodef-side-area-cover"/>');
			qodef.body.removeClass('qodef-side-area-animate--out').addClass('qodef-side-area--opened qodef-side-area-animate--in');

			$('.qodef-side-area-cover').on('click', function (e) {
				e.preventDefault();
				qodefSideArea.closeSideArea();
			});

			$(window).scroll(function () {
				if (Math.abs(qodef.scroll - currentScroll) > 400) {
					qodefSideArea.closeSideArea();
				}
			});

		},
		closeSideArea: function () {
			qodef.body.removeClass('qodef-side-area--opened qodef-side-area-animate--in').addClass('qodef-side-area-animate--out');
		},
		openerHoverColor: function (opener) {
			if (typeof opener.data('hover-color') !== 'undefined') {
				var hoverColor = opener.data('hover-color');
				var originalColor = opener.css('color');

				opener.on('mouseenter', function () {
					opener.css('color', hoverColor);
				});
				opener.on('mouseleave', function () {
					opener.css('color', originalColor);
				});
			}
		},
	};
	
})(jQuery);

(function ($) {

    $(window).on('load', function () {
        qodefUnderscore.init();
    });

    var qodefUnderscore = {
        init: function () {
            var isUnderscore = qodef.body.hasClass('qodef--underscore');

            isUnderscore && qodefUnderscore.prepHtml();
        },
        titleBlink: function ($title) {
            TweenMax.fromTo($title.find('.qodef--blinkable'), .5, {
                autoAlpha: 1,
            }, {
                autoAlpha: 0,
                yoyo: true,
                repeat: 1,
                delay: .1,
                ease: Power4.easeInOut,
                onComplete: function () {
                    qodefUnderscore.observeTitle($title);
                }
            })
        },
        observeTitle: function ($title) {
            $title.appear(function () {
                qodefUnderscore.titleBlink($title);
            })
        },
        prepTitles: function () {
            var $titles = $('.qodef-m-title');

            $titles.each(function () {
                var $title = $(this),
                    titleText = $title.text();

                if (titleText.trim().endsWith('_')) {
                    $title.html(function (_, txt) {
                        return txt.replace('_', "<span class='qodef-underscore qodef--blinkable'>_</span>");
                    });

                    qodefUnderscore.observeTitle($title);
                }
            });
        },
        prepButtons: function () {
            var $buttons = $('.qodef-button');
            
            $buttons.each(function () {
                var $button = $(this),
                    buttonText = $button.text();

                if (buttonText.trim().endsWith('_')) {
                    $button.html(function (_, txt) {
                        return txt.replace('_', "<span class='qodef-underscore qodef--blinkable'>_</span>");
                    });

                    $button.addClass('qodef--with-underscore');
                }
            });
        },
        prepHtml: function () {
            qodefUnderscore.prepTitles();
            qodefUnderscore.prepButtons();
        }

    };

})(jQuery);
(function ($) {
	
	$(document).ready(function () {
		qodefWooSelect2.init();
		qodefWooQuantityButtons.init();
	});
	
	var qodefWooSelect2 = {
		init: function (settings) {
			this.holder = [];
			this.holder.push({holder: $('#qodef-woo-page .woocommerce-ordering select'), options: {minimumResultsForSearch: Infinity}});
			this.holder.push({holder: $('#qodef-woo-page .variations select'), options: {}});
			this.holder.push({holder: $('#qodef-woo-page #calc_shipping_country'), options: {}});
			this.holder.push({holder: $('#qodef-woo-page .shipping select#calc_shipping_state'), options: {}});
			this.holder.push({holder: $('.widget.widget_archive select'), options: {}});
			this.holder.push({holder: $('.widget.widget_categories select'), options: {}});
			this.holder.push({holder: $('.widget.widget_text select'), options: {}});
			
			// Allow overriding the default config
			$.extend(this.holder, settings);
			
			if (typeof this.holder === 'object') {
				$.each(this.holder, function (key, value) {
					qodefWooSelect2.createSelect2(value.holder, value.options);
				});
			}
		},
		createSelect2: function (holder, options) {
			if (typeof holder.select2 === 'function') {
				holder.select2(options);
			}
		}
	};
	
	var qodefWooQuantityButtons = {
		init: function() {
			$(document).on('click', '.qodef-quantity-minus, .qodef-quantity-plus', function (e) {
				e.stopPropagation();
				
				var button = $(this),
					inputField = button.siblings('.qodef-quantity-input'),
					step = parseFloat(inputField.data('step')),
					max = parseFloat(inputField.data('max')),
					minus = false,
					inputValue = parseFloat(inputField.val()),
					newInputValue;
				
				if (button.hasClass('qodef-quantity-minus')) {
					minus = true;
				}
				
				if (minus) {
					newInputValue = inputValue - step;
					if (newInputValue >= 1) {
						inputField.val(newInputValue);
					} else {
						inputField.val(0);
					}
				} else {
					newInputValue = inputValue + step;
					if (max === undefined) {
						inputField.val(newInputValue);
					} else {
						if (newInputValue >= max) {
							inputField.val(max);
						} else {
							inputField.val(newInputValue);
						}
					}
				}
				
				inputField.trigger('change');
			});
		}
	};
	
})(jQuery);

(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefAccordion.init();
	});
	
	/**
	 * Init accordion functionality
	 */
	var qodefAccordion = {
		init: function () {
			this.holder = $('.qodef-accordion');
			
			if (this.holder.length) {
				this.holder.each(function () {
					var $thisHolder = $(this);
					
					if ($thisHolder.hasClass('qodef-behavior--accordion')) {
						qodefAccordion.initAccordion($thisHolder);
					}
					
					if ($thisHolder.hasClass('qodef-behavior--toggle')) {
						qodefAccordion.initToggle($thisHolder);
					}
					
					$thisHolder.addClass('qodef--init');
				});
			}
		},
		initAccordion: function (accordion) {
			accordion.accordion({
				animate: 300,
				collapsible: true,
				active: 0,
				icons: "",
				heightStyle: "content"
			});
		},
		initToggle: function (toggle) {
			var $toggleAccordionTitle = toggle.find('.qodef-accordion-title'),
				$toggleAccordionContent = $toggleAccordionTitle.next();
			
			toggle.addClass("accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset");
			$toggleAccordionTitle.addClass("ui-accordion-header ui-state-default ui-corner-top ui-corner-bottom");
			$toggleAccordionContent.addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").hide();
			
			$toggleAccordionTitle.each(function () {
				var $thisTitle = $(this);
				
				$thisTitle.hover(function () {
					$thisTitle.toggleClass("ui-state-hover");
				});
				
				$thisTitle.on('click', function () {
					$thisTitle.toggleClass('ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom');
					$thisTitle.next().toggleClass('ui-accordion-content-active').slideToggle(400);
				});
			});
		}
	};
	
})(jQuery);
(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefButton.init();
	});
	
	/**
	 * Init button hover functionality
	 */
	var qodefButton = {
		init: function () {
			this.buttons = $('.qodef-button');
			
			if (this.buttons.length) {
				this.buttons.each(function () {
					var $thisButton = $(this);
					
					qodefButton.buttonHoverColor($thisButton);
					qodefButton.buttonHoverBgColor($thisButton);
					qodefButton.buttonHoverBorderColor($thisButton);
				});
			}
		},
		buttonHoverColor: function (button) {
			if (typeof button.data('hover-color') !== 'undefined') {
				var hoverColor = button.data('hover-color');
				var originalColor = button.css('color');
				
				button.on('mouseenter', function () {
					qodefButton.changeColor(button, 'color', hoverColor);
				});
				button.on('mouseleave', function () {
					qodefButton.changeColor(button, 'color', originalColor);
				});
			}
		},
		buttonHoverBgColor: function (button) {
			if (typeof button.data('hover-background-color') !== 'undefined') {
				var hoverBackgroundColor = button.data('hover-background-color');
				var originalBackgroundColor = button.css('background-color');
				
				button.on('mouseenter', function () {
					qodefButton.changeColor(button, 'background-color', hoverBackgroundColor);
				});
				button.on('mouseleave', function () {
					qodefButton.changeColor(button, 'background-color', originalBackgroundColor);
				});
			}
		},
		buttonHoverBorderColor: function (button) {
			if (typeof button.data('hover-border-color') !== 'undefined') {
				var hoverBorderColor = button.data('hover-border-color');
				var originalBorderColor = button.css('borderTopColor');
				
				button.on('mouseenter', function () {
					qodefButton.changeColor(button, 'border-color', hoverBorderColor);
				});
				button.on('mouseleave', function () {
					qodefButton.changeColor(button, 'border-color', originalBorderColor);
				});
			}
		},
		changeColor: function (button, cssProperty, color) {
			button.css(cssProperty, color);
		}
	};
	
})(jQuery);
(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefCardsGallery.init();
	});
	
	/**
	 * Init progress bar shortcode functionality
	 */
	var qodefCardsGallery = {
		init: function () {
			this.holder = $('.qodef-cards-gallery');
			
			if (this.holder.length) {
				this.holder.each(function () {
					var $thisHolder = $(this);
					qodefCardsGallery.initCards( $thisHolder );
					qodefCardsGallery.initBundle( $thisHolder );
				});
			}
		},
		initCards: function (holder) {
			var cards = holder.find('.qodef-m-card');
			cards.each(function () {
				var card = $(this);
				
				card.on('click', function () {
					if (!cards.last().is(card)) {
						card.addClass('qodef-out qodef-animating').siblings().addClass('qodef-animating-siblings');
						card.detach();
						card.insertAfter(cards.last());
						
						setTimeout(function () {
							card.removeClass('qodef-out');
						}, 200);
						
						setTimeout(function () {
							card.removeClass('qodef-animating').siblings().removeClass('qodef-animating-siblings');
						}, 1200);
						
						cards = holder.find('.qodef-m-card');
						
						return false;
					}
				});
				
				
			});
		},
		initBundle: function(holder) {
			if (holder.hasClass('qodef-animation--bundle') && !qodef.html.hasClass('touch')) {
				holder.appear(function () {
					holder.addClass('qodef-appeared');
					holder.find('img').one('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function () {
						$(this).addClass('qodef-animation-done');
					});
				}, {accX: 0, accY: -100});
			}
		}
	};
	
})(jQuery);
(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefCountdown.init();
	});
	
	/**
	 * Init countdown functionality
	 */
	var qodefCountdown = {
		init: function () {
			this.countdowns = $('.qodef-countdown');
			
			if (this.countdowns.length) {
				this.countdowns.each(function () {
					var $thisCountdown = $(this),
						countdownElement = $thisCountdown.find('.qodef-m-date'),
						options = qodefCountdown.generateOptions($thisCountdown);
					qodefCountdown.initCountdown(countdownElement, options);
				});
			}
		},
		generateOptions: function(countdown) {
			var options = {};
			options.date = countdown.data('date') !== undefined ? countdown.data('date') : null;
			
			options.weekLabel = countdown.data('week-label') !== undefined ? countdown.data('week-label') : '';
			options.weekLabelPlural = countdown.data('week-label-plural') !== undefined ? countdown.data('week-label-plural') : '';
			
			options.dayLabel = countdown.data('day-label') !== undefined ? countdown.data('day-label') : '';
			options.dayLabelPlural = countdown.data('day-label-plural') !== undefined ? countdown.data('day-label-plural') : '';
			
			options.hourLabel = countdown.data('hour-label') !== undefined ? countdown.data('hour-label') : '';
			options.hourLabelPlural = countdown.data('hour-label-plural') !== undefined ? countdown.data('hour-label-plural') : '';
			
			options.minuteLabel = countdown.data('minute-label') !== undefined ? countdown.data('minute-label') : '';
			options.minuteLabelPlural = countdown.data('minute-label-plural') !== undefined ? countdown.data('minute-label-plural') : '';
			
			options.secondLabel = countdown.data('second-label') !== undefined ? countdown.data('second-label') : '';
			options.secondLabelPlural = countdown.data('second-label-plural') !== undefined ? countdown.data('second-label-plural') : '';
			
			return options;
			
		},
        initCountdown: function (countdownElement, options) {
            var $weekHTML = '<span class="qodef-digit-wrapper"><span class="qodef-digit">%w</span><span class="qodef-label">' + '%!w:' + options.weekLabel + ',' + options.weekLabelPlural + ';</span></span>';
            var $dayHTML = '<span class="qodef-digit-wrapper"><span class="qodef-digit">%d</span><span class="qodef-label">' + '%!d:' + options.dayLabel + ',' + options.dayLabelPlural + ';</span></span>';
            var $hourHTML = '<span class="qodef-digit-wrapper"><span class="qodef-digit">%H</span><span class="qodef-label">' + '%!H:' + options.hourLabel + ',' + options.hourLabelPlural + ';</span></span>';
            var $minuteHTML = '<span class="qodef-digit-wrapper"><span class="qodef-digit">%M</span><span class="qodef-label">' + '%!M:' + options.minuteLabel + ',' + options.minuteLabelPlural + ';</span></span>';
            var $secondHTML = '<span class="qodef-digit-wrapper"><span class="qodef-digit">%S</span><span class="qodef-label">' + '%!S:' + options.secondLabel + ',' + options.secondLabelPlural + ';</span></span>';

            countdownElement.countdown(options.date, function(event) {
                $(this).html(event.strftime($weekHTML + $dayHTML + $hourHTML + $minuteHTML + $secondHTML));
            });
        }
	};
	
})(jQuery);
(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefCounter.init();
	});
	
	/**
	 * Init counter functionality
	 */
	var qodefCounter = {
		init: function () {
			this.counters = $('.qodef-counter');
			
			if (this.counters.length) {
				this.counters.each(function () {
					var $thisCounter = $(this),
						counterElement = $thisCounter.find('.qodef-m-digit'),
						options = qodefCounter.generateOptions($thisCounter);

					$thisCounter.appear(function () {
						qodefCounter.counterScript(counterElement, options);
					});
				});
			}
		},
		generateOptions: function(counter) {
			var options = {};
			options.start = counter.data('start-digit') !== undefined ? counter.data('start-digit') : 0;
			options.end = counter.data('end-digit') !== undefined ? counter.data('end-digit') : null;
			options.duration = counter.data('duration') !== undefined ? counter.data('duration') : 1500;
			options.text = counter.data('digit-label') !== undefined ? counter.data('digit-label') : '';
			
			options.start = parseFloat(options.start);
			options.end = parseFloat(options.end);
			options.duration = parseFloat(options.duration);
			return options;
		},
		counterScript: function (counterElement, options) {
			counterElement.countTo({
				speed: options.duration,
				from: options.start,
				to: options.end,
				label: options.text,
				separator: '',
				thousand: '',
				refreshInterval: 40,
				onUpdate: function() {
					
				},
				onComplete: function() {
					
				}
			});
		}
	};
	
})(jQuery);
(function ($) {
    'use strict';

    $(document).ready(function () {
        qodefFrameSlider.init();
    });

    var qodefFrameSlider = {
        init: function () {
            this.holder = $('.qodef-frame-slider-holder');

            if (this.holder.length) {
                this.holder.each(function () {
                    var $thisHolder = $(this);
                    qodefFrameSlider.createSlider($thisHolder);
                });
            }
        },

        createSlider: function (holder) {
            var $swiperHolder = holder.find('.qodef-m-swiper'),
                $sliderHolder = holder.find('.qodef-m-items'),
                $pagination = holder.find('.swiper-pagination');

            var $swiper = new Swiper($swiperHolder, {
                slidesPerView: 'auto',
                centeredSlides: true,
                spaceBetween: 0,
                autoplay: false,
                loop: true,
                speed: 800,
                pagination: {
                    el: $pagination,
                    type: 'bullets',
                    clickable: true
                },
                on: {
                    init: function () {
                        qodefFrameSlider.slidesNav($swiperHolder, this);
                        setTimeout(function () {
                            $sliderHolder.addClass('qodef-swiper--initialized');
                        }, 1500);
                    }
                }
            });
        },
        slidesNav: function ($holder, swiper) {
            $holder.on('click', '.swiper-slide-next', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                swiper.slideNext();
            });
            $holder.on('click', '.swiper-slide-prev', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                swiper.slidePrev();
            });
        }
    };

})(jQuery);
(function ($) {
	"use strict";
	
	$(document).ready(function () {
		qodefGoogleMap.init();
	});
	
	var qodefGoogleMap = {
		init: function () {
			this.holder = $('.qodef-google-map');
			
			if (this.holder.length) {
				this.holder.each(function () {
					if (qodefCore.qodefGoogleMap !== undefined) {
						qodefCore.qodefGoogleMap.initMap($(this).find('.qodef-m-map'));
					}
				});
			}
		}
	};
	
})(jQuery);
(function ($) {

    $(window).on('load', function () {
        qodefHighlight.init();
    });

    var qodefHighlight = {
        init: function () {
            var $sections = $('.qodef-highlight');

            $sections.length && $sections.each(function () {
                var $section = $(this);

                qodefHighlight.observe($section);
                qodefHighlight.setBg($section);
            });
        },
        setBg: function ($section) {
            var $t = $section.find('.qodef-highlight-text');
            h = $t.data('highlight') || false;

            h && $t.css({
                'background-image': '-webkit-gradient(linear,left top,left bottom,from(' + h + '),to(' + h + '))',
                'background-image': 'linear-gradient(' + h + ',' + h + ')'
            })
        },
        observe: function ($section) {
            !$section.closest('#qodef-side-area-inner').length && $section.appear(function () {
                qodefHighlight.animate($section);
            });

            $section.closest('#qodef-side-area-inner').length && $('.qodef-side-area-opener').one('click', function () {
                qodefHighlight.animate($section);
            })
        },
        animate: function ($section) {
            var $text = $section.find('.qodef-highlight-text');

            TweenMax.to($text, 1, {
                backgroundSize: '100% 100%',
                ease: Power2.easeInOut
            })
        }
    };

})(jQuery);
(function ($) {
    'use strict';

    $(document).ready(function () {
        qodefIcon.init();
    });

    /**
     * Init button hover functionality
     */
    var qodefIcon = {
        init: function () {
            this.icons = $('.qodef-icon-holder');

            if (this.icons.length) {
                this.icons.each(function () {
                    var $thisIcon = $(this);

                    qodefIcon.iconHoverColor($thisIcon);
                    qodefIcon.iconHoverBgColor($thisIcon);
                    qodefIcon.iconHoverBorderColor($thisIcon);
                });
            }
        },
        iconHoverColor: function (iconHolder) {
            if (typeof iconHolder.data('hover-color') !== 'undefined') {
                var spanHolder = iconHolder.find('span');
                var originalColor = spanHolder.css('color');
                var hoverColor = iconHolder.data('hover-color');

                iconHolder.on('mouseenter', function () {
                    qodefIcon.changeColor(spanHolder, 'color', hoverColor);
                });
                iconHolder.on('mouseleave', function () {
                    qodefIcon.changeColor(spanHolder, 'color', originalColor);
                });
            }
        },
        iconHoverBgColor: function (iconHolder) {
            if (typeof iconHolder.data('hover-background-color') !== 'undefined') {
                var hoverBackgroundColor = iconHolder.data('hover-background-color');
                var originalBackgroundColor = iconHolder.css('background-color');

                iconHolder.on('mouseenter', function () {
                    qodefIcon.changeColor(iconHolder, 'background-color', hoverBackgroundColor);
                });
                iconHolder.on('mouseleave', function () {
                    qodefIcon.changeColor(iconHolder, 'background-color', originalBackgroundColor);
                });
            }
        },
        iconHoverBorderColor: function (iconHolder) {
            if (typeof iconHolder.data('hover-border-color') !== 'undefined') {
                var hoverBorderColor = iconHolder.data('hover-border-color');
                var originalBorderColor = iconHolder.css('borderTopColor');

                iconHolder.on('mouseenter', function () {
                    qodefIcon.changeColor(iconHolder, 'border-color', hoverBorderColor);
                });
                iconHolder.on('mouseleave', function () {
                    qodefIcon.changeColor(iconHolder, 'border-color', originalBorderColor);
                });
            }
        },
        changeColor: function (iconElement, cssProperty, color) {
            iconElement.css(cssProperty, color);
        }
    };

    qodefCore.qodefIcon = qodefIcon;

})(jQuery);
(function ($) {
    'use strict';

    $(document).ready(function () {
        qodefIconWithText.init();
    });

    /**
     * Init icon with text animation functionality
     */
    var qodefIconWithText = {
        init: function () {
            this.holder = $('.qodef-icon-with-text');

            if (this.holder.length) {
                this.max = 360;
                this.min = 30;

                this.holder.each(function (i) {
                    var $thisHolder = $(this),
                        $icon = $thisHolder.find('.qodef-m-icon-wrapper');

                    $thisHolder.hasClass('qodef--icon-animation') && $thisHolder.appear(function () {
                        qodefIconWithText.animate($icon);
                    });

                    $thisHolder.hasClass('qodef--loading-animation') &&$thisHolder.appear(function () {
                        qodefIconWithText.loadingAnimation($thisHolder, i)
                    });
                });
            }
        },
        rotationVal: function () {
            return Math.floor(Math.random() * (1 + this.max - this.min) + this.min);
        },
        loadingAnimation: function ($section, i) {
            var delay = Modernizr.touchevents ? 0 : i * 0.1,
                $title = $section.find('.qodef-m-title'),
                $text = $section.find('.qodef-m-text');

            var timeline = new TimelineMax();

            timeline
                .to($section, .3, {
                    autoAlpha: 1,
                    delay: delay,
                })
                .from($title, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.8,
                    skewY: "20deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.6")
                .from($text, .5, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.8,
                    skewY: "20deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4");
        },
        animate: function ($icon) {
            TweenMax.to($icon, 1, {
                rotation: qodefIconWithText.rotationVal(),
                duration: 2,
                ease: Power4.easeInOut,
                onComplete: function () {
                    $icon.appear(function () {
                        qodefIconWithText.animate($icon);
                    });
                },
                delay: Math.random() * 2
            })
        }
    };

})(jQuery);
(function ($) {
    'use strict';

    $(document).ready(function () {
        qodefImageWithTextTilt.init();
    });

    /**
     * Init tilt hover animation
     */
    var qodefImageWithTextTilt = {
        init: function () {
            var $iwts = $('.qodef-image-with-text');

            if ($iwts.length && !Modernizr.touchevents) {
                $iwts.each(function () {
                    var $trigger = $(this),
                        $targets = $trigger.find('.qodef-m-image');

                    $trigger
                        .on('mousemove touchmove', function (e) {
                            qodefImageWithTextTilt.enter(e, $trigger, $targets);
                        })
                        .on('mouseout touchend', function (e) {
                            qodefImageWithTextTilt.leave($targets);
                        });
                });

                $("#qodef-rev-slider-landing").length && qodefImageWithTextTilt.customBehavior($iwts);
            }
        },
        enter: function (e, tr, ta) {
            var aFx = 70,
                trF = 4,
                cH = tr.innerHeight(),
                cW = tr.innerWidth(),
                eX = (e.originalEvent.type === 'touchmove') ? e.originalEvent.touches[0].pageX : e.offsetX,
                eY = (e.originalEvent.type === 'touchmove') ? e.originalEvent.touches[0].pageY : e.offsetY;

            $.each(ta, function (i, el) {
                TweenMax.set($(el), {
                    transformOrigin: ((eX / (cW * trF) / 100 * 10000) + (trF * 10)) + '% ' + ((eY / (cH * trF) / 100 * 10000) + (trF * 10)) + '%',
                    transformPerspective: 1000 + (i * 500)
                });
                TweenMax.to($(el), 0.3, {
                    rotationX: ((eY - cH / 2) / aFx) - i * 2,
                    rotationY: ((eX - cW / 2) / aFx * -1) - i * 2,
                    y: (eY - (cH / 2)) / (30 - i * 20),
                    x: (eX - (cW / 2)) / (30 - i * 20)
                });
            });
        },
        leave: function (ta) {
            $.each(ta, function (i, el) {
                TweenMax.to($(el), .4, {
                    delay: .2,
                    y: 0,
                    x: 0,
                    rotationX: 0,
                    rotationY: 0,
                    transformPerspective: '1500'
                });
            });
        },
        customBehavior: function ($iwts) {
            //custom iwt behavior on landing page
            var animate = function () {
                TweenMax.staggerFrom($iwts.find('.qodef-m-title'), .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut,
                    delay: .6
                }, .1);

            }

            $(document).one('qodefScrolledTo', animate);
        }
    };

})(jQuery);
(function ($) {

    $(document).ready(function () {
        qodefInfoSection.init();
    });

    var qodefInfoSection = {
        init: function () {
            var $sections = $('.qodef-info-section.qodef--with-animation');

            $sections.length && $sections.each(function () {
                var $section = $(this);

                qodefInfoSection.observe($section);
            });
        },
        observe: function ($section) {
            $section.appear(function () {
                qodefInfoSection.animate($section);
            })
        },
        animate: function ($section) {
            var delay = Modernizr.touchevents ? 0 : $section.data('delay'),
                $title = $section.find('.qodef-m-title'),
                $text = $section.find('.qodef-m-text'),
                $button = $section.find('.qodef-m-button'),
                $bgText = $section.find('.qodef-m-background-text-text');

            var timeline = new TimelineMax();

            timeline
                .to($section, .6, {
                    autoAlpha: 1,
                    delay: delay / 1000
                })
                .from($title, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.6")
                .from($text, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($button, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($bgText, 1.2, {
                    scale: 1.1,
                    autoAlpha: 0,
                    ease: Power2.easeOut
                }, "-=.3")

        }
    };

})(jQuery);
(function ($) {
    'use strict';

    $(document).ready(function () {
        qodefInteractiveLinkShowcase.init();
    });

    /**
     * Init interactive link showcase functionality
     */
    var qodefInteractiveLinkShowcase = {
        init: function () {
            this.holder = $('.qodef-interactive-link-showcase');

            if (this.holder.length) {
                this.holder.each(function () {
                    var $thisHolder = $(this),
                        $images = $thisHolder.find('.qodef-m-image'),
                        $titles = $thisHolder.find('.qodef-m-item-title');

                    var isUnderscore = qodef.body.hasClass('qodef--underscore');
                    isUnderscore && qodefInteractiveLinkShowcase.prepHtml();

                    if ($thisHolder.hasClass('qodef-layout--slider')) {
                        var $swiperSlider = new Swiper($thisHolder.find('.swiper-container'), {
                            loop: true,
                            slidesPerView: 'auto',
                            centeredSlides: true,
                            speed: 1000,
                            mousewheel: true,
                            init: false,
                        });

                        $thisHolder.waitForImages(function () {
                            $swiperSlider.init();
                        });

                        $swiperSlider.on('init', function () {
                            var i = 0;

                            var ready = function () {
                                $thisHolder.find('.swiper-slide-active').addClass('qodef--active');
                            }

                            TweenMax.staggerFromTo($thisHolder.find('.qodef-m-item-title'), .3, {
                                autoAlpha: 0,
                                x: -20,
                                skewX: -5,
                                transformOrigin: '0 0',
                            }, {
                                autoAlpha: 1,
                                x: 0,
                                skewX: 0,
                                ease: Power3.easeOut,
                                delay: .3,
                                onComplete: function (e) {
                                    i == 3 && ready();
                                    i++;
                                }
                            }, .1);

                            $images.eq(0).addClass('qodef--active');

                            $swiperSlider.on('slideChangeTransitionStart', function () {
                                var $swiperSlides = $thisHolder.find('.swiper-slide'),
                                    $activeSlideItem = $thisHolder.find('.swiper-slide-active');

                                $images.removeClass('qodef--active').eq($activeSlideItem.data('swiper-slide-index')).addClass('qodef--active');
                                $swiperSlides.removeClass('qodef--active');

                                $activeSlideItem.addClass('qodef--active');
                            });

                            $thisHolder.find('.swiper-slide').on('click', function (e) {
                                var $thisSwiperLink = $(this),
                                    $activeSlideItem = $thisHolder.find('.swiper-slide-active');

                                if (!$thisSwiperLink.hasClass('swiper-slide-active')) {
                                    e.preventDefault();
                                    e.stopImmediatePropagation();

                                    if (e.pageX < $activeSlideItem.offset().left) {
                                        $swiperSlider.slidePrev();
                                        return false;
                                    }

                                    if (e.pageX > $activeSlideItem.offset().left + $activeSlideItem.outerWidth()) {
                                        $swiperSlider.slideNext();
                                        return false;
                                    }
                                }
                            });

                            $thisHolder.addClass('qodef--init');
                        });
                    } else {
                        var $links = $thisHolder.find('.qodef-m-item');

                        var setActive = function () {
                            $images.eq(0).addClass('qodef--active');
                            $links.eq(0).addClass('qodef--active');
                        }

                        TweenMax.staggerFromTo($titles, .5, {
                            y: 60,
                            skewY: 10,
                            scaleY: 1.2,
                            autoAlpha: 0
                        }, {
                            y: 0,
                            skewY: 0,
                            scaleY: 1,
                            autoAlpha: 1,
                            ease: Power4.easeInOut,
                            delay: .5
                        }, .1, setActive)

                        $links.on('touchstart mouseenter', function (e) {
                            var $thisLink = $(this);

                            if (
                                !qodef.html.hasClass('touchevents') ||
                                (!$thisLink.hasClass('qodef--active') && qodef.windowWidth > 680)
                            ) {
                                e.preventDefault();
                                $images.removeClass('qodef--active').eq($thisLink.index()).addClass('qodef--active');
                                $links.removeClass('qodef--active').eq($thisLink.index()).addClass('qodef--active');
                            }


                        }).on('touchend mouseleave', function (e) {
                            var $thisLink = $(this);

                            if (
                                !qodef.html.hasClass('touchevents') ||
                                (!$thisLink.hasClass('qodef--active') && qodef.windowWidth > 680)
                            ) {
                                $links.removeClass('qodef--active').eq($thisLink.index()).addClass('qodef--active');
                                $images.removeClass('qodef--active').eq($thisLink.index()).addClass('qodef--active');
                            }

                        });

                        $thisHolder.addClass('qodef--init');

                    }
                });
            }
        },
        prepHtml: function () {
            var $titles = $('.qodef-m-item-title');

            $titles.each(function () {
                var $title = $(this),
                    titleText = $title.text();

                if (titleText.trim().endsWith('_')) {
                    $title.html(function (_, txt) {
                        return txt.replace('_', "<span class='qodef-underscore qodef--blinkable'>_</span>");
                    });
                }
            });
        }
    };

})(jQuery);
(function ($) {
	'use strict';

	$(document).ready(function () {
		qodefProgressBar.init();
	});

	/**
	 * Init progress bar shortcode functionality
	 */
	var qodefProgressBar = {
		init: function () {
			this.holder = $('.qodef-progress-bar');

			if (this.holder.length) {
				this.holder.each(function () {
					var $thisHolder = $(this),
						layout = $thisHolder.data('layout'),
						data = qodefProgressBar.generateBarData($thisHolder, layout),
						container = '#qodef-m-canvas-' + $thisHolder.data('rand-number'),
						number = $thisHolder.data('number') / 100;
						
					switch (layout) {
						case 'circle':
							qodefProgressBar.initCircleBar(container, data, number);
							break;
						case 'semi-circle':
							qodefProgressBar.initSemiCircleBar(container, data, number);
							break;
						case 'line':
							number = $thisHolder.data('number');
							container = $thisHolder.find('.qodef-m-canvas');
							data = qodefProgressBar.generateLineData($thisHolder, layout, number);
							qodefProgressBar.initLineBar(container, data);
							break;
						case 'custom':
							container = "#" + $thisHolder.data('custom-shape-id');
							qodefProgressBar.initCustomBar(container, data, number);
							break;
					}
				});
			}
		},
		generateBarData: function (thisBar, layout) {
			var activeWidth = thisBar.data('active-line-width');
			var activeColor = thisBar.data('active-line-color');
			var inactiveWidth = thisBar.data('inactive-line-width');
			var inactiveColor = thisBar.data('inactive-line-color');
			var easing = 'linear';
			var duration = 1400;
			var textColor = thisBar.data('text-color');

			return {
				strokeWidth: activeWidth,
				color: activeColor,
				trailWidth: inactiveWidth,
				trailColor: inactiveColor,
				easing: easing,
				duration: duration,
				svgStyle: {
					width: '100%',
					height: '100%'
				},
				text: {
					style: {
						color: textColor
					},
					autoStyleContainer: false
				},
				from: {
					color: inactiveColor
				},
				to: {
					color: activeColor
				},
				step: function (state, bar) {
					if (layout !== 'custom') {
						bar.setText(Math.round(bar.value() * 100) + '%');
					}
				}
			};
		},
		generateLineData: function (thisBar, layout, number) {
			var height = thisBar.data('active-line-width');
			var activeColor = thisBar.data('active-line-color');
			var inactiveHeight = thisBar.data('inactive-line-width');
			var inactiveColor = thisBar.data('inactive-line-color');
			var duration = 800;
			var textColor = thisBar.data('text-color');

			return {
				percentage: number,
				duration: duration,
				fillBackgroundColor: activeColor,
				backgroundColor: inactiveColor,
				height: height,
				inactiveHeight: inactiveHeight,
				followText: true,
				textColor: textColor
			};
		},
		initCircleBar: function (container, data, number) {
			var bar = new ProgressBar.Circle(container, data);

			$(container).appear(function () {
				bar.animate(number);
			});
		},
		initSemiCircleBar: function (container, data, number) {
			var bar = new ProgressBar.SemiCircle(container, data);

			$(container).appear(function () {
				bar.animate(number);
			});
		},
		initCustomBar: function (container, data, number) {
			var bar = new ProgressBar.Path(container, data);
			bar.set(0);

			$(container).appear(function () {
				bar.animate(number);
			});
		},
		initLineBar: function (container, data) {
			$(container).appear(function () {
				container.LineProgressbar(data);
			});
		}
	};

})(jQuery);
(function ($) {
	'use strict';

	$(document).ready(function () {
		qodefSwappingImageGallery.init();
	});

	/**
	 * Init progress bar shortcode functionality
	 */
	var qodefSwappingImageGallery = {
		init: function () {
			this.holder = $('.qodef-swapping-image-gallery');

			if (this.holder.length) {
				this.holder.each(function () {
					var $thisHolder = $(this);

					qodefSwappingImageGallery.createSlider($thisHolder);
					qodefSwappingImageGallery.observe($thisHolder);
				});
			}
		},
		observe: function($holder) {
			var $thumbnails = qodefSwappingImageGallery.getItems($holder);

			$holder.appear(function () {
				qodefSwappingImageGallery.loadingAnimation($thumbnails);
			});
		},
		loadingAnimation: function ($thumbnails) {
			TweenMax.staggerFromTo($thumbnails, .4, {
				autoAlpha: 0,
				y: 100,
				scaleY: 1.8,
				skewY: "20deg",
				transformOrigin: "top",
			}, {
				autoAlpha: 1,
				y: 0,
				scaleY: 1,
				skewY: 0,
				ease: Power4.easeInOut
			}, .1);
		},
        getItems: function ($holder) {
            var $items = $holder.find('.qodef-m-thumbnail').sort(function (a, b) {
                return 0.5 - Math.random()
            })
            return $items;
        },
		createSlider: function (holder) {
			var swiperHolder = holder.find('.qodef-m-image-holder');
			var paginationHolder = holder.find('.qodef-m-thumbnails-holder .qodef-grid-inner');
			var spaceBetween = 0;
			var slidesPerView = 1;
			var centeredSlides = false;
			var loop = false;
			var autoplay = false;
			var speed = 800;

			var $swiper = new Swiper(swiperHolder, {
				slidesPerView: slidesPerView,
				centeredSlides: centeredSlides,
				spaceBetween: spaceBetween,
				autoplay: autoplay,
				loop: loop,
				speed: speed,
				pagination: {
					el: paginationHolder,
					type: 'custom',
					clickable: true,
					bulletClass: 'qodef-m-thumbnail'
				},
				on: {
					init: function () {
						swiperHolder.addClass('qodef-swiper--initialized');
						paginationHolder.find('.qodef-m-thumbnail').eq(0).addClass('qodef--active');
					},
					slideChange: function slideChange() {
						var swiper = this;
						var activeIndex = swiper.activeIndex;
						paginationHolder.find('.qodef--active').removeClass('qodef--active');
						paginationHolder.find('.qodef-m-thumbnail').eq(activeIndex).addClass('qodef--active');
					}
				}
			});
		}
	};

})(jQuery);
(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefTabs.init();
	});
	
	/**
	 * Init tabs functionality
	 */
	var qodefTabs = {
		init: function () {
			this.holder = $('.qodef-tabs');
			
			if (this.holder.length) {
				this.holder.each(function () {
					qodefTabs.initTabs($(this));
				});
			}
		},
		initTabs: function (tabs) {
			tabs.children('.qodef-tabs-content').each(function (index) {
				index = index + 1;
				
				var $that = $(this),
					link = $that.attr('id'),
					$navItem = $that.parent().find('.qodef-tabs-navigation li:nth-child(' + index + ') a'),
					navLink = $navItem.attr('href');
				
				link = '#' + link;
				
				if (link.indexOf(navLink) > -1) {
					$navItem.attr('href', link);
				}
			});
			
			tabs.addClass('qodef--init').tabs();
		}
	};
	
})(jQuery);
(function ($) {
	
	$(document).ready(function () {
		qodefVerticalNavMenu.init();
	});
	
	/**
	 * Function object that represents vertical menu area.
	 * @returns {{init: Function}}
	 */
	var qodefVerticalNavMenu = {
		initNavigation: function (verticalMenuObject) {
			var verticalNavObject = verticalMenuObject.find('.qodef-header-vertical-navigation');
			
			if (verticalNavObject.hasClass('qodef-vertical-drop-down--below')) {
				qodefVerticalNavMenu.dropdownClickToggle(verticalNavObject);
			} else if (verticalNavObject.hasClass('qodef-vertical-drop-down--side')) {
				qodefVerticalNavMenu.dropdownFloat(verticalNavObject);
			}
		},
		dropdownClickToggle: function (verticalNavObject) {
			var menuItems = verticalNavObject.find('ul li.menu-item-has-children');
			
			menuItems.each(function () {
				var elementToExpand = $(this).find(' > .qodef-drop-down-second, > ul');
				var menuItem = this;
				var dropdownOpener = $(this).find('> a');
				var slideUpSpeed = 'fast';
				var slideDownSpeed = 'slow';
				
				dropdownOpener.on('click tap', function (e) {
					e.preventDefault();
					e.stopPropagation();
					
					if (elementToExpand.is(':visible')) {
						$(menuItem).removeClass('qodef-menu-item--open');
						elementToExpand.slideUp(slideUpSpeed);
					} else if (dropdownOpener.parent().parent().children().hasClass('qodef-menu-item--open') && dropdownOpener.parent().parent().parent().hasClass('qodef-vertical-menu')) {
						$(this).parent().parent().children().removeClass('qodef-menu-item--open');
						$(this).parent().parent().children().find(' > .qodef-drop-down-second').slideUp(slideUpSpeed);
						
						$(menuItem).addClass('qodef-menu-item--open');
						elementToExpand.slideDown(slideDownSpeed);
					} else {
						
						if (!$(this).parents('li').hasClass('qodef-menu-item--open')) {
							menuItems.removeClass('qodef-menu-item--open');
							menuItems.find(' > .qodef-drop-down-second, > ul').slideUp(slideUpSpeed);
						}
						
						if ($(this).parent().parent().children().hasClass('qodef-menu-item--open')) {
							$(this).parent().parent().children().removeClass('qodef-menu-item--open');
							$(this).parent().parent().children().find(' > .qodef-drop-down-second, > ul').slideUp(slideUpSpeed);
						}
						
						$(menuItem).addClass('qodef-menu-item--open');
						elementToExpand.slideDown(slideDownSpeed);
					}
				});
			});
		},
		dropdownFloat: function (verticalNavObject) {
			var menuItems = verticalNavObject.find('ul li.menu-item-has-children');
			var allDropdowns = menuItems.find(' > .qodef-drop-down-second > .qodef-drop-down-second-inner > ul, > ul');
			
			menuItems.each(function () {
				var elementToExpand = $(this).find(' > .qodef-drop-down-second > .qodef-drop-down-second-inner > ul, > ul');
				var menuItem = this;
				
				if (Modernizr.touch) {
					var dropdownOpener = $(this).find('> a');
					
					dropdownOpener.on('click tap', function (e) {
						e.preventDefault();
						e.stopPropagation();
						
						if (elementToExpand.hasClass('qodef-float--open')) {
							elementToExpand.removeClass('qodef-float--open');
							$(menuItem).removeClass('qodef-menu-item--open');
						} else {
							if (!$(this).parents('li').hasClass('qodef-menu-item--open')) {
								menuItems.removeClass('qodef-menu-item--open');
								allDropdowns.removeClass('qodef-float--open');
							}
							
							elementToExpand.addClass('qodef-float--open');
							$(menuItem).addClass('qodef-menu-item--open');
						}
					});
				} else {
					//must use hoverIntent because basic hover effect doesn't catch dropdown
					//it doesn't start from menu item's edge
					$(this).hoverIntent({
						over: function () {
							elementToExpand.addClass('qodef-float--open');
							$(menuItem).addClass('qodef-menu-item--open');
						},
						out: function () {
							elementToExpand.removeClass('qodef-float--open');
							$(menuItem).removeClass('qodef-menu-item--open');
						},
						timeout: 300
					});
				}
			});
		},
		verticalAreaScrollable: function (verticalMenuObject) {
			return verticalMenuObject.hasClass('qodef-with-scroll');
		},
		initVerticalAreaScroll: function (verticalMenuObject) {
			if (qodefVerticalNavMenu.verticalAreaScrollable(verticalMenuObject)) {
				window.qodefCore.qodefPerfectScrollbar.init(verticalMenuObject);
			}
		},
		init: function () {
			var $verticalMenuObject = $('.qodef-header--vertical #qodef-page-header');
			
			if ($verticalMenuObject.length) {
				qodefVerticalNavMenu.initNavigation($verticalMenuObject);
				qodefVerticalNavMenu.initVerticalAreaScroll($verticalMenuObject);
			}
		}
	};
	
})(jQuery);
(function($){

    var fixedHeaderAppearance = {
        showHideHeader: function($pageOuter, $header){
            if(qodef.windowWidth > 1024) {
                if (qodef.scroll <= 0) {
                    qodef.body.removeClass('qodef-header--fixed-display');
                    $pageOuter.css('padding-top', '0');
                    $header.css('margin-top', '0');
                } else {
                    qodef.body.addClass('qodef-header--fixed-display');
                    $pageOuter.css('padding-top', parseInt(qodefGlobal.vars.headerHeight + qodefGlobal.vars.topAreaHeight) + 'px');
                    $header.css('margin-top', parseInt(qodefGlobal.vars.topAreaHeight) + 'px');
                }
            }
        },
        init: function(){
            var $pageOuter = $('#qodef-page-outer'),
                $header = $('#qodef-page-header');
            fixedHeaderAppearance.showHideHeader($pageOuter, $header);
            $(window).scroll(function() {
                fixedHeaderAppearance.showHideHeader($pageOuter, $header);
            });
            $(window).resize(function() {
                $pageOuter.css('padding-top', '0');
                fixedHeaderAppearance.showHideHeader($pageOuter, $header);
            });
        }
    };
    qodef.fixedHeaderAppearance = fixedHeaderAppearance.init;

})(jQuery);

(function($){

    var stickyHeaderAppearance = {
        displayAmount : function(){
            if(qodefGlobal.vars.qodefStickyHeaderScrollAmount !== 0){
                return parseInt(qodefGlobal.vars.qodefStickyHeaderScrollAmount);
            } else {
                return parseInt(qodefGlobal.vars.headerHeight + qodefGlobal.vars.adminBarHeight);
            }
        },
        showHideHeader: function(displayAmount){

            if(qodef.scroll < displayAmount) {
                qodef.body.removeClass('qodef-header--sticky-display');
            }else{
                qodef.body.addClass('qodef-header--sticky-display');
            }
        },
        init: function(){
            var displayAmount = stickyHeaderAppearance.displayAmount();

            stickyHeaderAppearance.showHideHeader(displayAmount);
            $(window).scroll(function() {
                stickyHeaderAppearance.showHideHeader(displayAmount);
            });
        }
    };
    qodef.stickyHeaderAppearance = stickyHeaderAppearance.init;

})(jQuery);

(function ($) {
    "use strict";

    $(document).ready(function(){
        qodefSearchCoversHeader.init();
    });

    var qodefSearchCoversHeader = {
        init: function(){
            var $searchOpener = $('a.qodef-search-opener'),
                $searchForm = $('form.qodef-search-cover'),
                $searchClose = $('.qodef-search-close');

            if ($searchOpener.length && $searchForm.length) {
                $searchOpener.on('click', function (e) {
                    e.preventDefault();
                    qodefSearchCoversHeader.openCoversHeader($searchForm);

                });
                $searchClose.on('click', function (e) {
                    e.preventDefault();
                    qodefSearchCoversHeader.closeCoversHeader($searchForm);
                });
            }
        },
        openCoversHeader: function($searchForm){
            qodef.body.addClass('qodef-covers-search--opened qodef-covers-search--fadein');
            qodef.body.removeClass('qodef-covers-search--fadeout');

            setTimeout(function () {
                $searchForm.find('.qodef-search-field').focus();
            }, 600);
        },
        closeCoversHeader: function($searchForm){
            qodef.body.removeClass('qodef-covers-search--opened qodef-covers-search--fadein');
            qodef.body.addClass('qodef-covers-search--fadeout');

            setTimeout(function () {
                $searchForm.find('.qodef-search-field').val('');
                $searchForm.find('.qodef-search-field').blur();
                qodef.body.removeClass('qodef-covers-search--fadeout');
            }, 300);
        }
    };

})(jQuery);

(function($) {
    "use strict";

    $(document).ready(function(){
        qodefSearchFullscreen.init();
    });

	var qodefSearchFullscreen = {
	    init: function(){
            var $searchOpener = $('a.qodef-search-opener'),
                $searchHolder = $('.qodef-fullscreen-search-holder'),
                $searchClose = $('.qodef-search-close');

            if ($searchOpener.length && $searchHolder.length) {
                $searchOpener.on('click', function (e) {
                    e.preventDefault();
                    if(qodef.body.hasClass('qodef-fullscreen-search--opened')){
                        qodefSearchFullscreen.closeFullscreen($searchHolder);
                    }else{
                        qodefSearchFullscreen.openFullscreen($searchHolder);
                    }
                });
                $searchClose.on('click', function (e) {
                    e.preventDefault();
                    qodefSearchFullscreen.closeFullscreen($searchHolder);
                });

                //Close on escape
                $(document).keyup(function (e) {
                    if (e.keyCode === 27) { //KeyCode for ESC button is 27
                        qodefSearchFullscreen.closeFullscreen($searchHolder);
                    }
                });
            }
        },
        openFullscreen: function($searchHolder){
            qodef.body.removeClass('qodef-fullscreen-search--fadeout');
	        qodef.body.addClass('qodef-fullscreen-search--opened qodef-fullscreen-search--fadein');

            setTimeout(function () {
                $searchHolder.find('.qodef-search-field').focus();
            }, 900);

            qodefCore.qodefScroll.disable();
        },
        closeFullscreen: function($searchHolder){
            qodef.body.removeClass('qodef-fullscreen-search--opened qodef-fullscreen-search--fadein');
            qodef.body.addClass('qodef-fullscreen-search--fadeout');

            setTimeout(function () {
                $searchHolder.find('.qodef-search-field').val('');
                $searchHolder.find('.qodef-search-field').blur();
                qodef.body.removeClass('qodef-fullscreen-search--fadeout');
            }, 300);

            qodefCore.qodefScroll.enable();
        }
    };

})(jQuery);

(function ($) {
    'use strict';

    $(document).ready(function () {
        inverseFade.init();
    });

    /**
     * Init clients grid animation functionality
     */
    var inverseFade = {
        init: function () {
            this.holder = $('.qodef-clients-list.qodef-hover-animation--inverse-fade');
            if (this.holder.length) {
                this.holder.each(function () {
                    var $thisHolder = $(this),
                        $items = inverseFade.getItems($thisHolder);

                    !$thisHolder.hasClass('qodef-swiper-container') && $thisHolder.appear(function () {
                        inverseFade.loadingAnimation($items);
                    })

                    $items.each(function () {
                        var $item = $(this);
                        inverseFade.hoverAnimation($thisHolder, $item, $items);
                    });
                });
            }
        },
        loadingAnimation: function ($items) {
            TweenMax.staggerTo($items, .4, {
                autoAlpha: 1,
            }, .05);
        },
        hoverAnimation: function ($thisHolder, $item, $items) {
            $item
                .on('mouseenter', function (e) {
                    $item.addClass('qodef--hovered');
                    TweenMax.to($item, .2, {
                        autoAlpha: 1,
                        overwrite: 1
                    })
                    TweenMax.fromTo($item, .25, {
                        y: 0
                    }, {
                        y: -5,
                        ease: Power4.easeInOut,
                        yoyo: true,
                        repeat: 1,
                    })
                    TweenMax.staggerTo($items.not('.qodef--hovered'), .2, {
                        autoAlpha: .5,
                        overwrite: 1,
                        ease: Power4.easeOut,
                    }, .015);
                });

            $item
                .on('mouseleave', function () {
                    $item.removeClass('qodef--hovered');
                    TweenMax.staggerTo($items.filter('.qodef--hovered'), .2, {
                        autoAlpha: 0,
                        ease: Power2.easeOut
                    }, .1);
                });

            $thisHolder
                .on('mouseleave', function () {
                    $item.removeClass('qodef--hovered');
                    TweenMax.staggerTo($items, .2, {
                        autoAlpha: 1,
                        ease: Power2.easeOut
                    }, .05);
                });
        },
        getItems: function ($holder) {
            var $items = $holder.find('.qodef-e').sort(function (a, b) {
                return 0.5 - Math.random()
            })
            return $items;
        }
    };

})(jQuery);
(function ($) {

    $(document).ready(function () {
        qodefBlogListMinimal.init();
    });

    var qodefBlogListMinimal = {
        init: function () {
            var $sections = $('.qodef-blog.qodef-item-layout--minimal.qodef--with-animation');

            $sections.length && $sections.each(function () {
                var $section = $(this);
                qodefBlogListMinimal.observe($section);
            });
        },
        observe: function ($section) {
            var $items = $section.find('.qodef-e');

            $section.appear(function () {
                $items.each(function (i) {
                    var $item = $(this);

                    qodefBlogListMinimal.animate($item, i / 10);
                })
            })
        },
        animate: function ($item, delay) {
            var $top = $item.find('.qodef-info--top'),
                $title = $item.find('.qodef-e-title'),
                $bottom = $item.find('.qodef-info--bottom');

            var timeline = new TimelineMax();

            timeline
                .to($item, .6, {
                    autoAlpha: 1,
                    delay: Modernizr.touchevents ? 0 : delay
                })
                .from($top, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.6")
                .from($title, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($bottom, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4");
        }
    };

})(jQuery);
(function ($) {

    $(window).on('load', function () {
        qodefInfoBelowParallax.init();
    });

    var qodefInfoBelowParallax = {
        init: function () {
            var $articles = $('.qodef-item-layout--info-below.qodef--with-parallax article');

            if ($articles.length && !Modernizr.touchevents) {
                $articles.each(function () {
                    var $article = $(this);

                    $article.data('y', Math.floor(Math.random() * 100) + 50);
                    qodefInfoBelowParallax.observe($article);
                })
            }
        },
        observe: function ($item) {
            qodefInfoBelowParallax.isInViewport($item[0]) && qodefInfoBelowParallax.animate($item);

            requestAnimationFrame(function () {
                qodefInfoBelowParallax.observe($item);
            })
        },
        animate: function ($item) {
            var y = qodefInfoBelowParallax.yVal($item);

            TweenMax.to($item.find('.qodef-e-inner'), 1, {
                y: y,
                ease: Power4.easeOut,
                overwrite: 1
            });
        },
        yVal: function ($el) {
            var max = 1;
            var min = 0;
            var y = ($el[0].getBoundingClientRect().top + $el[0].clientHeight) / window.innerHeight;
            return (1 - Math.max(Math.min(y, max), min)) * -$el.data('y');
        },
        isInViewport: function (elem) {
            var bounding = elem.getBoundingClientRect();
            return (
                bounding.top + elem.clientHeight >= 0 &&
                bounding.bottom - elem.clientHeight <= (window.innerHeight || document.documentElement.clientHeight)
            );
        }
    };

})(jQuery);
(function ($) {

    $(window).on('load', function () {
        qodefPtfInfoOnHoverAnimation.init();
    });

    var qodefPtfInfoOnHoverAnimation = {
        init: function () {
            var $sections = $('.qodef-portfolio-list.qodef-item-layout--info-on-hover.qodef--with-animation');

            $sections.length && $sections.each(function () {
                var $section = $(this),
                    $items = qodefPtfInfoOnHoverAnimation.getItems($section);

                qodefPtfInfoOnHoverAnimation.observe($section, $items);
            });
        },
        observe: function ($section, $items) {
            $section.appear(function () {
                qodefPtfInfoOnHoverAnimation.animate($items);
            })
        },
        animate: function ($items) {
            var i = 0;

            TweenMax.staggerFromTo($items, .5, {
                scaleX: 1.25,
                transformOrigin: '0 0'
            }, {
                scaleX: 1,
                ease: Power4.easeOut,
                onComplete: function () {
                    TweenMax.set($items[i], {
                        'pointer-events': 'auto'
                    });
                    i++;
                }
            }, .05)

            TweenMax.staggerFromTo($items, .5, {
                autoAlpha: 0,
            }, {
                autoAlpha: 1,
                ease: Expo.easeOut,
            }, .05)
        },
        shuffle: function (array) {
            var currentIndex = array.length,
                temporaryValue, randomIndex;

            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        },
        getItems: function ($holder) {
            var $items = $holder.find('.qodef-e-inner').sort(function (a, b) {
                return 0.5 - Math.random()
            })
            return $items;
        }
    };

})(jQuery);
(function ($) {

    $(document).ready(function () {
        qodefTeamInfoBelow.init();
    });

    var qodefTeamInfoBelow = {
        init: function () {
            var $sections = $('.qodef-team-list.qodef--with-animation');

            $sections.length && $sections.each(function () {
                var $section = $(this);
                qodefTeamInfoBelow.observe($section);
            });
        },
        observe: function ($section) {
            var $items = $section.find('.qodef-e');

            $section.appear(function () {
                $items.each(function (i) {
                    var $item = $(this);

                    qodefTeamInfoBelow.animate($item, i / 5);
                })
            })
        },
        animate: function ($item, delay) {
            var $image = $item.find('.qodef-e-image'),
                $role = $item.find('.qodef-e-role'),
                $title = $item.find('.qodef-e-title'),
                $icons = $item.find('.qodef-team-member-social-icons');

            var timeline = new TimelineMax();

            timeline
                .to($item, .6, {
                    autoAlpha: 1,
                    delay: Modernizr.touchevents ? 0 : delay
                })
                .from($image, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power4.easeOut
                }, "-=.6")
                .from($role, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($title, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($icons, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4");
        }
    };

})(jQuery);
(function ($) {
	'use strict';
	
	$(window).on('load', function () {
		qodefTilt.init();
	});
	
	$(document).on('boldlab_trigger_get_new_posts', function () {
		qodefTilt.init();
	});
	
	/**
	 * Init tilt hover animation
	 */
	var qodefTilt = {
		init: function () {
            var $items = $('.qodef-hover-animation--tilt article');

            if ($items.length && !Modernizr.touchevents) {
                $items.each(function () {
                    var $trigger = $(this),
                        $targets = $trigger.find('.qodef-e-image');

                    $trigger
                        .on('mousemove touchmove', function (e) {
                            qodefTilt.enter(e, $trigger, $targets);
                        })
                        .on('mouseout touchend', function (e) {
                            qodefTilt.leave($targets);
                        });
                });
            }
        },
        enter: function (e, tr, ta) {
            var aFx = 70,
                trF = 4,
                cH = tr.innerHeight(),
                cW = tr.innerWidth(),
                eX = (e.originalEvent.type === 'touchmove') ? e.originalEvent.touches[0].pageX : e.offsetX,
				eY = (e.originalEvent.type === 'touchmove') ? e.originalEvent.touches[0].pageY : e.offsetY;
				
            $.each(ta, function (i, el) {
                TweenMax.set($(el), {
                    transformOrigin: ((eX / (cW * trF) / 100 * 10000) + (trF * 10)) + '% ' + ((eY / (cH * trF) / 100 * 10000) + (trF * 10)) + '%',
                    transformPerspective: 1000 + (i * 500)
                });
                TweenMax.to($(el), 0.5, {
                    rotationX: ((eY - cH / 2) / aFx) - i * 2,
                    rotationY: ((eX - cW / 2) / aFx * -1) - i * 2,
                    y: (eY - (cH / 2)) / (50 - i * 20),
                    x: (eX - (cW / 2)) / (50 - i * 20)
                });
            });
        },
        leave: function (ta) {
            $.each(ta, function (i, el) {
                TweenMax.to($(el), .75, {
                    delay: .2,
                    y: 0,
                    x: 0,
                    rotationX: 0,
                    rotationY: 0,
                    transformPerspective: '1500'
                });
            });
        },
	};
	
})(jQuery);
(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefInfoFollow.init();
	});
	
	$(document).on('boldlab_trigger_get_new_posts', function () {
		qodefInfoFollow.init();
	});
	
	/**
	 * Init follow hover animation
	 */
	var qodefInfoFollow = {
		init: function () {
			var $gallery = $('.qodef-hover-animation--follow');
			
			if ($gallery.length) {
				qodef.body.append('<div class="qodef-follow-info-holder"><div class="qodef-follow-info-inner"><span class="qodef-follow-info-category"></span><br/><span class="qodef-follow-info-title"></span></div></div>');
				
				var $followInfoHolder = $('.qodef-follow-info-holder'),
					$followInfoCategory = $followInfoHolder.find('.qodef-follow-info-category'),
					$followInfoTitle = $followInfoHolder.find('.qodef-follow-info-title');
				
				$gallery.each(function () {
					$gallery.find('.qodef-e-inner').each(function () {
						var $thisItem = $(this);
						
						//info element position
						$thisItem.on('mouseenter mousemove', function (e) {
							if(e.clientX + 20 + $followInfoHolder.width() > qodef.windowWidth){
                                $followInfoHolder.addClass('qodef-right');
							}else{
                                $followInfoHolder.removeClass('qodef-right');
							}
							$followInfoHolder.css({
								top: e.clientY + 20,
								left: e.clientX + 20
							});
						});

						//show/hide info element
						$thisItem.on('mouseenter', function () {
							var $thisItemTitle = $(this).find('.qodef-e-title'),
								$thisItemCategory = $(this).find('.qodef-e-info-category');
							
							if ($thisItemTitle.length) {
								$followInfoTitle.html($thisItemTitle.clone());
							}
							
							if ($thisItemCategory.length) {
								$followInfoCategory.html($thisItemCategory.html());
							}
							
							if (!$followInfoHolder.hasClass('qodef-is-active')) {
								$followInfoHolder.addClass('qodef-is-active');
							}
						}).on('mouseleave', function () {
							if ($followInfoHolder.hasClass('qodef-is-active')) {
								$followInfoHolder.removeClass('qodef-is-active');
							}
						});
					});
				});
			}
		}
	};
	
})(jQuery);
(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefHoverDir.init();
	});
	
	$(document).on('boldlab_trigger_get_new_posts', function () {
		qodefHoverDir.init();
	});
	
	/**
	 * Init hoverdir hover animation
	 */
	var qodefHoverDir = {
		init: function () {
			var $gallery = $('.qodef-hover-animation--direction-aware');
			
			if ($gallery.length) {
				$gallery.each(function () {
					var $this = $(this);
					$this.find('article').each(function () {
						$(this).hoverdir({
							hoverElem: 'div.qodef-e-content',
							speed: 330,
							hoverDelay: 35,
							easing: 'ease'
						});
					});
				});
			}
		}
	};
	
})(jQuery);