��          L      |       �   {   �   %   %     K  ;   f     �  .  �  �   �  %   �     �  ;   �     �                                         Embed Google reviews fast and easily into your WordPress site. Increase SEO, trust and sales using Google business reviews. Trustindex.io <support@trustindex.io> Widgets for Google Reviews https://wordpress.org/plugins/wp-reviews-plugin-for-google/ https://www.trustindex.io/ PO-Revision-Date: 2021-03-10 15:24:25+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - Widgets for Google Reviews - Development (trunk)
 Intégrez vos avis Google dans votre site WordPress facilement et vite. Améliorez votre SEO, la confiance et les ventes utilisant des avis Google. Trustindex.io <support@trustindex.io> Widgets d'avis Google https://wordpress.org/plugins/wp-reviews-plugin-for-google/ https://www.trustindex.io/ 